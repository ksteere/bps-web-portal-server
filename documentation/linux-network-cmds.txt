LINUX NETWORKING COMMANDS CHEAT SHEET wlan0 (74:da:38:de:4a:92) wlx74da38de4a92
-----------------------------------------------------------------------------------------------------------------------------------------------------------
CONFIG FILES
/etc/network/interfaces
auto wlan0:1
iface wlan0:1 inet static
address 192.168.50.222
netmask 255.255.255.0
broadcast 192.168.50.255
 /etc/init.d/networking restart OR service networking restart
sudo ifup wlan0:1
-----------------------------------------------------------------------------------------------------------------------------------------------------------
RESARTING SERVICES
/etc/init.d/networking restart 
service networking restart
sudo service network-manager restart
systemctl restart network-manager
service network-manager restart
man nm-settings
-----------------------------------------------------------------------------------------------------------------------------------------------------------
ifconfig -v
iw list | grep "combinations" <- may only work when combos are supported but needs the interfaces config filed to support a virtual interface

iw dev wlx74da38de4a92 interface add wlan0_sta type managed addr 74:da:38:de:4a:92 (generates table A when viewed with nw manager)
rebooting kills wlan0 but hotplugging the device switches it back to wlan0

Table A
DEVICE           TYPE      STATE        CONNECTION
enp2s0           ethernet  connected    enp2s0
wlan0            wifi      connected    Orbital
wlx74da38de4a92  wifi      unavailable  --

-----------------------------------------------------------------------------------------------------------------------------------------------------------
nmcli  connection show "Wired connection 1"
nmcli device status
nmcli -w 10 device show enp2s0
nmcli --terse --fields device,type,state,connection devic status
nmcli --terse --fields connection.type,802-3-ethernet.mac-address,ipv4.method,IP4.ADDRESS,IP4.DNS,IP4.GATEWAY,DHCP4.OPTION connection show "Wired connection 1"
-----------------------------------------------------------------------------------------------------------------------------------------------------------
sudo nmcli -w 10 device wifi connect Orbital password MercuryVenusEarthMars ifname wlx74da38de4a92
-----------------------------------------------------------------------------------------------------------------------------------------------------------
Network Manager connections management
sudo nmcli con delete id Orbital
sudo nmcli con up Orbital
sudo nmcli con down Orbital
sudo nmcli d connect wlan0
sudo nmcli d disconnect wla0
nmcli dev set wlan0 managed yes
-----------------------------------------------------------------------------------------------------------------------------------------------------------
Networking Manager modify connection information see Table 1
nmcli con mod "Orbital" ipv4.address 192.168.50.62/24
nmcli con mod Orbital ipv4.method manual ipv4.addr "192.168.50.220/24"
ipv.method [auto, link-local, manual, shared, disabled].
-----------------------------------------------------------------------------------------------------------------------------------------------------------
Security Settings/iPad and iPhone 
nmcli c modify <name> wifi-sec.key-mgmt wpa-psk wifi-sec.psk <password>
nmcli connection add type wifi con-name "MySSID" ifname wlp3s0 ssid "MySSID" wifi-sec.key-mgmt wpa-eap 802-1x.eap tls 802-1x.identity "USERNAME" 802-1x.ca-cert ~/ca.pem 802-1x.client-cert ~/cert.pem 802-1x.private-key-password "password1234" 802-1x.private-key ~/key.pem
nmcli connection modify "Test" wifi-sec.key-mgmt wpa-eap 802-1x.eap tls 802-1x.identity "admin" 802-1x.ca-cert ca.pem 802-1x.client-cert cert.pem 802-1x.private-key-password "password1234" 802-1x.private-key key.pem
-----------------------------------------------------------------------------------------------------------------------------------------------------------
Network Manager General
nmcli -m tabular/multiline
nmcli -w 10 (wait 10 seconds)
nmcli g (general status)
nmcli g hostname
-----------------------------------------------------------------------------------------------------------------------------------------------------------
Network Manager simple hotspot creation
sudo nmcli d wifi hotspot ifname wlx74da38de4a92 con-name scott-linux ssid scott-linux password suckmydick
sudo nmcli -w 10 device wifi connect Orbital password MercuryVenusEarthMars ifname wlx74da38de4a92
-----------------------------------------------------------------------------------------------------------------------------------------------------------
Table1 - Network Manager items below should be set with the modify command 
-----------------------------------------------------------------------------------------------------------------------------------------------------------
connection.id:                          Wired connection 1
connection.uuid:                        d6b41003-b885-32ed-a058-7539088d5bb6
connection.interface-name:              --
connection.type:                        802-3-ethernet
connection.autoconnect:                 yes
connection.autoconnect-priority:        -999
connection.timestamp:                   1585892660
connection.read-only:                   no
connection.permissions:
connection.zone:                        --
connection.master:                      --
connection.slave-type:                  --
connection.autoconnect-slaves:          -1 (default)
connection.secondaries:
connection.gateway-ping-timeout:        0
connection.metered:                     unknown
connection.lldp:                        -1 (default)
802-3-ethernet.port:                    --
802-3-ethernet.speed:                   0
802-3-ethernet.duplex:                  --
802-3-ethernet.auto-negotiate:          yes
802-3-ethernet.mac-address:             00:22:68:62:9F:99
802-3-ethernet.cloned-mac-address:      --
802-3-ethernet.mac-address-blacklist:
802-3-ethernet.mtu:                     auto
802-3-ethernet.s390-subchannels:
802-3-ethernet.s390-nettype:            --
802-3-ethernet.s390-options:
802-3-ethernet.wake-on-lan:             1 (default)
802-3-ethernet.wake-on-lan-password:    --
ipv4.method:                            auto
ipv4.dns:
ipv4.dns-search:
ipv4.dns-options:                       (default)
ipv4.dns-priority:                      0
ipv4.addresses:
ipv4.gateway:                           --
ipv4.routes:
ipv4.route-metric:                      -1
ipv4.ignore-auto-routes:                no
ipv4.ignore-auto-dns:                   no
ipv4.dhcp-client-id:                    --
ipv4.dhcp-timeout:                      0
ipv4.dhcp-send-hostname:                yes
ipv4.dhcp-hostname:                     --
ipv4.dhcp-fqdn:                         --
ipv4.never-default:                     no
ipv4.may-fail:                          yes
ipv4.dad-timeout:                       -1 (default)
ipv6.method:                            auto
ipv6.dns:
ipv6.dns-search:
ipv6.dns-options:                       (default)
ipv6.dns-priority:                      0
ipv6.addresses:
ipv6.gateway:                           --
ipv6.routes:
ipv6.route-metric:                      -1
ipv6.ignore-auto-routes:                no
ipv6.ignore-auto-dns:                   no
ipv6.never-default:                     no
ipv6.may-fail:                          yes
ipv6.ip6-privacy:                       -1 (unknown)
ipv6.addr-gen-mode:                     stable-privacy
ipv6.dhcp-send-hostname:                yes
ipv6.dhcp-hostname:                     --
GENERAL.NAME:                           Wired connection 1
GENERAL.UUID:                           d6b41003-b885-32ed-a058-7539088d5bb6
GENERAL.DEVICES:                        enp2s0
GENERAL.STATE:                          activated
GENERAL.DEFAULT:                        yes
GENERAL.DEFAULT6:                       no
GENERAL.VPN:                            no
GENERAL.ZONE:                           --
GENERAL.DBUS-PATH:                      /org/freedesktop/NetworkManager/ActiveConnection/0
GENERAL.CON-PATH:                       /org/freedesktop/NetworkManager/Settings/1
GENERAL.SPEC-OBJECT:                    /
GENERAL.MASTER-PATH:                    --
IP4.ADDRESS[1]:                         192.168.50.215/24
IP4.GATEWAY:                            192.168.50.1
IP4.ROUTE[1]:                           dst = 169.254.0.0/16, nh = 0.0.0.0, mt = 1000
IP4.DNS[1]:                             192.168.50.1
DHCP4.OPTION[1]:                        requested_subnet_mask = 1
DHCP4.OPTION[2]:                        requested_rfc3442_classless_static_routes = 1
DHCP4.OPTION[3]:                        subnet_mask = 255.255.255.0
DHCP4.OPTION[4]:                        domain_name_servers = 192.168.50.1
DHCP4.OPTION[5]:                        ip_address = 192.168.50.215
DHCP4.OPTION[6]:                        requested_static_routes = 1
DHCP4.OPTION[7]:                        dhcp_server_identifier = 192.168.50.1
DHCP4.OPTION[8]:                        requested_time_offset = 1
DHCP4.OPTION[9]:                        broadcast_address = 192.168.50.255
DHCP4.OPTION[10]:                       requested_interface_mtu = 1
DHCP4.OPTION[11]:                       dhcp_rebinding_time = 72399
DHCP4.OPTION[12]:                       requested_domain_name_servers = 1
DHCP4.OPTION[13]:                       dhcp_message_type = 5
DHCP4.OPTION[14]:                       requested_broadcast_address = 1
DHCP4.OPTION[15]:                       routers = 192.168.50.1
DHCP4.OPTION[16]:                       wpad = a
DHCP4.OPTION[17]:                       dhcp_renewal_time = 39999
DHCP4.OPTION[18]:                       requested_domain_name = 1
DHCP4.OPTION[19]:                       requested_routers = 1
DHCP4.OPTION[20]:                       expiry = 1585978526
DHCP4.OPTION[21]:                       requested_wpad = 1
DHCP4.OPTION[22]:                       host_name = linux-dev-desktop
DHCP4.OPTION[23]:                       requested_netbios_scope = 1
DHCP4.OPTION[24]:                       requested_ms_classless_static_routes = 1
DHCP4.OPTION[25]:                       requested_netbios_name_servers = 1
DHCP4.OPTION[26]:                       network_number = 192.168.50.0
DHCP4.OPTION[27]:                       requested_domain_search = 1
DHCP4.OPTION[28]:                       requested_ntp_servers = 1
DHCP4.OPTION[29]:                       next_server = 192.168.50.1
DHCP4.OPTION[30]:                       requested_host_name = 1
DHCP4.OPTION[31]:                       dhcp_lease_time = 86400
IP6.ADDRESS[1]:                         fe80::14ed:645c:d5a0:2781/64
IP6.GATEWAY:
