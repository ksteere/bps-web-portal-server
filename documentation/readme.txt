This document will contain the various items that describe this project and the ideas about how it should proceed in it's entirety beyond the scope of just the portal. It will also include an over view of the structure, setup, and forward looking thoughts.


Project Setup

The project is composed of two pieces - client and server. The server is built on node.js using javascript and the client is an Angular/TS project built upon the Angular frame work. You will need to install node.js and NPM locally to run the Angular CLI which is used to built the client, here: https://nodejs.org/en/. The server side JS doesn't require andy interpolation or compilation like Angular beause it is built with pure JS where the Angular project uses mostly Type Script. Once Node is installed you can install the Angular CLI by opening a command console and typing the following: - npm install -g @angular/cli - see here:  https://angular.io/cli.

It is also suggested that you install MS VS Code here: https://code.visualstudio.com/download

Once you are ready, assuming you use VS code, you can open the top level or root folder of the project bps-web-portal-server and open in VS Code.
At this point go to VS menu bar and select Terminal \New Terminal to bring up a new terminal window (this can be done from a windows command prompt as well). 

In the terminal (assuming you opened the top level project your current directory should be the same). Switch directories, cd transact-ps-portal, to transact-ps-portal. Then type the command ng build. The project should build and copy the contents to a folder called "content" under the parent or top level bps-web-portal.

[+]bash-scripts
	----helper scripts used by the REST API
[+]content
	----content (output) directory
[+]transact-ps-portal (GUI project)
	----angular project
[+]node_modules
	----node NMP 3rd party modules 
[+]service-modules
	----portal or PS sepcific modules
[+]lib
	----transact specific modules

*transact.portal.service.js (bootstrap or main node entry point) this does all the wiring and magic

[+] transact-ps-portal (this is an angular 6 prject)
 


This will install all dependencies including NWM and NodeJs

You can then copy over the project files required to run the project. Under the parent/root directory of the project bps-web-portal-server
copy the following to your run time env

-bash-scripts
-content
-service-modules
-lib
-transact.portal.service.js
-configuration

Next you will need to setup the runtime env on whichever platform you are running on. See configuration\provisoin.sh. Make the provision script executable - chmod +x ./configuration/provision.sh and run the script ./configuration/provision.sh

The above will install all dependences if not already installed. 

Verify node is installed:
type node -v

run the servie sudo node transact.portal.service.js

The portal consists of web content an http server and several rest API's. These API's can be extended using scripts and can be interfaced from the any client one the server is running (it will run as system daemon). This means things like configuration can be automated through outside tools and not just the portal.

API's that need to be supported - reference APIs.txt (96 - 112)
96.) path: /api/v1/user/authenticate, verb: POST, type: object
97.) path: /api/v1/connectivity/devices, verb: GET, type: object
98.) path: /api/v1/connectivity/connections, verb: GET, type: object
99.) path: /api/v1/connectivity/wifinetworks, verb: GET, type: object
100.) path: /api/v1/connectivity/wifi/connect, verb: POST, type: object
101.) path: /api/v1/connectivity/device/details, verb: GET, type: object
102.) path: /api/v1/connectivity/connection/details, verb: GET, type: object
103.) path: /api/v1/connectivity/connection/setting, verb: POST, type: object
104.) path: /api/v1/connectivity/connection/ip/method, verb: POST, type: object
105.) path: /api/v1/connectivity/connection/delete, verb: POST, type: object
106.) path: /api/v1/connectivity/ap/connect, verb: POST, type: object
107.) path: /api/v1/system/proxy/setting, verb: DELETE, type: object
108.) path: /api/v1/system/proxy/setting, verb: GET, type: object
109.) path: /api/v1/system/proxy/setting, verb: POST, type: object
110.) path: /api/v1/system/avahi/name, verb: POST, type: object
111.) path: /api/v1/system/avahi/name, verb: GET, type: object
112.) path: /api/v1/connectivity/echo/host/connection, verb: GET, type: object




GNOME - https://wiki.debian.org/NetworkManager
The idea that we develop and solve independently is only going to result in a brittle solution that fails to deliver, will become problematic to integrate and will ultimate be comprimised and rushed out the door.

Our biggest challenge is system wide architecture, particulary on the linux or compute module (RPI). The PI ships default with DHCPCD and a few other network utilities but just like the design of our system these tools manage configurations somewhat independently and silod. Without getting into the internals of networking on linux there is another solution, which the portal is being based around, and that is NetworkManager. NM has been around since 2004 and started out as solution to simplify networking on linux. NM has since evolved and many use it beyond the desktop. This will run on the pi as well but may require the default image to be provisioned with it and to possible remove DHCPCD though that isn't necessary and infact NM can be confiured to use DHCPCD as its DCHP client if so desired (v4 ip addresses only) otherwise it uses its own internal. 

The benefits of NM are the following:
History and hardended. 2004
A single management utility for interfacing for configuration data regarding networks.
A CLI purposefully designed to product clean delimitable records.
A native C++ lib
A DBUS interface

The above benefits allow many disparte parties or services to communicate to one hub using a single standard to get native information (we could use the portals api as well) but for things that may not be supported. In addition the use of DBUS allows for the use of PUB/SUB something that we should be implmenting in our architecture. DBUS is light weight and can be run on the pi as it is the defacto messaging bus for linux (built into the kenrel).

On that note we should be considering DBUS as the use of inter proc communication as it facilitates the command and control as well as data exchage directly or through multiple subscribers (pub/sub).