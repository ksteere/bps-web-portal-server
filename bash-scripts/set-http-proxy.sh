#!/bin/bash

#"/etc/profile.d/http-settings.sh" or ~/.profile
#it is important to know that the sudo doesn't have a true shell and env parameters and that the bash shell is a child process
#of the command shell and thus 1. environmental variables are lost unless you use source <scriptname> and two it is all but impossible
#to execute something partially as root. 1 option would be to specify this script or the user it's run under in sudoers file but that didn't
#seem to work either. Linux and bash are screwy that way and make it beyond difficult to do something simple as call script with user and 
#edit a file. That is why we allow the profile/system file option as the second argument (remember it's API/POST/GET params first then system and absolute
#parameters next. We could actually figure out the profile from node or the running application or the shell but we won't but we won't for now.)
#file $HOME/.profile would eliminate a variable
#------------------------------------------------------------------------------------------------------------------------------------------------
#NOTE: could use awk here...
# results="$($file | 'awk {
#     gsub(/.*http_proxy*/,"", $0);
#     print $0
# }')"
# echo $results > $file
#------------------------------------------------------------------------------------------------------------------------------------------------
newproxy="$(echo $1 | grep -Po '"httpProxy"[ ]*: *\K"[^"]*"')"
#strip off any leading quotes
tempconn="${newproxy%\"}"
newproxy="${tempconn#\"}"
file=$2
if  test -f "$file" ; then
    sudo sed -i 's/http_proxy.*//g' $file
    sudo echo "http_proxy=$newproxy" >> "$file"
else
    sudo touch $file
    sudo sed -i 's/http_proxy.*//g' $file
fi;
export "http_proxy=$newproxy"
