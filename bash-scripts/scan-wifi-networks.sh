#!/bin/bash
#Angular Structure class Network
# state: String;
# ssid: String;
# channel: Number;
# frequency: Number; // in MHz
# signal_level: Number; // in dB
# bars : Number;
# quality: Number; // same as signal level but in %
# security: String; // format depending on locale for open networks in Windows
# device : String;
#Example Formatted JSON Data output
# [
#   {
#     "state": "connected",
#     "ssid": "Orbital",
#     "channel": "1",
#     "frequency": "2412 MHz",
#     "signal_level": "84",
#     "bars": "4",
#     "security": "WPA2",
#     "device": "wlx74da38de4a92"
#   },
#   {
#     "state": "disconnected",
#     "ssid": "KimWifi",
#     "channel": "1",
#     "frequency": "2412 MHz",
#     "signal_level": "82",
#     "bars": "4",
#     "security": "WPA1 WPA2",
#     "device": "wlx74da38de4a92"
#   },
#   {
#     "state": "disconnected",
#     "ssid": "CenturyLink3448",
#     "channel": "1",
#     "frequency": "2412 MHz",
#     "signal_level": "82",
#     "bars": "4",
#     "security": "WPA1 WPA2",
#     "device": "wlx74da38de4a92"
#   }
#  ]
#set variables for the start and end fo the json object which is an array
jsonStart="["
jsonEnd="]"
adapter="$(echo $1 | grep -Po '"adapter"[ ]*: *\K"[^"]*"')"
tempadapter="${adapter%\"}"
adapter="${tempadapter#\"}"
#$1
#wlx74da38de4a92
command="nmcli --terse --fields active,ssid,chan,freq,signal,bars,security,device device wifi list ifname $adapter"
#the body of the array set into a variable
#object  = objectStart quote "state" quote ":" quote fields[1] quote  "," quote "ssid" quote ":" quote fields[2] quote "," quote "channel" quote ":" quote fields[3] quote  "," quote "frequency"  quote ":" quote fields[4] quote  "," quote "signal_level"  quote ":" quote fields[5] quote "," quote "bars"  quote ":" quote fields[6] quote "," quote "security"  quote ":" quote fields[7] quote "," quote "device"  quote ":" quote fields[8] quote objectEnd ; 	
arraybody="$($command |
	 awk '{ objectStart = "{";
	objectEnd = "}";
	quote = "\"";	
	json = jsonStart; 
    constate = "disconnected";
	split($0,fields, ":");
    if(fields[1] == "yes"){
        constate = "connected";
    }
    bars = int(fields[5]/25);
    object  = objectStart quote "state" quote ":" quote constate quote  "," quote "ssid" quote ":" quote fields[2] quote "," quote "channel" quote ":" quote fields[3] quote  "," quote "frequency"  quote ":" quote fields[4] quote  "," quote "signal_level"  quote ":" quote fields[5] quote "," quote "bars"  quote ":" quote bars quote "," quote "security"  quote ":" quote fields[7] quote "," quote "device"  quote ":" quote fields[8] quote objectEnd; 	
	json = json object ",";
	print json;
	}')"
#return vales
#NOTE: Scripts will need to report an error in the form of 
# { 
# 	 "result": "error", 
# 	 "message": "message", 
# 	 "errorObject": null 
# }
#above is a happy path exampleclear

echo $jsonStart${arraybody::-1}$jsonEnd
