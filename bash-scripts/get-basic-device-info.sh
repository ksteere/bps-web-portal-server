#!/bin/bash
# Example Formatted JSON Data output
# [
#    {
#       "device":"enp2s0",
#       "type":"ethernet",
#       "state":"connected",
#       "connection":"Wired connection 1"
#    },
#    {
#       "device":"wlx74da38de4a92",
#       "type":"wifi",
#       "state":"connected",
#       "connection":"Orbital"
#    },

#    {
#       "device":"lo",
#       "type":"loopback",
#       "state":"unmanaged",
#       "connection":"--"
#    }
# ]
#set variables for the start and end fo the json object which is an array
jsonStart="["
jsonEnd="]"
#the body of the array set into a variable
arraybody="$(nmcli -t -f  device,type,state,connection d |
	 awk '{ objectStart = "{";
	objectEnd = "}";
	quote = "\"";	
	json = jsonStart; 
	split($0,fields, ":");
	object  = objectStart quote "device" quote ":" quote fields[1] quote  "," quote "type" quote ":" quote fields[2] quote "," quote "state" quote ":" quote fields[3] quote  "," quote "connection"  quote ":" quote fields[4] quote objectEnd ; 	
	json = json object ",";
	print json;
	}')"
#return vales
#NOTE: Scripts will need to report an error in the form of 
# { 
# 	 "result": "error", 
# 	 "message": "message", 
# 	 "errorObject": null 
# }
#above is a happy path example
echo $jsonStart ${arraybody::-1} $jsonEnd

