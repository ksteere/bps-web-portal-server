#!/bin/bash
elementname="$(echo $1 | grep -Po '"name"[ ]*: *\K"[^"]*"')"
temp="${elementname%\"}"
elementname="${temp#\"}"
elementvalue="$(echo $1 | grep -Po '"value"[ ]*: *\K"[^"]*"')"
temp="${elementvalue%\"}"
elementname="${temp#\"}"
file=$2
if  test -f "$file" ; then
    sudo sed -i "s/<$elementname>.*<\/$elementname>/<$elementname>$elementvalue<\/$elementname>/g" $file
else
    echo "ERROR: $file does not exist."
    exit 1
fi;
