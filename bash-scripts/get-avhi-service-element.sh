#!/bin/bash
elementname="$(echo $1 | grep -Po '"name"[ ]*: *\K"[^"]*"')"
temp="${elementname%\"}"
elementname="${temp#\"}"
file=$2
element="$(grep -oP "(?<=<$elementname>)(.*?)(?=</$elementname>)" "$file")"
echo $element
echo "{\"$elementname\" : \"$element\"}"