#!/bin/bash
#Example Formatted JSON Data output
# {
#   "hostname": "linux-dev-desktop",
#	"id":"Orbital"	
#   "type": "802-11-wireless",
#   "method": "auto",
#   "mac": "74:da:38:de:4a:92",
#   "security": "wpa-psk",
#   "mode": "infrastructure",
#   "hidden": "no",
#   "primaryAddress": "192.168.50.54/24",
#   "dns": "192.168.50.1",
#   "gateway": "192.168.50.1",
#   "requested_subnet_mask": "1",
#   "requested_rfc3442_classless_static_routes": "1",
#   "subnet_mask": "255.255.255.0",
#   "domain_name_servers": "192.168.50.1",
#   "ip_address": "192.168.50.54",
#   "requested_static_routes": "1",
#   "dhcp_server_identifier": "192.168.50.1",
#   "requested_time_offset": "1",
#   "broadcast_address": "192.168.50.255",
#   "requested_interface_mtu": "1",
#   "dhcp_rebinding_time": "75600",
#   "requested_domain_name_servers": "1",
#   "dhcp_message_type": "5",
#   "requested_broadcast_address": "1",
#   "routers": "192.168.50.1",
#   "wpad": "a",
#   "dhcp_renewal_time": "43200",
#   "requested_domain_name": "1",
#   "requested_routers": "1",
#   "expiry": "1587169754",
#   "requested_wpad": "1",
#   "host_name": "linux-dev-desktop",
#   "requested_netbios_scope": "1",
#   "requested_ms_classless_static_routes": "1",
#   "requested_netbios_name_servers": "1",
#   "network_number": "192.168.50.0",
#   "requested_domain_search": "1",
#   "requested_ntp_servers": "1",
#   "next_server": "192.168.50.1",
#   "requested_host_name": "1",
#   "dhcp_lease_time": "86400",
# 	"ssid" : "Orbital",
# }

#set variables for the start and end fo the json object which is an array
#input {"name" : "con-name"}
jsonStart="{"
jsonEnd="}"
connection="$(echo $1 | grep -Po '"name"[ ]*: *\K"[^"]*"')"
temp="${connection%\"}"
connection="${temp#\"}"
hostname="$(nmcli g hostname)"
command="nmcli --terse --fields connection.id,connection.type,ipv4.method,IP4.ADDRESS,IP4.DNS,IP4.GATEWAY,DHCP4.OPTION,802-3-ethernet.mac-address,802-11-wireless.mac-address,802-11-wireless-security.key-mgmt,802-11-wireless.mode,802-11-wireless.hidden,802-11-wireless.ssid connection show $connection"
arraybody="$($command |
	 awk '{ 
   	quote = "\"";	
	text = "";
	splitchar = ":";
	fieldName = "";
	fieldValue = "null";
	if($0 ~/connection.id/ || $0 ~/802-11-wireless.ssid/){
		text = $0;
	}
	else{
		text = tolower($0);
	}
	if(text ~/mac-address/){
		gsub(".mac-address:", ".mac-address%", text);
		splitchar = "%";
	}
	else if(text ~/dhcp4.option\[[0-9]*]:/){
		gsub(/dhcp4.option\[[0-9]*]:/, "", text);
		splitchar = "=";
	}
	split(text,fields, splitchar);
	gsub(/ /, "", fields[1]);
	gsub(/ /, "", fields[2]);
	if(fields[1] == "connection.type"){
		fieldName = "type";
		fieldValue = fields[2];
	}
	else if(fields[1] ~/ip4\.dns/){
		fieldName = "dns";
		fieldValue = fields[2];
	}
	else if(fields[1] == "ipv4.method"){
		fieldName = "method";
		fieldValue = fields[2];

	}
	else if(fields[1] == "802-11-wireless-security.key-mgmt"){
		fieldName = "security";
		fieldValue = fields[2];

	}
	else if(fields[1] == "802-11-wireless.mode"){
		fieldName = "mode";
		fieldValue = fields[2];

	}
	else if(fields[1] == "802-11-wireless.ssid"){
		fieldName = "ssid";
		fieldValue = fields[2];

	}
	else if(fields[1] == "connection.id"){
		fieldName = "id";
		fieldValue = fields[2];

	}
	else if(fields[1] == "802-11-wireless.hidden"){
		fieldName = "hidden";
		fieldValue = fields[2];

	}
	else if(fields[1] ~/mac-address/){
		fieldName = "mac";
		fieldValue = fields[2];

	}
	else if(fields[1] == "ip4.address[1]"){
		fieldName = "primaryAddress";
		fieldValue = fields[2];

	}
	else if(fields[1] == "ip4.address[2]"){
		fieldName = "secondaryAddress";
		fieldValue = fields[2];
	}
	else if(fields[1] == "ip4.gateway"){
		fieldName = "gateway";
		fieldValue = fields[2];
	}
	else{
		fieldName = fields[1];
		fieldValue = fields[2];
	}
   line = quote fieldName quote ":" quote fieldValue quote  ",";
	print line;
	}')"
#return vales
#NOTE: Scripts will need to report an error in the form of 
# { 
# 	 "result": "error", 
# 	 "message": "message", 
# 	 "errorObject": null 
# }
#above is a happy path exampleclear
echo $jsonStart\"hostname\":\"$hostname\",${arraybody::-1}$jsonEnd
