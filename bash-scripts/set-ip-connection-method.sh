#!/bin/bash
#get the parameters from the json
connection="$(echo $1 | grep -Po '"connection"[ ]*: *\K"[^"]*"')"
ip="$(echo $1 | grep -Po '"ip"[ ]*: *\K"[^"]*"')"
subnet="$(echo $1 | grep -Po '"subnet"[ ]*: *\K"[^"]*"')"
gateway="$(echo $1 | grep -Po '"gateway"[ ]*: *\K"[^"]*"')"
method="$(echo $1 | grep -Po '"method"[ ]*: *\K"[^"]*"')"
#strip off any leading quotes
tempconn="${connection%\"}"
tempip="${ip%\"}"
tempsubnet="${subnet%\"}"
tempgateway="${gateway%\"}"
tempmethod="${method%\"}"
#strip off any ending quotes and set the variable to the final value
connection="${tempconn#\"}"
ip="${tempip#\"}"
subnet="${tempsubnet#\"}"
gateway="${tempgateway#\"}"
method="${tempmethod#\"}"
#auto, link-local, manual, shared, disabled
if [[ $method == "manual" ]] || [[ $method == "shared" ]]; then
	if [[ -z "$gateway" ]];then
		nmcli con modify $connection ipv4.method $method ipv4.addresses $ip #ipv4.gateway $gateway
	else
		nmcli con modify $connection ipv4.method $method ipv4.addresses $ip ipv4.gateway $gateway
	fi
else
	nmcli con modify $connection ipv4.method "auto"
fi
nmcli con down $connection
nmcli con up $connection 