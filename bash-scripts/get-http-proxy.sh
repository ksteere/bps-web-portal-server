#!/bin/bash
file=$1
#------------------------------------------------------------------------------------------------------------------------------------------------
#------------------------------------------------------------------------------------------------------------------------------------------------
#we should go directly to the file but we can do that if we need to. Both env and file should be set. Though because of the nuances of linux
#security and how it invokes scripts it is possible that depending on the user this is all run under and where sudo is used this could all turn out
#out of sync. This is why the setup, provisioning and finalization of any data this is in config files, esecially, files that export env variables
#is critcal. We need to know the user and the rights of the user so that we can approriately use the right format when calling scripts. 'source' may
#may turn out to be an issue. We should also think about returning a json object here instead of letting the client do the splitting but since it's proabably
#easier to at the client than in awk we will leave it as is for now. Otherwise it should be:
##{"proxy:" "http://x.1.com", "username" : "name", "password" : "123456"}
proxy="$(grep -oP "(?<=http_proxy=).*" $file)"
echo "{\"httpProxy\":\"$proxy\",\"proxyServer\":\"\",\"proxyPort\":\"\",\"userName\":\"\",\"password\":\"\"}"