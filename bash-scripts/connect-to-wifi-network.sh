#!/bin/bash
jsonText=$1
ssid="$(echo $jsonText | grep -Po '"ssid"[ ]*: *\K"[^"]*"')"
tempssid="${ssid%\"}"
ssid="${tempssid#\"}"
password="$(echo $jsonText | grep -Po '"password"[ ]*: *\K"[^"]*"')"
temppassword="${password%\"}"
password="${temppassword#\"}"
adapter="$(echo $jsonText | grep -Po '"adapter"[ ]*: *\K"[^"]*"')"
tempadapter="${adapter%\"}"
adapter="${tempadapter#\"}"
#nmcli -w 10 device wifi connect Orbital password MercuryVenusEarthMars ifname wlx74da38de4a92
 #"error={ \"result\": \"error\", \"message\": $error, \"errorObject\": null}
#wlx74da38de4a92
# { 
# 	 "result": "error", 
# 	 "message": "message", 
# 	 "errorObject": null 
# }
existing="$(nmcli -t -f name c | grep "$ssid")"
if [[ $ssid == $existing ]]; then
	stealth="$(nmcli c delete $ssid)"
fi;
nmcli -w 30 device wifi connect $ssid password $password ifname $adapter
