#!/bin/bash
connection="$(echo $1 | grep -Po '"name"[ ]*: *\K"[^"]*"')"
temp="${connection%\"}"
connection="${temp#\"}"
nmcli conn delete $connection