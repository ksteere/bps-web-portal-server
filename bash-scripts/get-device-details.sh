#!/bin/bash
#Example Formatted JSON Data output
# {
#   "hostname": "linux-dev-desktop",
#   "device": "wlan0",
#   "type": "wifi",
#   "mac": "74:da:38:de:4a:92",
#   "mtu": "0",
#   "state": "100(connected)",
#   "connection": "orbital",
#   "dbus": "/org/freedesktop/networkmanager/activeconnection/2",
#   "ip4address_1": "192.168.50.54/24",
#   "ip4gateway": "192.168.50.1",
#   "ip4route_1": "dst=169.254.0.0/16,nh=0.0.0.0,mt=1000",
#   "ip4dns_1": "192.168.50.1",
#   "ip6address_1": "fe80::e6ce:a04:6f28:bc01/64",
#   "ip6gateway": ""
# }
#set variables for the start and end fo the json object which is an array
#input {"device" : "dev-name"}
jsonStart="{"
jsonEnd="}"
device="$(echo $1 | jq .device)"
temp="${device%\"}"
device="${temp#\"}"
hostname="$(nmcli g hostname)"
command="nmcli device show $device"
arraybody="$($command |
	 awk '{ 
   	quote = "\"";	
	text = "";
	splitchar = ":";
	fieldName = "";
	fieldValue = "null";
	text = tolower($0);
	if(text ~/general\./){
		if(text~/general.hwaddr:/){
            sub(":","=",text)
            splitchar = "=";
        }
	    gsub(/general\./, "", text);
	}
	else if(text~/ip[0-9]\..*:/){
        if(text~/ip6\.address/){
            sub(":","=",text)
            splitchar = "=";
        }
		sub(/\./, "", text);
        sub(/\[/,"_", text);
        sub(/\]/,"", text);
	}
	split(text,fields, splitchar);
	gsub(/ /, "", fields[1]);
	gsub(/ /, "", fields[2]);
	if(fields[1] == "con-path"){
		fieldName = "dbus";
		fieldValue = fields[2];
	}
	else if(fields[1] == "hwaddr"){
		fieldName = "mac";
		fieldValue = fields[2];
	}
	else{
		fieldName = fields[1];
		fieldValue = fields[2];
	}
   line = quote fieldName quote ":" quote fieldValue quote  ",";
	print line;
	}')"
#return vales
#NOTE: Scripts will need to report an error in the form of 
# { 
# 	 "result": "error", 
# 	 "message": "message", 
# 	 "errorObject": null 
# }
#above is a happy path exampleclear
echo $jsonStart\"hostname\":\"$hostname\",${arraybody::-1}$jsonEnd
