#!/bin/bash
#When passing variables via the plugin in controller (settings file) the API's REST data is always first which is often JSON followed by any 
#system or literal, user parameters!
##Json Input = {"serviceName":"name of service"}
servicename="$(echo $1 | grep -Po '"serviceName"[ ]*: *\K"[^"]*"')"
#strip off any leading quotes
tempconn="${servicename%\"}"
servicename="${tempconn#\"}"
file=$2
template=$3
if  test -f "$file" ; then
    sudo sed -i "s/<name.*>.*<\/name>/<name replace-wildcards=\"yes\">$servicename %h<\/name>/g" "$file"
else
    if test -f "$template"; then
        sudo cp "$template" "$file"
        sudo sed -i "s/<name.*>.*<\/name>/<name replace-wildcards=\"yes\">$servicename %h<\/name>/g" "$file"
    fi;
fi;
restart=$(sudo systemctl restart avahi-daemon)
