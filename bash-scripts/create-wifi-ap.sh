#!/bin/bash
jsonText=$1
ssid="$(echo $jsonText | grep -Po '"ssid"[ ]*: *\K"[^"]*"')"
tempssid="${ssid%\"}"
ssid="${tempssid#\"}"

password="$(echo $jsonText | grep -Po '"password"[ ]*: *\K"[^"]*"')"
temppassword="${password%\"}"
password="${temppassword#\"}"

adapter="$(echo $jsonText | grep -Po '"adapter"[ ]*: *\K"[^"]*"')"
tempadapter="${adapter%\"}"
adapter="${tempadapter#\"}"

#get the current list
existing="$(nmcli -t -f name c | grep "$ssid")"
if [[ $ssid == $existing ]]; then
	stealth="$(nmcli c delete "$ssid")"
fi;
nmcli d wifi hotspot ifname $adapter con-name $ssid ssid $ssid password $password