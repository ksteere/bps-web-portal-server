#!/bin/bash
# Example Formatted JSON Data output
# [
#   {
#     "name": "enp2s0",
#     "uuid": "db881938-b609-431b-9a50-a09706774ece",
#     "type": "802-3-ethernet",
#     "device": "enp2s0"
#   },
#   {
#     "name": "poop",
#     "uuid": "76e25c8d-5710-477a-b760-ef6b2346f79b",
#     "type": "802-11-wireless",
#     "device": "wlan0"
#   }
# ]
#set variables for the start and end fo the json object which is an array
jsonStart="["
jsonEnd="]"
#the body of the array set into a variable
arraybody="$(nmcli -t -f  name,uuid,type,device c |
	 awk '{ objectStart = "{";
	objectEnd = "}";
	quote = "\"";	
	json = jsonStart; 
	split($0,fields, ":");
	object  = objectStart quote "nsmr" quote ":" quote fields[1] quote  "," quote "uuid" quote ":" quote fields[2] quote "," quote "type" quote ":" quote fields[3] quote  "," quote "device"  quote ":" quote fields[4] quote objectEnd ; 	
	json = json object ",";
	print json;
	}')"
#return vales
#NOTE: Scripts will need to report an error in the form of 
# { 
# 	 "result": "error", 
# 	 "message": "message", 
# 	 "errorObject": null 
# }
#above is a happy path example
echo $jsonStart ${arraybody::-1} $jsonEnd

