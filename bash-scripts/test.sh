#!/bin/bash
#PATH=/home/scott/bin:/home/scott/.local/bin:/usr/local/sbin:/usr/local/bin:/usr/sbin:/usr/bin:/sbin:/bin:/usr/games:/usr/local/games:/snap/bin
#file $HOME/.profile would eliminate a variable
#When passing variables via the plugin in controller (settings file) the API's REST data is always first which is often JSON followed by any 
#system or literal, user parameters!
# export http_proxy="http://balls.com"
# sudo touch $file
# sudo sed -i 's/http_proxy.*//g' $file
# sudo echo "http_proxy=http://balls.com" >> "$file"
#NOTE: could use awk here...
# if  test -f "$file" ; then
#     sudo sed -i "s/<name>.*<\/name>/<name>$servicename<\/name>/g" $file
# else
#     if test -f "$template"; then
#         sudo cp "$template" "$file"
#         sudo sed -i "s/<name>.*<\/name>/<name>$servicename<\/name>/g" $file
#     fi;
# fi;
#> $file
# servicename=$1
# file=$2
# template=$3
# if  test -f "$file" ; then
#     sudo sed -i "s/<name>.*<\/name>/<name>$servicename<\/name>/g" $file
# else
#     if test -f "$template"; then
#         sudo cp "$template" "$file"
#         sudo sed -i "s/<name>.*<\/name>/<name>$servicename<\/name>/g" $file
#     fi;
# fi;
#file=$1
#name="$(grep -oP "(?<=<name>)(.*?)(?=%h</name>)" "$file")"
#final="$(echo $name | sed -e 's/replace-wildcards.*=.*>//g')"
# echo $final

#exit -1 if we want to report an error
jsonText=$1
adapter=$2
#input json
#'{"ssid":"Orbital","password" :"MercuryVenusEarthMars"}'
$jsonText | jq
ssid=$(echo $jsonText | jq -r .ssid)
password=$(echo $jsonText | jq -r .password)
temp="${ssid%\"}"
ssid="${temp#\"}"
temp="${password%\"}"
password="${temp#\"}"
#"{\"ssid\": \"Orbital\", \"password\": \"MercuryVenusEarthMars\", \"adapter\" : \"wlan0\"}"
