#!/bin/bash
name=`grep "<host-name>" /etc/avahi/services/printer.service | cut -d'>' -f2 | cut -d'<' -f1`
echo "{\"serviceName\" : \"$name\"}"
##Json Output = {"serviceName":"name of service"}
