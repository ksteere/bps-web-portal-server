#!/bin/bash
#get the parameters from the json
connection="$(echo $1 | grep -Po '"connection"[ ]*: *\K"[^"]*"')"
settingname="$(echo $1 | grep -Po '"name"[ ]*: *\K"[^"]*"')"
settingvalue="$(echo $1 | grep -Po '"value"[ ]*: *\K"[^"]*"')"
recycleconnection="$(echo $1 | grep -Po '"recycle"[ ]*: *\K"[^"]*"')"
#strip off any leading quotes
tempconn="${connection%\"}"
tempname="${settingname%\"}"
tempvalue="${settingvalue%\"}"
temprecycle="${recycleconnection%\"}"
#strip off any ending quotes and set the variable to the final value
connection="${tempconn#\"}"
settingname="${tempname#\"}"
settingvalue="${tempvalue#\"}"
recycleconnection="${temprecycle#\"}"
nmcli con modify $connection "$settingname" "$settingvalue"



