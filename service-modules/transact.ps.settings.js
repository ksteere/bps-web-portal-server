/**
 * New node file
 */
//This is a project specific module that will hold all the global settings
//the read and persist methods SHOULD be moved into another module or class and injected - the are the only items that don't properly follow
//the DI or minimum dependency patterns - however the benefit of having in each module is that we can change the implmentations to target other 
//persistant storage or media besides hard disk
var fs = require('fs');
var path = require('path');
var settingsmodule = module.exports = {
		initialized : false,
		http : {port: '1337', host: 'http://localhost'}, 
		websocket : {port : '1338'},
		mode : "production",
		authentication : {username : 'admin', password : 'password', cipher : "none"},
		connections : [
			{type : "wifi",  password : "", adapter : "wlan0", mode : "ap"},
			{type : "wifi",  password : "", adapter : "wlan1", mode :"is"}
		],
		network : {dhcp : true, staticip : null, proxyAddress : "", proxyPort : null},
		sessiontoken : {format : "", expirationDelta : 36000000, evictionInterval : 60000},
		/*the settings below are specific to the Work Stations printing service*/
		system : {model : "BOHA!Workstation", serialNumber : "ZXY0123456789", softwareVersion:"Vxxxxxxxxx", 
		firmwareId : "PE9900-1.54", friendlyName : "BOHA! Test Printer", location : "Don's office", 
		customerPartNumber : "M9800-1234", mfgDate : "12/10/2019", asBuiltConfig : "CFG900_001", rma1 : null, rma2 : null},
		printeroptions : {prtRet : false, wrap : false, calLenP1 : false, calLenP2 : false, eoP1 : 10, eoP2 : 10, tofCalP1 : 0, tofCalP2 : 0, 
		maxLenP1 : 50, maxLenP2 : 50, lenP1 : 0, lenP2 : 0, gapP1 : 1, gapP2 : 1, widthP1 : 0, widthP2 : 0, speedP1 : 1, speedP2 : 1},
		root : {timestamp : Date.now(), version: "1.0", serialport : null, http : null, tunnel : null, authentication : null, 
					connections : null, network : null, sessiontoken : null, system : null, printeroptions:null, restapiplugins : null},
		init(fileName = null){
			if(this.initialized == false){
				this.root.serialport 	= this.serialport;
				this.root.http 			= this.http;
				this.root.tunnel 		= this.tunnel;
				this.root.authentication = this.authentication;
				this.root.connections 	= this.connections;
				this.root.system		= this.system;
				this.root.printeroptions= this.printeroptions;
				this.root.network 		= this.network;
				this.root.sessiontoken 	= this.sessiontoken;
				this.root.restapiplugins = this.restapiplugins;
				this.root.mode 			= this.mode;
				///////////////////////////////////////////////////////////
				if(fileName != null){
					var fileExists = fs.existsSync(fileName);
					let directory =  path.dirname(fileName);
					let syncing = false;
					if(fileExists){
						fs.watch(fileName, { encoding: 'buffer' }, (eventType, filename) => {
							if (filename && eventType == 'change' && !syncing) {
								syncing = true;
								console.log("Reloading settings file...");
								this.readSettingsFile(directory, null);
							  	syncing = false;
							}
						  });
					}
	
				}
			}
			this.initialized = true;
		},
		persistSettingsObject:function(storagelocation, object = null)
		{
			let succeeded = false;
			try{
				var tmplocation = path.normalize(storagelocation);
				var fileLocation = path.format({
					root: '/ignored',
					dir: tmplocation,
					base:"settings.json"
				})
				this.init(fileLocation);
				var directory = path.dirname(fileLocation);
				if (!fs.existsSync(directory)){
					fs.mkdirSync(directory, { recursive: true });
				}
				var tempsettings = null;
				if(object != null){
					tempsettings = object;
				}
				else{
					tempsettings = this.root;
				}
				fs.writeFileSync(fileLocation, JSON.stringify(tempsettings));
				succeeded = true;
			}
			catch(error){
				console.log("An error occurred while writting the settings file. %s", error);
				succeeded = false;
			
			}
			finally{
				return succeeded;
			}
			
		},
		readSettingsFile:function(directory = null, create = false){
			let succeeded = false;
			try
			{
				var tmplocation = path.normalize(directory);
				var fileLocation = path.format({
					root: '/ignored',
					dir: tmplocation,
					base:"settings.json"
				});
				this.init(fileLocation);
				var fileExists = fs.existsSync(fileLocation);
				if(fileExists){
					var data = fs.readFileSync(fileLocation);
					this.root = JSON.parse(data);
					succeeded = true;
				}
				else{
					if(create){
						succeeded = this.persistSettingsObject(directory);
					}
				}
			}
			catch(err)
			{
				console.log("Failed reading settings file " + err + " was returned.");
				succeeded = false;
			}
			finally{
				return succeeded;
			}
		
		}
		
};




