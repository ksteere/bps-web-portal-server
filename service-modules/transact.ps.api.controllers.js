var httputils = require("../lib/transact.http.utilities");
var platformutils = require("../lib/transact.platform.utilities");
const fspath = require('path');
const url = require('url');
var ActiveSessions = [];
module.exports = {
    getActiveSessions(){return ActiveSessions;},
    //Controller/handler for the authentication routes API's
    AuthenticationController: class AuthenticationController{
        constructor(routemgr, settings, logger){
            if(routemgr != null && routemgr != undefined){
                let sessionmanager = routemgr.getSessionManager();
                if(sessionmanager != null){
                    sessionmanager.excludeRoute('/api/v1/user/authenticate', 'POST');
                    sessionmanager.excludeRoute('/api/v1/user/token', 'POST');
                }
                routemgr.setRoute('/api/v1/user/token','POST', function(path, req, res){
                    let body = '';
                    let sessionManager = routemgr.getSessionManager();
                    req.on('data', chunk => {
                        body += chunk.toString(); // convert Buffer to string
                    });
                    req.on('end', () => {
                        if(body != null && body != undefined){
                            var token = body;
                            sessionmanager.isSessionTokenValid(token);
                            logger.LogMessage("Valid token supplied.");
                        }
                        else{
                            logger.LogMessage("Invalid token supplied.");
                            httputils.writeErrorResponse(res, HTTP_RETURN_CODES.NOTAUTHORIZED);
                        }
                        
                
                    });
                });
                routemgr.setRoute('/api/v1/user/authenticate','POST', function(path, req, res){
                    let body = '';
                    let sessionManager = routemgr.getSessionManager();
                    req.on('data', chunk => {
                        body += chunk.toString(); // convert Buffer to string
                    });
                    req.on('end', () => {
                        try{
                            if(body != null && body != undefined){
                                var credentials = JSON.parse(body);
                                if(credentials.username.toUpperCase() == settings.authentication.username.toUpperCase()){
                                    if(credentials.password == settings.authentication.password){
                                        var sessionToken = (sessionManager!= null ? sessionManager.createSession() : "1");
                                        logger.LogMessage("Authentication succedded %s %s", [JSON.stringify(settings.authentication), sessionToken]);
                                        httputils.writeSuccessResponse(res, "application/json", 
                                        JSON.stringify({'username' : settings.authentication.username, 'password' : settings.authentication.password, 
                                        'isAuthenticated' : true, 'sessionToken' : sessionToken}), HTTP_RETURN_CODES.SUCCESS);
                                    }
                                    else{
                                        var error = "Incorrect username or password";
                                        logger.LogMessage();
                                        httputils.writeErrorResponse(res, HTTP_RETURN_CODES.NOTAUTHORIZED, error);
                                    }
                                }
                                else{
                                    var error = "Invalid username supplied";
                                    logger.LogMessage(error);
                                    httputils.writeErrorResponse(res, HTTP_RETURN_CODES.NOTAUTHORIZED, error);
                                }
                            }
                            else{
                                var error = "Invalid credentials supplied.";
                                logger.LogMessage(error);
                                httputils.writeErrorResponse(res, HTTP_RETURN_CODES.NOTAUTHORIZED, error);
                            }
                        }
                        catch(error){
                            logger.LogMessage("An error occurred while authenticating user. %s", [error]);
                            httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                        }
                
                    });
                });
            }
        }

    },
    SettingsController : class SettingsController{
        constructor(routemgr, settings, logger, callback = null){ //we need to really do some session managmement here
            this.settings = settings;
            this.routemgr = routemgr;
            this.logger = logger;
            this.validatechange = callback;
            if(this.routemgr != null && this.routemgr != undefined){
                routemgr.setRoute('/api/v1/settings','GET', function(path, req, res){
                    try{
                        var json = JSON.stringify(settings);
                        httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension('.json'), json, HTTP_RETURN_CODES.SUCCESS);
                    }
                    catch(error){
                        logger.LogMessage("An error occurred while getting settings information. %s", [error]);
                        httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                    }
                });
                routemgr.setRoute('/api/v1/settings','POST', function(path, req, res){
                    try{
                        let body = "";
                        req.on('data', chunk => {
                            body += chunk.toString(); // convert Buffer to string
                        });
                        req.on('end', () => {
                            try{
                                var tempsettings = JSON.parse(body);
                                if(tempsettings != null){
                                    if(callback != null){
                                        var changeok = callback(tempsettings);
                                        if(changeok){
                                            httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension('.json'), json, HTTP_RETURN_CODES.SUCCESS);
                                        }
                                        else{
                                            var error = "An error occurred while saving settings information.";
                                            httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR, error);
                                            logger.LogMessage(error);
                                        }
                                        
                                    }
                                    else{
                                        settings = tempsettings;
                                        httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension('.json'), json, HTTP_RETURN_CODES.SUCCESS);
    
                                    }
                                }
                                else{
                                    var error = "Unable to parse settings information.";
                                    console.log(error);
                                    httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR, error);
                                }
                            }
                            catch(error){
                                logger.LogMessage("An error occurred while setting settings information. %s", [error]);
                                httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                            }
                            
                        });
                        
                    }
                    catch(error){
                        logger.LogMessage("An error occurred while setting settings information. %s", [error]);
                        httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                    }

                });
                routemgr.setRoute('/api/v1/settings/connections','GET', function(path, req, res){
                    try{
                        var json = JSON.stringify(settings.connections);
                        httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension('.json'), json, HTTP_RETURN_CODES.SUCCESS);
                    }
                    catch(error){
                        logger.LogMessage("An error occurred while getting connection information. %s", [error]);
                        httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                    }
                    
                });
                routemgr.setRoute('/api/v1/settings/user/admin/password','POST', function(path, req, res){
                    try{
                        let body = "";
                        req.on('data', chunk => {
                            body += chunk.toString(); // convert Buffer to string
                        });
                        req.on('end', () => {
                            try{
                                var userInfo = JSON.parse(body);
                                if(userInfo.username == settings.authentication.username && userInfo.password == settings.authentication.password){
                                    var tempvalue = settings.authentication.password;
                                    settings.authentication.password = userInfo.newpassword;
                                    if(callback != null){
                                        //validate that the owner says this is ok (i.e. can persist before we save it so we aren't out of sync
                                        //this is probably overkill but it does make for better user experience
                                        var changeok = callback(settings);
                                        if(changeok){
                                            var json = JSON.stringify(userInfo);//for now let's just echo this back - we could just return nothing though {}
                                            httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension('.json'), json, HTTP_RETURN_CODES.SUCCESS);
                                        }
                                        else{
                                            settings.authentication.password = tempvalue; //reset the value
                                            var error = "Saving user information failed."
                                            logger.LogMessage("An error occurred while saving admin user settings information. %s", error);
                                            httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                                           
                                        }
                                        
                                    }
                                    else{
                                        
                                        httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension('.json'), json, HTTP_RETURN_CODES.SUCCESS);
                                    }
                                }
                                else{
                                    var error = "Invalid credentials."
                                    logger.LogMessage("An error occurred while updating admin user information. %s", [error]);
                                    httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR, error); 
                                }
                                
                            }
                            catch(error){
                                logger.LogMessage("An error occurred while updating admin user information. %s", [error]);
                                httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR, error.toString());
                            }
                            
                        });
                    }
                    catch(error){
                        logger.LogMessage("An error occurred while getting user information. %s", [error]);
                        httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR, error.toString());
                    }
                    
                });
               
               
            }   
        }
        
    },
    ConnectivityController : class ConnectivityController{
        constructor(routemgr, sessionmanager, logger){
            this.sessionmanager = sessionmanager;
            this.routemgr = routemgr;
            this.logger = logger;
            if(sessionmanager != null){
                sessionmanager.excludeRoute('/api/v1/connectivity/echo/host/connection', 'GET');
            }
            if(this.routemgr != null && this.routemgr != undefined){
                routemgr.setRoute('/api/v1/connectivity/echo/host/connection','GET', function(path, req, res){
                    //Essentially all we are doing here is echoing the connected IP address of the host.
                    try{
                        var addressObj = req.socket.address();
                        var index = addressObj.address.lastIndexOf(":");
                        var address ="";
                        if(index >= 0){
                            address = addressObj.address.substr(index + 1);
                        }
                        else{
                            address =addressObj.address;
                        }
                        var json = JSON.stringify({address : address, family : addressObj.family, port : addressObj.port});
                        httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension('.json'), json, HTTP_RETURN_CODES.SUCCESS);
                    }
                    catch(error){
                        
                        logger.LogMessage("An error occurred while getting settings client connection information. %s", [error]);
                        httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                    }

                });
            }
        }
    },
    //This controller simplfies the addition and mapping of script or executable plugins from the settigns object/file
    //the settings object. What should be done is instead of the settings file we should pass in the base object that holds the map and map alone
    //so that we can become more generic and reuse this with any object from anywhere. 
    APIAddPluginsController : class APIAddPluginsController{
        //|Name:preProcessDynamicRoute
        //|Summary:initial entry point and the pre processor
        //|this function will do the intial check and validation as well as attach any generic data that is being requested
        //|before complete processing.
        //|-----------------------------
        //|-----------------------------
        //|Parmameters:[]
        //|Return:
        //|Author:
        preProcessDynamicRoute(path, req, res){
            // query: [Object: null prototype] {},
            // pathname: '/api/v1/connectivity/devices',
            // path: '/api/v1/connectivity/devices',
            // href: '/api/v1/connectivity/devices'  
            try{
                var params = [];
                var verb = req.method.toUpperCase();
                if(verb == "GET" || verb == "DELETE"){
                    //we should pull out query string stuff here 
                    var queryObject = url.parse(req.url,true).query;
                    var stringIfied = JSON.stringify(queryObject);
                    if(stringIfied.length > 2 && stringIfied != "{}"){
                        params.push(stringIfied);
                    }
                    this.processDynamicRoute(req, res, params);
                }
                else if (verb == "POST" || verb == "PUT" ){
                    let body = '';
                    req.on('data', chunk => {
                        body += chunk.toString(); // convert Buffer to string
                    });
                    req.on('end', () => {
                        console.log("POST");
                        console.log(body);
                        params.push(body);
                        this.processDynamicRoute(req, res, params);
                    });
                }
                
            }catch(error){
                this.logger.LogMessage("An error occurred while pre-processing %s.", [error]);
                httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
      
            }           
           
        };
        //|Name:processDynamicRoute
        //|Summary:Main generic handler for the api controller
        //|-----------------------------
        //|-----------------------------
        //|-----------------------------
        //|-----------------------------
        //|Parmameters:[req, res, params]
        //|Return:
        //|Author:
        processDynamicRoute(req, res,params){
            try{
                var key = url.parse(req.url,true).pathname;
                var script = this.plugins.map.find(x=>x.uri==key && x.verb==req.method);
                if(script != null){
                    var executablePath = "";
                    if(script.pathType == "relative"){
                        //if we are a relative path use the one that is set in the attribute
                        executablePath = fspath.join(this.plugins.relativeDirectory, script.command);
                    }
                    else{
                        //otherwise assume an absolute path
                        executablePath = script.command;
                    }
                    if(script.params != null && script.params.length > 0){
                        //let's look for variables otherwise we assume they are literals
                        script.params.forEach(element=>{
                            var obj = /(?<=\%)(.*?)(?=\%)/.exec(element);
                            if(obj != null){
                                if(obj[0].toLocaleLowerCase() == "hostip"){
                                    var addressObj = req.socket.address();
                                    var index = addressObj.address.lastIndexOf(":");
                                    var address ="";
                                    if(index >= 0){
                                        address = addressObj.address.substr(index + 1);
                                    }
                                    else{
                                        address =addressObj.address;
                                    }
                                }
                                else{
                                    var temp = this.restdataoffset+obj[0];
                                    var path = temp.replace(/\./g, "/");
                                    var data = this.restdata.getData(path);
                                    if(data != null){
                                        var parsed = JSON.parse(data);
                                        console.log(parsed);
                                        if(parsed.data == typeof "object"){
                                            params.push(JSON.stringify(parsed.data));
                                        }
                                        else{
                                            params.push(parsed.data);
                                        }
                                        
                                    }
                                }
                            }
                            else{
                                //push the literal otherwise
                                params.push(element);
                            }
                        });
                    }
                    platformutils.spwanExecutableScriptOrProcess(executablePath, params).then((result)=>{
                        //Result in the event that is an expected error condition i.e. something not on stderr or the error object from spawn is null
                        //shold be checked for the error object on the client.
                        var json = null;
                        var type = ".json";
                        var objectType = typeof result;
                        try{
                            if(objectType== "string"){ //maybe we should automagically create a json object here {data : "...", type : "string/number/boolean"}
                                if(!httputils.JSONStringToObject(result)){
                                    json = JSON.stringify({response : result, timestamp : Date.now})
                                }
                                else{
                                    json = result;
                                }        
                                
                            }
                            else if(objectType == "number" || objectType == "boolean" || objectType == "bigint"){
                                json = JSON.stringify({value : result.toString(), timestamp : Date.now});
                            }
                            else if(objectType == "object"){
                                json = JSON.stringify(obj);
                            }
                        }
                        catch(error){
                            var error = "An error occurred while processing the command " + script.command + " " + error.stderror;
                            this.logger.LogMessage(error);
                            httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR, error);
                        }
                        finally{
                            httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension(type), json, HTTP_RETURN_CODES.SUCCESS);
                        }
                        
    
                    },(err)=>{
                        //{error : err, stderror : stderr} is returned
                        //if we are here there is likely an error on standard out/error
                        this.logger.LogMessage("An error occurred while processing the command %s %s", [script.command, err.stderror]);
                        httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
                
                    });
                }
            }
            catch(exception){
                this.logger.LogMessage("An exception occurred while executing the function 'processDynamicRoute' - %s", [exception]);
                httputils.writeErrorResponse(res,HTTP_RETURN_CODES.SERVERERROR);
            }
        };
        constructor(routemgr, restdata, restdataoffset, plugins, logger){
            this.logger = logger;
            this.routemgr = routemgr;
            this.restdata = restdata;
            this.plugins = plugins;
            this.restdataoffset = restdataoffset;
            if(this.routemgr != null && this.routemgr != undefined){
                this.plugins.map.forEach(element=>{
                    this.routemgr.setRoute(element.uri, element.verb, (path, req, res)=>{ 
                        //I screwed up this should all be static and a singleton or declared inline with local vars pushed into an anonymous call back
                        //like the above classes. There really should only be one controller in each project - we can fix this later though but for now
                        //let's get a little clever and provide acces to global scope - otherwise => undefined callback hell. ES6 standard.  
                        this.preProcessDynamicRoute(path, req, res);
                    });
                });


            }
        }
    },
    //The test controller is really intended for quick command line testing and validating of the API's - it mocks the HTTP process and in doing so
    //allows HTTP API calls to be executed simply on the command line. It essentially uses routemanager to perform the tasks and as a result could
    //be viewed as generic and thus should be structured under lib in a generic container. We will do that later. By default the test controller 
    //adds a fake session key but COULD simply tell
    //route manager
    TestController : class TestController{
        //|Name: testAPI
        //|Summary:Used to test API from the console or command line
        //|and other automated testing process
        //|-----------------------------
        //|-----------------------------
        //|-----------------------------
        //|Parmameters:[path, verb, data]
        //|Return: nothing
        //|Author:
        testAPI(path, verb = 'GET', verbose = true, data = null){
            var parsedurl = url.parse(path, true).pathname;
            var sessionKey =  "3735928559"; //For those curious folks out there; hint - this is an unsigned integer...but don't let Peta see it!
            var req = {url : path, method : verb, on :function(operation, callback){
                if(operation == 'data'){
                    callback(data);
                }
                else if(operation == 'end'){
                    callback();
                }
                
            }, headers : {authorization :sessionKey}};
            var res = {writeHead : function(data){console.log(data);}, end : function(data){
                if(verbose){
                    if(typeof data == "object"){
                        console.log(JSON.stringify(data));
                    }
                    else{
                        console.log(data);
                    }
                }
            }};
            if(this.sessionmanager != null){
                this.sessionmanager.kickSessionKey(sessionKey);
            }
            if(this.httpserver != null){
                this.httpserver.httpHandler(req, res);
            }
            
        };
        constructor(httpserver,sessionmanager, logger = null){
            this.sessionmanager = sessionmanager;
            this.logger = logger;
            this.httpserver = httpserver;
            
        };
    },
    
   
}; 