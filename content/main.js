(window["webpackJsonp"] = window["webpackJsonp"] || []).push([["main"],{

/***/ "./src/$$_lazy_route_resource lazy recursive":
/*!**********************************************************!*\
  !*** ./src/$$_lazy_route_resource lazy namespace object ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

function webpackEmptyAsyncContext(req) {
	// Here Promise.resolve().then() is used instead of new Promise() to prevent
	// uncaught exception popping up in devtools
	return Promise.resolve().then(function() {
		var e = new Error('Cannot find module "' + req + '".');
		e.code = 'MODULE_NOT_FOUND';
		throw e;
	});
}
webpackEmptyAsyncContext.keys = function() { return []; };
webpackEmptyAsyncContext.resolve = webpackEmptyAsyncContext;
module.exports = webpackEmptyAsyncContext;
webpackEmptyAsyncContext.id = "./src/$$_lazy_route_resource lazy recursive";

/***/ }),

/***/ "./src/app/admin/admin.component.css":
/*!*******************************************!*\
  !*** ./src/app/admin/admin.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "\r\n"

/***/ }),

/***/ "./src/app/admin/admin.component.html":
/*!********************************************!*\
  !*** ./src/app/admin/admin.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\r\n    text-align: center;\r\n    margin-top: 10px;\r\n    border: 1px solid var(--boha-neutral-gray-medium);\r\n    border-radius: 5px;\r\n    min-height: 300px;\r\n    width: 100%;\r\n\r\n    h3\r\n    font-size: 1.75rem;\r\n    /* background-color: var(--boha-primary-light-blue); */\r\n    border-radius: 5px 5px 0 0;\r\n    border-bottom: 1px solid var(--boha-neutral-gray-medium);\r\n    padding: 5px;\r\n\r\n-->\r\n<div class=\"boha-child-page\">\r\n    <pop-up-message>\r\n    </pop-up-message>\r\n    <div class=\"boha-info-panel form-group\" >\r\n        <div class=\"boha-info-panel-input-container\" style=\"height: 285px\">\r\n            <div class=\"row boha-info-panel-header-row\">\r\n                <div class=\"col\" >\r\n                    Admin User (password)\r\n                </div>\r\n\r\n            </div>\r\n            <div class=\"row boha-info-panel-input-row\">\r\n                <div class=\"col-4 boha-info-panel-label-col\"> \r\n                    User Name:\r\n                </div>\r\n                <div class=\"col-8\">\r\n                    <!--<div class=\"col\">\r\n                        <input type=\"text\" placeholder=\"User Name\" \r\n                        aria-label=\"User Name\" class=\"form-control\" [(ngModel)]=\"userName\"/>\r\n                    </div>-->\r\n                    <span >{{userName}}</span>\r\n                </div>\r\n            </div>\r\n            <div class=\"row boha-info-panel-input-row\">\r\n                <div class=\"col-4 boha-info-panel-label-col\"> \r\n                Current Password:\r\n                </div>\r\n                <div class=\"col-8\">\r\n                    <input type=\"password\" placeholder=\"Enter Current Password\" \r\n                    aria-label=\"Password\" class=\"form-control\" [(ngModel)]=\"currentPassword\" />\r\n                </div>\r\n            </div>\r\n            <div class=\"row boha-info-panel-input-row\">\r\n                <div class=\"col-4 boha-info-panel-label-col\"> \r\n                New Password:\r\n                </div>\r\n                <div class=\"col-8\">\r\n                    <input type=\"password\" placeholder=\"Enter Password\" \r\n                    aria-label=\"Password\" class=\"form-control\" [(ngModel)]=\"userPassword1\" (keydown)=\"userPasswordKeyEvent($event)\"/>\r\n                </div>\r\n            </div>\r\n            <div class=\"row boha-info-panel-input-row\">\r\n                <div class=\"col-4 boha-info-panel-label-col\"> \r\n                    Renter New Password:\r\n                </div>\r\n                <div class=\"col-8\">\r\n                    <input type=\"password\" placeholder=\"Renter Password\" \r\n                    aria-label=\"Reneter\" class=\"form-control\" [(ngModel)]=\"userPassword2\" (keydown)=\"userPasswordKeyEvent($event)\"/>\r\n                </div>\r\n            </div>\r\n            <div class=\"row\" style=\"line-height: 55px; height: 55px;\">\r\n                <div class=\"col\" style=\"margin-top: 10px;\">\r\n                    <button class=\"btn boha-btn-primary\"  type=\"submit\" style=\"float: right;min-width: 100px;\" \r\n                    (click)=\"saveUserInformation()\" [disabled]=\"!userInfoTouched\"><i class=\"fad fa-save\"></i>&nbsp;Save</button>\r\n                </div>\r\n            </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/admin/admin.component.ts":
/*!******************************************!*\
  !*** ./src/app/admin/admin.component.ts ***!
  \******************************************/
/*! exports provided: AdminComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AdminComponent", function() { return AdminComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/services/page.header.service */ "./src/app/common/services/page.header.service.ts");
/* harmony import */ var _common_services_settings_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/services/settings.service */ "./src/app/common/services/settings.service.ts");
/* harmony import */ var _common_data_settings_user_information__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/data/settings.user.information */ "./src/app/common/data/settings.user.information.ts");
/* harmony import */ var _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/ui-widgets/pop-up-message/pop-up-message.component */ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AdminComponent = /** @class */ (function () {
    function AdminComponent(pageHeaderService, settingsService) {
        this.pageHeaderService = pageHeaderService;
        this.settingsService = settingsService;
        this.userPassword1 = "";
        this.userPassword2 = "";
        this.currentPassword = "";
        this.userName = "admin";
        this.userInfoTouched = false;
    }
    AdminComponent.prototype.ngOnInit = function () {
    };
    AdminComponent.prototype.ngAfterViewInit = function () {
        console.log("Admin view init....");
        this.pageHeaderService.setHeaderTitle("User Administration", "fa-user");
    };
    AdminComponent.prototype.ngDestroy = function () {
    };
    AdminComponent.prototype.clearUserCredentials = function () {
        this.currentPassword = "";
        this.userPassword1 = "";
        this.userPassword2 = "";
    };
    AdminComponent.prototype.saveUserInformation = function () {
        var _this = this;
        var userInfo = new _common_data_settings_user_information__WEBPACK_IMPORTED_MODULE_3__["UserInformation"]();
        userInfo.newpassword = this.userPassword1.toString();
        userInfo.password = this.currentPassword.toString();
        userInfo.username = this.userName.toString();
        if (this.userPassword1 == this.userPassword2) {
            this.settingsService.adminPassword(userInfo).subscribe(function (resp) {
                console.log(resp);
                _this.popup.displayMessage(true, "Successfully updated user information.");
                _this.clearUserCredentials();
                _this.userInfoTouched = false;
            }, function (err) {
                _this.popup.displayMessage(false, "An error occured while attempting to update the user. " + err.error);
            });
        }
        else {
            this.popup.displayMessage(false, "Passwords must match. Please verify.");
        }
    };
    AdminComponent.prototype.userPasswordKeyEvent = function (event) {
        this.userInfoTouched = true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_4__["PopUpMessageComponent"]),
        __metadata("design:type", _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_4__["PopUpMessageComponent"])
    ], AdminComponent.prototype, "popup", void 0);
    AdminComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-admin',
            template: __webpack_require__(/*! ./admin.component.html */ "./src/app/admin/admin.component.html"),
            styles: [__webpack_require__(/*! ./admin.component.css */ "./src/app/admin/admin.component.css")],
        }),
        __metadata("design:paramtypes", [_common_services_page_header_service__WEBPACK_IMPORTED_MODULE_1__["PageHeaderService"], _common_services_settings_service__WEBPACK_IMPORTED_MODULE_2__["SettingsService"]])
    ], AdminComponent);
    return AdminComponent;
}());



/***/ }),

/***/ "./src/app/app-routing.module.ts":
/*!***************************************!*\
  !*** ./src/app/app-routing.module.ts ***!
  \***************************************/
/*! exports provided: AppRoutingModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppRoutingModule", function() { return AppRoutingModule; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _connectivity_connectivity_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./connectivity/connectivity.component */ "./src/app/connectivity/connectivity.component.ts");
/* harmony import */ var _common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./common/services/route.guard.service */ "./src/app/common/services/route.guard.service.ts");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _system_system_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./system/system.component */ "./src/app/system/system.component.ts");
/* harmony import */ var _avahi_avahi_component__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./avahi/avahi.component */ "./src/app/avahi/avahi.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};









var routes = [
    {
        path: "home",
        canActivate: [_common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_5__["RouteGuardService"]],
        //loadChildren: "./home-component/home.module#HomeModule",
        component: _home_home_component__WEBPACK_IMPORTED_MODULE_2__["HomeComponent"],
        data: { pageTitle: "Home", animation: "Avahi" }
    },
    {
        path: 'login',
        component: _login_login_component__WEBPACK_IMPORTED_MODULE_3__["LoginComponent"],
        data: { pageTitle: "Login", animation: "Login" }
    },
    {
        canActivate: [_common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_5__["RouteGuardService"]],
        path: 'connect',
        component: _connectivity_connectivity_component__WEBPACK_IMPORTED_MODULE_4__["ConnectivityComponent"],
        data: { pageTitle: "Connectivity", animation: "Connectivity" }
    },
    {
        canActivate: [_common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_5__["RouteGuardService"]],
        path: 'admin',
        component: _admin_admin_component__WEBPACK_IMPORTED_MODULE_6__["AdminComponent"],
        data: { pageTitle: "Administration", animation: "Administration" }
    },
    {
        canActivate: [_common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_5__["RouteGuardService"]],
        path: 'system',
        component: _system_system_component__WEBPACK_IMPORTED_MODULE_7__["SystemComponent"],
        data: { pageTitle: "System", animation: "System" }
    },
    {
        canActivate: [_common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_5__["RouteGuardService"]],
        path: 'avahi',
        component: _avahi_avahi_component__WEBPACK_IMPORTED_MODULE_8__["AvahiComponent"],
        data: { pageTitle: "Avahi", animation: "Avahi" }
    },
    {
        path: '',
        redirectTo: '/home',
        pathMatch: 'full'
    }
];
var AppRoutingModule = /** @class */ (function () {
    function AppRoutingModule() {
    }
    AppRoutingModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["NgModule"])({
            imports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"].forRoot(routes, { useHash: true })],
            exports: [_angular_router__WEBPACK_IMPORTED_MODULE_1__["RouterModule"]]
        })
    ], AppRoutingModule);
    return AppRoutingModule;
}());



/***/ }),

/***/ "./src/app/app.component.css":
/*!***********************************!*\
  !*** ./src/app/app.component.css ***!
  \***********************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/app.component.html":
/*!************************************!*\
  !*** ./src/app/app.component.html ***!
  \************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--The content below is only a placeholder and can be replaced.-->\r\n<!--<router-outlet></router-outlet>-->\r\n<div [@routeAnimations]=\"o && o.activatedRouteData \r\n      && o.activatedRouteData['animation']\">\r\n    <router-outlet #o=\"outlet\"></router-outlet>\r\n</div>"

/***/ }),

/***/ "./src/app/app.component.ts":
/*!**********************************!*\
  !*** ./src/app/app.component.ts ***!
  \**********************************/
/*! exports provided: AppComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppComponent", function() { return AppComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_services_route_animation__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./common/services/route-animation */ "./src/app/common/services/route-animation.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var AppComponent = /** @class */ (function () {
    function AppComponent() {
        this.title = 'BOHA! Workstation';
    }
    AppComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-root',
            template: __webpack_require__(/*! ./app.component.html */ "./src/app/app.component.html"),
            styles: [__webpack_require__(/*! ./app.component.css */ "./src/app/app.component.css")],
            animations: [_common_services_route_animation__WEBPACK_IMPORTED_MODULE_1__["fadeTransition"]]
        }),
        __metadata("design:paramtypes", [])
    ], AppComponent);
    return AppComponent;
}());



/***/ }),

/***/ "./src/app/app.module.ts":
/*!*******************************!*\
  !*** ./src/app/app.module.ts ***!
  \*******************************/
/*! exports provided: AppModule */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AppModule", function() { return AppModule; });
/* harmony import */ var _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/platform-browser */ "./node_modules/@angular/platform-browser/fesm5/platform-browser.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var _app_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./app.component */ "./src/app/app.component.ts");
/* harmony import */ var _home_home_component__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ./home/home.component */ "./src/app/home/home.component.ts");
/* harmony import */ var _app_routing_module__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ./app-routing.module */ "./src/app/app-routing.module.ts");
/* harmony import */ var _connectivity_connectivity_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ./connectivity/connectivity.component */ "./src/app/connectivity/connectivity.component.ts");
/* harmony import */ var _login_login_component__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ./login/login.component */ "./src/app/login/login.component.ts");
/* harmony import */ var _common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ./common/services/route.guard.service */ "./src/app/common/services/route.guard.service.ts");
/* harmony import */ var _angular_forms__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! @angular/forms */ "./node_modules/@angular/forms/fesm5/forms.js");
/* harmony import */ var _common_services_session_token_intercepter_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ./common/services/session.token.intercepter.service */ "./src/app/common/services/session.token.intercepter.service.ts");
/* harmony import */ var _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__ = __webpack_require__(/*! @angular/platform-browser/animations */ "./node_modules/@angular/platform-browser/fesm5/animations.js");
/* harmony import */ var _admin_admin_component__WEBPACK_IMPORTED_MODULE_12__ = __webpack_require__(/*! ./admin/admin.component */ "./src/app/admin/admin.component.ts");
/* harmony import */ var _system_system_component__WEBPACK_IMPORTED_MODULE_13__ = __webpack_require__(/*! ./system/system.component */ "./src/app/system/system.component.ts");
/* harmony import */ var _avahi_avahi_component__WEBPACK_IMPORTED_MODULE_14__ = __webpack_require__(/*! ./avahi/avahi.component */ "./src/app/avahi/avahi.component.ts");
/* harmony import */ var _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_15__ = __webpack_require__(/*! ./common/ui-widgets/pop-up-message/pop-up-message.component */ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts");
/* harmony import */ var _common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_16__ = __webpack_require__(/*! ./common/ui-widgets/modal-confirmation/modal-confirmation.component */ "./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

















var AppModule = /** @class */ (function () {
    function AppModule(routeGuard) {
        this.routeGuard = routeGuard;
    }
    AppModule.prototype.ngInit = function () {
    };
    AppModule = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["NgModule"])({
            declarations: [
                _app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"],
                _home_home_component__WEBPACK_IMPORTED_MODULE_4__["HomeComponent"],
                _connectivity_connectivity_component__WEBPACK_IMPORTED_MODULE_6__["ConnectivityComponent"],
                _login_login_component__WEBPACK_IMPORTED_MODULE_7__["LoginComponent"],
                _admin_admin_component__WEBPACK_IMPORTED_MODULE_12__["AdminComponent"],
                _system_system_component__WEBPACK_IMPORTED_MODULE_13__["SystemComponent"],
                _avahi_avahi_component__WEBPACK_IMPORTED_MODULE_14__["AvahiComponent"],
                _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_15__["PopUpMessageComponent"],
                _common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_16__["ModalConfirmationComponent"]
            ],
            imports: [
                _angular_platform_browser__WEBPACK_IMPORTED_MODULE_0__["BrowserModule"],
                _app_routing_module__WEBPACK_IMPORTED_MODULE_5__["AppRoutingModule"],
                _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HttpClientModule"],
                _angular_forms__WEBPACK_IMPORTED_MODULE_9__["FormsModule"],
                _angular_platform_browser_animations__WEBPACK_IMPORTED_MODULE_11__["BrowserAnimationsModule"]
            ],
            providers: [_common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_8__["RouteGuardService"],
                {
                    provide: _angular_common_http__WEBPACK_IMPORTED_MODULE_2__["HTTP_INTERCEPTORS"],
                    useClass: _common_services_session_token_intercepter_service__WEBPACK_IMPORTED_MODULE_10__["TokenInterceptor"],
                    multi: true
                },
                _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_15__["PopUpMessageComponent"]
            ],
            bootstrap: [_app_component__WEBPACK_IMPORTED_MODULE_3__["AppComponent"]]
        }),
        __metadata("design:paramtypes", [_common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_8__["RouteGuardService"]])
    ], AppModule);
    return AppModule;
}());



/***/ }),

/***/ "./src/app/avahi/avahi.component.css":
/*!*******************************************!*\
  !*** ./src/app/avahi/avahi.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/avahi/avahi.component.html":
/*!********************************************!*\
  !*** ./src/app/avahi/avahi.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\r\n    text-align: center;\r\n    margin-top: 10px;\r\n    border: 1px solid var(--boha-neutral-gray-medium);\r\n    border-radius: 5px;\r\n    min-height: 300px;\r\n    width: 100%;\r\n\r\n    h3\r\n    font-size: 1.75rem;\r\n    /* background-color: var(--boha-primary-light-blue); */\r\n    border-radius: 5px 5px 0 0;\r\n    border-bottom: 1px solid var(--boha-neutral-gray-medium);\r\n    padding: 5px;\r\n\r\n-->\r\n<div class=\"boha-child-page\">\r\n  <pop-up-message>\r\n  </pop-up-message>\r\n  <div class=\"boha-info-panel form-group\" >\r\n      <div class=\"boha-info-panel-input-container\">\r\n          <div class=\"row\" class=\"boha-info-panel-header-row\">\r\n              <div class=\"col\" style=\"padding: 0px;\">\r\n                  Avahi service configuration\r\n              </div>\r\n        </div>\r\n          <div class=\"row boha-info-panel-input-row\">\r\n              <div class=\"col-3 boha-info-panel-label-col\"> \r\n              Service Name:\r\n              </div>\r\n              <div class=\"col-9\">\r\n                    <input type=\"text\" placeholder=\"Enter Avahi Service Name\" \r\n                    aria-label=\"Avahi Service Label\" class=\"form-control\" [(ngModel)]=\"avahiServiceName\" \r\n                    (keydown)=\"avahiServiceNameKeyEvent($event)\"/>\r\n              </div>\r\n          </div>\r\n          <div class=\"row\" style=\"line-height: 55px; height: 55px;\">\r\n              <div class=\"col\" style=\"margin-top: 10px;\">\r\n                  <button class=\"btn boha-btn-primary\" type=\"submit\"  style=\"float: right; min-width: 100px;\" \r\n                  (click)=\"saveAvahiServiceInformation()\" [disabled]=\"!avahiSettingsTouched\"><i class=\"fad fa-save\"></i>&nbsp;Save</button>\r\n              </div>\r\n          </div>\r\n      </div>\r\n  </div>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/avahi/avahi.component.ts":
/*!******************************************!*\
  !*** ./src/app/avahi/avahi.component.ts ***!
  \******************************************/
/*! exports provided: AvahiComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvahiComponent", function() { return AvahiComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/services/page.header.service */ "./src/app/common/services/page.header.service.ts");
/* harmony import */ var _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/ui-widgets/pop-up-message/pop-up-message.component */ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts");
/* harmony import */ var _common_services_system_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/services/system.service */ "./src/app/common/services/system.service.ts");
/* harmony import */ var _common_data_avahi_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/data/avahi.service */ "./src/app/common/data/avahi.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};





var AvahiComponent = /** @class */ (function () {
    function AvahiComponent(pageHeaderService, systemService) {
        this.pageHeaderService = pageHeaderService;
        this.systemService = systemService;
        this.avahiServiceName = "Avahi Service";
        this.avahiSettingsTouched = false;
    }
    AvahiComponent.prototype.ngOnInit = function () {
        this.getAvahiServicInformation();
    };
    AvahiComponent.prototype.ngAfterViewInit = function () {
        console.log("Avahi view init....");
        this.pageHeaderService.setHeaderTitle("Avahi Configuration", "fa-magic");
    };
    AvahiComponent.prototype.ngDestroy = function () {
    };
    AvahiComponent.prototype.getAvahiServicInformation = function () {
        var _this = this;
        this.systemService.getAvahiName().subscribe(function (resp) {
            _this.avahiServiceName = resp.serviceName.trim(); //just in case we get some whitespace by mistake
        }, function (err) {
            var error = "An error occured while retieving the avahi service information. " + err.error;
            _this.popup.displayMessage(false, error);
            console.log(error);
        });
    };
    AvahiComponent.prototype.test = function () {
        console.log("this is a test");
    };
    AvahiComponent.prototype.saveAvahiServiceInformation = function () {
        var _this = this;
        console.log("saveAvahiServiceInformation");
        var serviceData = new _common_data_avahi_service__WEBPACK_IMPORTED_MODULE_4__["AvahiService"]();
        serviceData.serviceName = this.avahiServiceName;
        if (this.avahiServiceName.length > 0) {
            this.systemService.setAvahiName(serviceData).subscribe(function (resp) {
                console.log(resp);
                _this.popup.displayMessage(true, "Successfully updated avahi service name");
                _this.avahiSettingsTouched = false;
            }, function (err) {
                console.log("saveAvahiServiceInformation error A");
                var error = "An error occured while retieving the avahi service information. " + err.error;
                _this.popup.displayMessage(false, error);
                console.log(error);
            });
        }
        else {
            console.log("saveAvahiServiceInformation error B");
            var error = "Service name can not be blank. Please provide a valid name.";
            this.popup.displayMessage(false, error);
            console.log(error);
        }
    };
    AvahiComponent.prototype.avahiServiceNameKeyEvent = function (event) {
        this.avahiSettingsTouched = true;
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_2__["PopUpMessageComponent"]),
        __metadata("design:type", _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_2__["PopUpMessageComponent"])
    ], AvahiComponent.prototype, "popup", void 0);
    AvahiComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-avahi',
            template: __webpack_require__(/*! ./avahi.component.html */ "./src/app/avahi/avahi.component.html"),
            styles: [__webpack_require__(/*! ./avahi.component.css */ "./src/app/avahi/avahi.component.css")]
        }),
        __metadata("design:paramtypes", [_common_services_page_header_service__WEBPACK_IMPORTED_MODULE_1__["PageHeaderService"], _common_services_system_service__WEBPACK_IMPORTED_MODULE_3__["SystemService"]])
    ], AvahiComponent);
    return AvahiComponent;
}());



/***/ }),

/***/ "./src/app/common/data/avahi.service.ts":
/*!**********************************************!*\
  !*** ./src/app/common/data/avahi.service.ts ***!
  \**********************************************/
/*! exports provided: AvahiService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AvahiService", function() { return AvahiService; });
var AvahiService = /** @class */ (function () {
    function AvahiService() {
    }
    return AvahiService;
}());



/***/ }),

/***/ "./src/app/common/data/connection.details.ts":
/*!***************************************************!*\
  !*** ./src/app/common/data/connection.details.ts ***!
  \***************************************************/
/*! exports provided: ConnectionDetails */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionDetails", function() { return ConnectionDetails; });
var ConnectionDetails = /** @class */ (function () {
    function ConnectionDetails() {
    }
    return ConnectionDetails;
}());



/***/ }),

/***/ "./src/app/common/data/connection.info.ts":
/*!************************************************!*\
  !*** ./src/app/common/data/connection.info.ts ***!
  \************************************************/
/*! exports provided: ConnectionInfo */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectionInfo", function() { return ConnectionInfo; });
var ConnectionInfo = /** @class */ (function () {
    function ConnectionInfo() {
    }
    return ConnectionInfo;
}());



/***/ }),

/***/ "./src/app/common/data/http.proxy.ts":
/*!*******************************************!*\
  !*** ./src/app/common/data/http.proxy.ts ***!
  \*******************************************/
/*! exports provided: HttProxy */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HttProxy", function() { return HttProxy; });
var HttProxy = /** @class */ (function () {
    function HttProxy() {
    }
    return HttProxy;
}());



/***/ }),

/***/ "./src/app/common/data/ip.connection.method.ts":
/*!*****************************************************!*\
  !*** ./src/app/common/data/ip.connection.method.ts ***!
  \*****************************************************/
/*! exports provided: IPConnectionMethod */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "IPConnectionMethod", function() { return IPConnectionMethod; });
var IPConnectionMethod = /** @class */ (function () {
    function IPConnectionMethod() {
    }
    return IPConnectionMethod;
}());



/***/ }),

/***/ "./src/app/common/data/settings.user.information.ts":
/*!**********************************************************!*\
  !*** ./src/app/common/data/settings.user.information.ts ***!
  \**********************************************************/
/*! exports provided: UserInformation */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserInformation", function() { return UserInformation; });
var UserInformation = /** @class */ (function () {
    function UserInformation() {
    }
    return UserInformation;
}());



/***/ }),

/***/ "./src/app/common/data/user.credentials.ts":
/*!*************************************************!*\
  !*** ./src/app/common/data/user.credentials.ts ***!
  \*************************************************/
/*! exports provided: UserCredentials */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "UserCredentials", function() { return UserCredentials; });
var UserCredentials = /** @class */ (function () {
    function UserCredentials() {
    }
    return UserCredentials;
}());



/***/ }),

/***/ "./src/app/common/data/wifi.credentials..ts":
/*!**************************************************!*\
  !*** ./src/app/common/data/wifi.credentials..ts ***!
  \**************************************************/
/*! exports provided: WifiCredentials */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "WifiCredentials", function() { return WifiCredentials; });
var WifiCredentials = /** @class */ (function () {
    function WifiCredentials() {
    }
    return WifiCredentials;
}());



/***/ }),

/***/ "./src/app/common/services/auth.service.ts":
/*!*************************************************!*\
  !*** ./src/app/common/services/auth.service.ts ***!
  \*************************************************/
/*! exports provided: AuthService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "AuthService", function() { return AuthService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var AuthService = /** @class */ (function () {
    function AuthService(http) {
        this.http = http;
        this.baseUrl = "/api/v1/user/"; ///login?userName=bella&password=bella";  
        this.httpHeaders = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Accept': 'application/json'
            })
        };
    }
    AuthService.prototype.authenticate = function (credentials) {
        var url = this.baseUrl + "authenticate";
        console.log(url);
        return this.http.post(url, credentials, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    AuthService.prototype.verifyToken = function (token) {
        var url = this.baseUrl + "token";
        console.log(url);
        return this.http.post(url, token, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    AuthService.prototype.handleError = function (error) {
        console.log(error);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
    };
    AuthService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], AuthService);
    return AuthService;
}());



/***/ }),

/***/ "./src/app/common/services/connectivity.service.ts":
/*!*********************************************************!*\
  !*** ./src/app/common/services/connectivity.service.ts ***!
  \*********************************************************/
/*! exports provided: ConnectivityService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectivityService", function() { return ConnectivityService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var ConnectivityService = /** @class */ (function () {
    function ConnectivityService(http) {
        this.http = http;
        this.baseUrl = "/api/v1/connectivity/";
        this.httpHeaders = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Accept': 'application/json',
            })
        };
    }
    ConnectivityService.prototype.uplinkTest = function (site) {
        var url = site; //"https://cors-anywhere.herokuapp.com/" + site;
        return this.http.get(url, { headers: { 'Accept': 'text/html', 'x-Requested-With': 'XMLHttpRequest' }, responseType: "text" }).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.networks = function (adapter) {
        var url = this.baseUrl + "wifinetworks?adapter=" + adapter;
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.connectionDetails = function (connection) {
        var url = this.baseUrl + "connection/details?name=" + connection;
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.connectionDelete = function (connection) {
        var url = this.baseUrl + "connection/delete";
        return this.http.post(url, connection, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.devices = function () {
        var url = this.baseUrl + "devices";
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.connections = function () {
        var url = this.baseUrl + "connections";
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.deviceDetails = function (device) {
        var url = this.baseUrl + "device/details?device=" + device;
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.connectWifi = function (credentials) {
        var url = this.baseUrl + "wifi/connect";
        return this.http.post(url, credentials, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.hostConnectionEcho = function () {
        var url = this.baseUrl + "echo/host/connection";
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.apWifiConnection = function (credentials) {
        var url = this.baseUrl + "ap/connect";
        return this.http.post(url, credentials, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.setConnectionSetting = function (setting) {
        var url = this.baseUrl + "connection/setting";
        return this.http.post(url, setting, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ConnectivityService.prototype.setConnectionMethod = function (setting) {
        var url = this.baseUrl + "connection/ip/method";
        return this.http.post(url, setting, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ////////////////////////////ASYNC///////////////////////////////////
    ////////////////////////////////////////////////////////////////////
    ConnectivityService.prototype.setConnectionSetting_async = function (setting) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.setConnectionSetting(setting).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.setConnectionMethod_async = function (setting) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.setConnectionMethod(setting).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.apWifiConnection_async = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.apWifiConnection(credentials).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.connectWifi_async = function (credentials) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connectWifi(credentials).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.networks_async = function (adapter) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.networks(adapter).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.hostConnectionEcho_async = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.hostConnectionEcho().subscribe(function (resp) {
                resolve(resp.address);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.devices_async = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.devices().subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.connections_async = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connections().subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.deviceDetails_async = function (device) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.deviceDetails(device).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.connectionDelete_async = function (connection) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connectionDelete(connection).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.connectionDetails_async = function (connection) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connectionDetails(connection).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    ConnectivityService.prototype.handleError = function (error) {
        console.log(error);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
    };
    ConnectivityService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], ConnectivityService);
    return ConnectivityService;
}());



/***/ }),

/***/ "./src/app/common/services/global-helper-functions.ts":
/*!************************************************************!*\
  !*** ./src/app/common/services/global-helper-functions.ts ***!
  \************************************************************/
/*! exports provided: removeClass, addClass, parsePrimaryIp, isStringEmpty, isNullOrUndefined, isValidOctet, validateIPRegEx, validateIPAddress, getSubnetFromMask, getMaskFromSubnetDot, getFormattedErrorMessage, assembleProxyAddress, getProxyParts */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "removeClass", function() { return removeClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "addClass", function() { return addClass; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "parsePrimaryIp", function() { return parsePrimaryIp; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isStringEmpty", function() { return isStringEmpty; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isNullOrUndefined", function() { return isNullOrUndefined; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "isValidOctet", function() { return isValidOctet; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateIPRegEx", function() { return validateIPRegEx; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "validateIPAddress", function() { return validateIPAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getSubnetFromMask", function() { return getSubnetFromMask; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getMaskFromSubnetDot", function() { return getMaskFromSubnetDot; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getFormattedErrorMessage", function() { return getFormattedErrorMessage; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "assembleProxyAddress", function() { return assembleProxyAddress; });
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "getProxyParts", function() { return getProxyParts; });
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");

function removeClass(element, className) {
    if (element != null) {
        var tempclasses = element.className.replace(className);
        if (tempclasses != null && tempclasses != undefined) {
            element.className = tempclasses;
        }
    }
}
function addClass(element, className) {
    if (element != null) {
        element.className += " " + className;
    }
}
function parsePrimaryIp(ipAddress, network) {
    if (network === void 0) { network = true; }
    if (ipAddress != null && ipAddress != undefined) {
        var parts = ipAddress.split("/");
        if (parts != null && parts.length == 2) {
            if (network) {
                return parts[0];
            }
            else {
                return parts[1];
            }
        }
        else {
            return ipAddress;
        }
    }
    else {
        return null;
    }
}
function isStringEmpty(value) {
    if (!isNullOrUndefined(value)) {
        return value.length <= 0;
    }
    else {
        return true;
    }
}
function isNullOrUndefined(object) {
    var sanityCheck = (object == undefined || object == null);
    return sanityCheck;
}
function isValidOctet(octet) {
    console.log(octet);
    return (octet != NaN && octet <= 255);
}
function validateIPRegEx(ip) {
    var first3BytesRg = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))$/;
    var fourthByteRg = /^(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    var octets = ip.split(".");
    var nb = octets.length;
    if (nb > 4)
        return false;
    if (octets[nb - 2] == "")
        return false;
    if (octets[nb - 1] == "")
        return true;
    if (nb < 4) {
        return first3BytesRg.test(octets[nb - 1]);
    }
    return fourthByteRg.test(octets[nb - 1]);
}
function validateIPAddress(address) {
    var addressParts = address.split(".");
    var isOK = true;
    if (addressParts.length == 4) {
        var part1 = Number.parseInt(addressParts[0]);
        var part2 = Number.parseInt(addressParts[1]);
        var part3 = Number.parseInt(addressParts[2]);
        var part4 = Number.parseInt(addressParts[3]);
        isOK = isValidOctet(part1) && isValidOctet(part2) &&
            isValidOctet(part3) && isValidOctet(part4);
    }
    else if (address == "") {
        isOK = true;
    }
    else {
        isOK = false;
    }
    return isOK;
}
function getSubnetFromMask(address) {
    var addressParts = address.split(".");
    if (addressParts.length == 4) {
        var part1 = Number.parseInt(addressParts[0]);
        var part2 = Number.parseInt(addressParts[1]);
        var part3 = Number.parseInt(addressParts[2]);
        var part4 = Number.parseInt(addressParts[3]);
        if (part1 == 255 && part2 == 255 && isValidOctet(part3) && isValidOctet(part4)) {
            var addressRange = (256 - part3) * (256 - part4);
            var dot = Math.round(32 - Math.log2(addressRange)); //we could do more checking both with respect to the range parts 
            //and the dot range but if the user enters and invalid mask and we don't catch it...well to bad we will decide for them.
            console.log(dot);
            return dot;
        }
        else {
            return -1;
        }
    }
    else {
        return -1;
    }
}
function getMaskFromSubnetDot(dot) {
    var iDot = Number.parseInt(dot);
    var addressBase = 32 - iDot;
    var mask = ["255", "255", "255", "255"];
    if (addressBase > 0 && addressBase <= 16) {
        var hostBase = 16 - addressBase; //addressBase should be less than 16
        if (hostBase <= 8) {
            var fourthOctet = 256 - Math.pow(2, addressBase);
            mask[3] = fourthOctet.toString();
        }
        else if (hostBase > 8 && hostBase <= 16) {
            var thirdOctet = 256 - Math.pow(2, hostBase - 8);
            var fourthOctet = 256 - Math.pow(2, 16 - hostBase); //algebraically speaking....we could just use addressBase as 16 - hostBase = addressBase 
            //because 16 - addresBase = hostBase
            mask[2] = thirdOctet.toString();
            mask[3] = fourthOctet.toString();
        }
    }
    return mask.join(".");
}
function getFormattedErrorMessage(message, exception) {
    var finalMessage;
    if (exception instanceof _angular_common_http__WEBPACK_IMPORTED_MODULE_0__["HttpErrorResponse"]) {
        finalMessage = message + " details: " + exception.error;
    }
    else {
        finalMessage = message + " details " + exception.toString();
    }
    return finalMessage;
}
function assembleProxyAddress(parts) {
    var proxy = "";
    var protocol = /http+[s]?:\/\//.exec(parts.url);
    var tempUserNamePassword = "";
    if (!isStringEmpty(parts.username)) {
        if (!isStringEmpty(parts.password)) {
            tempUserNamePassword = parts.username + ":" + parts.password + "@";
        }
        else {
            tempUserNamePassword = parts.username + "@";
        }
    }
    if (protocol != null && protocol.index != -1) {
        proxy = parts.url.replace(protocol[0], protocol[0] + tempUserNamePassword);
    }
    else {
        proxy = parts.url + tempUserNamePassword;
    }
    if (!isStringEmpty(parts.port)) {
        proxy += ":" + parts.port;
    }
    return proxy;
}
function getProxyParts(proxyAddress) {
    // JRN JIRA WWP-1 - Rework so that Safari doesn't blow up on use of regular expressions.
    // Now using JS URL() class to do the parsing instead of custom parsing code.
    var proxyParts = { username: "", password: "", url: "", port: null };
    // Strip off any characters before http:// or https:// for the URL() class
    var location = proxyAddress.indexOf("http://");
    if (location == -1) {
        location = proxyAddress.indexOf("https://");
    }
    if (location > 0) {
        proxyAddress = proxyAddress.substr(location);
    }
    // create new URL object from passed in proxy string
    try {
        var url = new URL(proxyAddress);
        // use URL() method to parse and set values in proxyParts struct
        proxyParts.username = url.username;
        proxyParts.password = url.password;
        proxyParts.url = url.hostname;
        // if port isn't empty set it. Otherwise the default of null has been set above as the default
        if (url.port) {
            proxyParts.port = url.port;
        }
        console.log(proxyParts);
    }
    catch (err) { }
    return proxyParts;
}


/***/ }),

/***/ "./src/app/common/services/loading.service.ts":
/*!****************************************************!*\
  !*** ./src/app/common/services/loading.service.ts ***!
  \****************************************************/
/*! exports provided: LoadingService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoadingService", function() { return LoadingService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var LoadingService = /** @class */ (function () {
    function LoadingService() {
        this.loadingBackground = document.getElementById("boha-app-loading-background");
        this.loadingContainer = document.getElementById("boha-app-loading-container");
        this.loadingText = document.getElementById("boha-app-loading-text");
        if (this.loadingBackground == undefined || this.loadingContainer == undefined || this.loadingText == undefined) {
            throw ("loading service failed to initialize due to missing elements...");
        }
    }
    LoadingService.prototype.setWaiting = function (waiting, displayMessage) {
        if (displayMessage === void 0) { displayMessage = ""; }
        try {
            if (waiting) {
                this.loadingBackground.style.display = 'block';
                this.loadingBackground.style.opacity = ".75";
                this.loadingContainer.style.display = 'block';
                this.loadingText.innerText = displayMessage;
            }
            else {
                this.loadingContainer.style.display = 'none';
                this.loadingBackground.style.display = 'none';
                this.loadingBackground.style.opacity = "1";
            }
        }
        catch (err) {
            console.log("An error occurred while setting wait screen %s", err);
        }
    };
    LoadingService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], LoadingService);
    return LoadingService;
}());



/***/ }),

/***/ "./src/app/common/services/page.header.service.ts":
/*!********************************************************!*\
  !*** ./src/app/common/services/page.header.service.ts ***!
  \********************************************************/
/*! exports provided: PageHeaderService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PageHeaderService", function() { return PageHeaderService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _global_helper_functions__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./global-helper-functions */ "./src/app/common/services/global-helper-functions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};



var PageHeaderService = /** @class */ (function () {
    function PageHeaderService() {
        var _this = this;
        this.autoRefreshClicked = new rxjs__WEBPACK_IMPORTED_MODULE_1__["BehaviorSubject"](false);
        this.headerElement = document.getElementById("main-page-header");
        this.autoRefreshButton = document.getElementById("main-page-auto-refresh");
        this.autoRefreshOnIcon = document.getElementById("main-page-auto-refresh-on");
        this.autoRefreshOffIcon = document.getElementById("main-page-auto-refresh-off");
        this.menuItemOpen = document.getElementById("menu-open");
        this.navConnectionModeIcon = document.getElementById("main-page-connection-icon");
        document.addEventListener('click', function (event) {
            if (event.target.id != "menu-open" && event.target.id != "menu-open-label") {
                _this.menuItemOpen.checked = false;
            }
        });
        this.autoRefreshButton.addEventListener('click', function (event) {
            _this.autoRefreshOn = !_this.autoRefreshOn;
            if (_this.autoRefreshOn) {
                _this.autoRefreshOnIcon.style.display = "inline";
                _this.autoRefreshOffIcon.style.display = "none";
            }
            else {
                _this.autoRefreshOnIcon.style.display = "none";
                _this.autoRefreshOffIcon.style.display = "inline";
            }
            _this.autoRefreshClicked.next(_this.autoRefreshOn);
        });
    }
    PageHeaderService.prototype.getAutoRefresh = function () {
        return this.autoRefreshOn;
    };
    PageHeaderService.prototype.setHeaderTitle = function (title, iconClass) {
        if (iconClass === void 0) { iconClass = null; }
        if (iconClass != null) {
            var spacing = "&nbsp;&nbsp;";
            var faIcon = "<i class=\"fad fa-lg " + iconClass + "\"></i>";
            this.headerElement.innerHTML = faIcon + spacing + title;
        }
        else {
            this.headerElement.innerHTML = title;
        }
    };
    PageHeaderService.prototype.setConnectivityIconState = function (state) {
        if (state === void 0) { state = 1 /* AP */; }
        Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(this.navConnectionModeIcon, "fa-wifi-1");
        Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(this.navConnectionModeIcon, "fa-signal");
        Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(this.navConnectionModeIcon, "fa-wifi");
        Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(this.navConnectionModeIcon, "fa-router");
        Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["removeClass"])(this.navConnectionModeIcon, "fa-ethernet");
        if (state == 1 /* AP */) {
            Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["addClass"])(this.navConnectionModeIcon, "fa-router");
        }
        else if (state == 2 /* Wifi */) {
            Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["addClass"])(this.navConnectionModeIcon, "fa-wifi");
        }
        else if (state == 3 /* Ethernet */) {
            Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["addClass"])(this.navConnectionModeIcon, "fa-ethernet");
        }
        else if (state == 4 /* LTE */) {
            Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["addClass"])(this.navConnectionModeIcon, "fa-signal");
        }
        else {
            Object(_global_helper_functions__WEBPACK_IMPORTED_MODULE_2__["addClass"])(this.navConnectionModeIcon, "fa-wifi-1");
        }
        this.connectivityState = state;
    };
    PageHeaderService.prototype.setHeaderIcon = function (iconClass, concat) {
        if (concat === void 0) { concat = false; }
        if (concat) {
            var spacing = "&nbsp;&nbsp;";
            var faIcon = spacing + "<i class=\"fad fa-lg " + iconClass + "\"></i>";
            this.headerElement.innerHTML += faIcon;
        }
        else {
            var spacing = "&nbsp;&nbsp;";
            var faIcon = "<i class=\"fad fa-lg " + iconClass + "\"></i>" + spacing;
            this.headerElement.innerHTML = faIcon;
        }
    };
    PageHeaderService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [])
    ], PageHeaderService);
    return PageHeaderService;
}());



/***/ }),

/***/ "./src/app/common/services/route-animation.ts":
/*!****************************************************!*\
  !*** ./src/app/common/services/route-animation.ts ***!
  \****************************************************/
/*! exports provided: fadeTransition */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "fadeTransition", function() { return fadeTransition; });
/* harmony import */ var _angular_animations__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/animations */ "./node_modules/@angular/animations/fesm5/animations.js");

var fadeTransition = Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["trigger"])('routeAnimations', [
    Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["transition"])('* <=> *', [
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter, :leave', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
            position: 'absolute',
            left: 0,
            width: '100%',
            opacity: 0,
            transform: 'scale(0) translate(100%)'
        }), { optional: true }),
        Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["group"])([
            Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["query"])(':enter', [
                Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["animate"])('1000ms ease', Object(_angular_animations__WEBPACK_IMPORTED_MODULE_0__["style"])({
                    position: 'relative',
                    opacity: 1,
                    transform: 'scale(1) translate(0)'
                }))
            ], { optional: true }),
        ])
    ]),
]);


/***/ }),

/***/ "./src/app/common/services/route.guard.service.ts":
/*!********************************************************!*\
  !*** ./src/app/common/services/route.guard.service.ts ***!
  \********************************************************/
/*! exports provided: RouteGuardService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "RouteGuardService", function() { return RouteGuardService; });
/* harmony import */ var _angular_router__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/router */ "./node_modules/@angular/router/fesm5/router.js");
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var RouteGuardService = /** @class */ (function () {
    function RouteGuardService(_router) {
        this.circularMenuId = "index-circular-menu";
        this.sessionStoreUserKey = "boha-portal:user";
        this.sessionStoreTokenKey = "boha-portal:token";
        this.isAuthenticated = false;
        this.router = _router;
        console.log(_router);
        this.setAuthenticationFlag("", "", false, false);
        _router.events.subscribe(function (event) {
            //console.log(event);
        });
    }
    RouteGuardService.prototype.checkForStoredSessionData = function () {
        var token = sessionStorage.getItem(this.sessionStoreUserKey);
        var user = sessionStorage.getItem(this.sessionStoreTokenKey);
        if (user != null && token != null) {
            this.setAuthenticationFlag(user, token, true, false);
        }
    };
    RouteGuardService.prototype.getActiveUser = function () {
        return this.activeUser;
    };
    RouteGuardService.prototype.getSessionToken = function () {
        return this.sessionToken;
    };
    RouteGuardService.prototype.getIsAuthenticated = function () {
        return this.isAuthenticated;
    };
    RouteGuardService.prototype.setAuthenticationFlag = function (user, token, authenticated, routehome) {
        this.activeUser = user;
        this.sessionToken = token;
        this.isAuthenticated = authenticated;
        if (this.isAuthenticated) {
            if (routehome) {
                this.router.navigate(["home"]);
            }
            this.makeMenuAvailable(true);
            sessionStorage.setItem(this.sessionStoreUserKey, user);
            sessionStorage.setItem(this.sessionStoreTokenKey, token);
        }
        else {
            //sessionStorage.clear();
            sessionStorage.removeItem(this.sessionStoreUserKey);
            sessionStorage.removeItem(this.sessionStoreTokenKey);
            this.makeMenuAvailable(false);
        }
    };
    RouteGuardService.prototype.makeMenuAvailable = function (available) {
        if (available) {
            document.getElementById(this.circularMenuId).style.display = "block";
        }
        else {
            document.getElementById(this.circularMenuId).style.display = "none";
        }
    };
    RouteGuardService.prototype.canActivate = function (route, state) {
        if (!this.isAuthenticated) {
            console.log("Not authenticated redirected...");
            console.log(this.isAuthenticated);
            this.router.navigate(["login"]);
        }
        return this.isAuthenticated;
    };
    RouteGuardService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_1__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_router__WEBPACK_IMPORTED_MODULE_0__["Router"]])
    ], RouteGuardService);
    return RouteGuardService;
}());



/***/ }),

/***/ "./src/app/common/services/session.token.intercepter.service.ts":
/*!**********************************************************************!*\
  !*** ./src/app/common/services/session.token.intercepter.service.ts ***!
  \**********************************************************************/
/*! exports provided: TokenInterceptor */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "TokenInterceptor", function() { return TokenInterceptor; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _route_guard_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ./route.guard.service */ "./src/app/common/services/route.guard.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};


var TokenInterceptor = /** @class */ (function () {
    function TokenInterceptor(routeGuard) {
        this.routeGuard = routeGuard;
    }
    TokenInterceptor.prototype.intercept = function (request, next) {
        request = request.clone({
            setHeaders: {
                Authorization: this.routeGuard.getSessionToken()
            }
        });
        console.log(request);
        return next.handle(request);
    };
    TokenInterceptor = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])(),
        __metadata("design:paramtypes", [_route_guard_service__WEBPACK_IMPORTED_MODULE_1__["RouteGuardService"]])
    ], TokenInterceptor);
    return TokenInterceptor;
}());



/***/ }),

/***/ "./src/app/common/services/settings.service.ts":
/*!*****************************************************!*\
  !*** ./src/app/common/services/settings.service.ts ***!
  \*****************************************************/
/*! exports provided: SettingsService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SettingsService", function() { return SettingsService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SettingsService = /** @class */ (function () {
    function SettingsService(http) {
        this.http = http;
        this.baseUrl = "/api/v1/settings/";
        this.httpHeaders = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Accept': 'application/json'
            })
        };
    }
    SettingsService.prototype.discreteSetting = function (path) {
        var normalizedPath;
        if (path.charAt(0) == '/') {
            normalizedPath = path.substr(1, path.length - 1).toLowerCase();
        }
        else {
            normalizedPath = path.toLowerCase();
        }
        var url = "/api/v1/setting/" + normalizedPath;
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    SettingsService.prototype.connections = function () {
        var url = this.baseUrl + "connections";
        console.log(url);
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    SettingsService.prototype.adminPassword = function (userInfo) {
        var url = this.baseUrl + "user/admin/password";
        console.log(url);
        return this.http.post(url, userInfo, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ///////////////////////////ASYNC///////////////////////////////
    SettingsService.prototype.adminPassword_async = function (userInfo) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.adminPassword(userInfo).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    SettingsService.prototype.connections_async = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.connections().subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    SettingsService.prototype.discreteSetting_async = function (path) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.discreteSetting(path).subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    SettingsService.prototype.handleError = function (error) {
        console.log(error);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
    };
    SettingsService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SettingsService);
    return SettingsService;
}());



/***/ }),

/***/ "./src/app/common/services/system.service.ts":
/*!***************************************************!*\
  !*** ./src/app/common/services/system.service.ts ***!
  \***************************************************/
/*! exports provided: SystemService */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SystemService", function() { return SystemService; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_common_http__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/common/http */ "./node_modules/@angular/common/fesm5/http.js");
/* harmony import */ var rxjs_operators__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! rxjs/operators */ "./node_modules/rxjs/_esm5/operators/index.js");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};




var SystemService = /** @class */ (function () {
    function SystemService(http) {
        this.http = http;
        this.baseUrl = "/api/v1/system/";
        this.httpHeaders = {
            headers: new _angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpHeaders"]({
                'Accept': 'application/json'
            })
        };
    }
    SystemService.prototype.getAvahiName = function () {
        var url = this.baseUrl + "avahi/name";
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    SystemService.prototype.setAvahiName = function (serviceData) {
        var url = this.baseUrl + "avahi/name";
        return this.http.post(url, serviceData, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    SystemService.prototype.setProxySetting = function (proxySetting) {
        var url = this.baseUrl + "proxy/setting";
        // JRN JIRA WWP-4 Make changing proxy setting persistent
        // make return type of post call match the Observable type
        return this.http.post(url, proxySetting, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    SystemService.prototype.getProxySetting = function () {
        var url = this.baseUrl + "proxy/setting";
        return this.http.get(url, this.httpHeaders).pipe(Object(rxjs_operators__WEBPACK_IMPORTED_MODULE_2__["catchError"])(this.handleError));
    };
    ///////////////ASYNC FUNCTION///////////////////////////
    SystemService.prototype.getProxySetting_async = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getProxySetting().subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    SystemService.prototype.setProxySetting_async = function (proxySetting) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.setProxySetting(proxySetting).subscribe(function (resp) { resolve(resp); }, function (err) { reject(err); });
        });
    };
    SystemService.prototype.getAvahiName_sync = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.getAvahiName().subscribe(function (resp) { resolve(resp); }, function (err) { reject(err); });
        });
    };
    SystemService.prototype.setAvahiName_async = function (serviceData) {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.setAvahiName(serviceData).subscribe(function (resp) { resolve(resp); }, function (err) { reject(err); });
        });
    };
    SystemService.prototype.handleError = function (error) {
        console.log(error);
        return Object(rxjs__WEBPACK_IMPORTED_MODULE_3__["throwError"])(error);
    };
    SystemService = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Injectable"])({
            providedIn: 'root'
        }),
        __metadata("design:paramtypes", [_angular_common_http__WEBPACK_IMPORTED_MODULE_1__["HttpClient"]])
    ], SystemService);
    return SystemService;
}());



/***/ }),

/***/ "./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.css":
/*!***************************************************************************************!*\
  !*** ./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.css ***!
  \***************************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".boha-modal-background{\r\n    position: absolute;\r\n    top:0px;\r\n    left: 0px;\r\n    z-index: 415;\r\n    width: 100%; \r\n    height: calc(100% + 50px); \r\n    text-align: center;\r\n    vertical-align: middle;\r\n    background-color: rgba(34, 51, 68, .65);\r\n    color:white;\r\n    overflow: hidden;\r\n    /*padding-left: 5%;\r\n    padding-right: 5%;*/\r\n\r\n  }\r\n  .boha-modal-box{\r\n    width: 100%;\r\n    position: relative;\r\n    margin-top: 50%;\r\n    min-height: 125px;\r\n    background-color: var(--boha-neutral-gray-very-light);\r\n    text-align: left;\r\n    padding: 10px;\r\n    color: var(--boha-neutral-gray-dark);\r\n    border: 1px solid var(--boha-neutral-gray-medium);\r\n    /* box-shadow: -2px 4px 3px var(--boha-neutral-gray-dark); */\r\n   \r\n  }\r\n  .boha-modal-header{\r\n    font-weight: 700;\r\n    height: 35px;\r\n    border-bottom: 1px solid var(--boha-primary-natural-blue);\r\n    margin-bottom: 10px;\r\n  }\r\n  .boha-modal-button{\r\n    min-height: 35px;\r\n    min-width: 100px;\r\n    float: right;\r\n    margin-left: 10px;\r\n    background-color: var( --boha-primary-natural-blue);\r\n    color: var(--boha-neutral-gray-very-light);\r\n  }\r\n  .boha-modal-message-area{\r\n    min-height: 50px;\r\n    margin-bottom: 10px;\r\n    padding: 15px;\r\n  }\r\n"

/***/ }),

/***/ "./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.ts":
/*!**************************************************************************************!*\
  !*** ./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.ts ***!
  \**************************************************************************************/
/*! exports provided: ModalConfirmationComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ModalConfirmationComponent", function() { return ModalConfirmationComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var ModalConfirmationComponent = /** @class */ (function () {
    function ModalConfirmationComponent() {
        this.messageText = "";
        this.title = "";
        this.messageTimeout = 5000;
    }
    ModalConfirmationComponent.prototype.ngOnInit = function () {
    };
    ModalConfirmationComponent.prototype.showDialog = function (message, title, autoHide) {
        var _this = this;
        if (autoHide === void 0) { autoHide = false; }
        return new Promise(function (resolve, reject) {
            try {
                if (!_this.show) {
                    _this.resolve = resolve;
                    _this.reject = reject;
                    _this.show = true;
                    _this.messageText = message;
                    _this.title = title;
                    if (autoHide) {
                        setTimeout(function () {
                            _this.show = false;
                            _this.respond(0 /* Cancel */);
                        }, _this.messageTimeout);
                    }
                }
            }
            catch (error) {
                _this.respond(0 /* Cancel */, true);
            }
        });
    };
    ModalConfirmationComponent.prototype.respond = function (response, reject) {
        if (reject === void 0) { reject = false; }
        if (reject) {
            if (this.reject != null && this.reject != undefined) {
                this.reject(response);
            }
        }
        else {
            if (this.resolve != null && this.resolve != undefined) {
                this.resolve(response);
            }
        }
    };
    ModalConfirmationComponent.prototype.userResponse = function (response) {
        this.show = false;
        if (response == 2) {
            this.respond(2 /* Yes */);
        }
        else if (response == 1) {
            this.respond(1 /* No */);
        }
        else {
            this.respond(0 /* Cancel */);
        }
    };
    ModalConfirmationComponent.prototype.hideDialog = function () {
        this.show = false;
        this.resolve(0 /* Cancel */);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ModalConfirmationComponent.prototype, "messageText", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], ModalConfirmationComponent.prototype, "title", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], ModalConfirmationComponent.prototype, "messageTimeout", void 0);
    ModalConfirmationComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'modal-confirmation',
            template: '<div class="boha-modal-background" *ngIf="show">' +
                '<div class="boha-modal-box">' +
                '<div class="row boha-modal-header">' +
                '<div class="col">{{title}}</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col boha-modal-message-area">' +
                '{{messageText}}' +
                '</div>' +
                '</div>' +
                '<div class="row">' +
                '<div class="col">' +
                '<button class="btn boha-modal-button" (click)="userResponse(0)">Cancel</button>' +
                '<button class="btn boha-modal-button" (click)="userResponse(1)">No</button>' +
                '<button class="btn boha-modal-button" (click)="userResponse(2)">Yes</button>' +
                '</div>' +
                '</div>' +
                '</div>' +
                '</div>',
            styles: [__webpack_require__(/*! ./modal-confirmation.component.css */ "./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.css")]
        }),
        __metadata("design:paramtypes", [])
    ], ModalConfirmationComponent);
    return ModalConfirmationComponent;
}());

;


/***/ }),

/***/ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.css":
/*!*******************************************************************************!*\
  !*** ./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.css ***!
  \*******************************************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".boha-error-container{\r\n    width: 100%;\r\n    margin-top: 5px;\r\n    position: relative;\r\n    margin-bottom: 5px;\r\n }"

/***/ }),

/***/ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts":
/*!******************************************************************************!*\
  !*** ./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts ***!
  \******************************************************************************/
/*! exports provided: PopUpMessageComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "PopUpMessageComponent", function() { return PopUpMessageComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};

var PopUpMessageComponent = /** @class */ (function () {
    function PopUpMessageComponent() {
        this.messageTimeout = 5000;
    }
    PopUpMessageComponent.prototype.ngOnInit = function () {
    };
    PopUpMessageComponent.prototype.displayMessage = function (success, message) {
        var _this = this;
        if (!this.displayErrorMessage && !this.displaySuccessMessage) {
            this.displaySuccessMessage = success;
            this.displayErrorMessage = !success;
            this.messageText = message;
            setTimeout(function () {
                _this.displaySuccessMessage = false;
                _this.displayErrorMessage = false;
            }, this.messageTimeout);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", String)
    ], PopUpMessageComponent.prototype, "messageText", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Input"])(),
        __metadata("design:type", Number)
    ], PopUpMessageComponent.prototype, "messageTimeout", void 0);
    PopUpMessageComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'pop-up-message',
            template: '<div class="boha-error-container">' +
                '<div class="alert alert-danger" *ngIf="displayErrorMessage">' +
                '<strong>Error!&nbsp;</strong>{{messageText}}' +
                '</div>' +
                '<div class="alert alert-success" *ngIf="displaySuccessMessage">' +
                '<strong>Success!&nbsp;</strong>{{messageText}}.' +
                '</div>' +
                '</div>',
            styles: [__webpack_require__(/*! ./pop-up-message.component.css */ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.css")],
        }),
        __metadata("design:paramtypes", [])
    ], PopUpMessageComponent);
    return PopUpMessageComponent;
}());



/***/ }),

/***/ "./src/app/connectivity/connectivity.component.css":
/*!*********************************************************!*\
  !*** ./src/app/connectivity/connectivity.component.css ***!
  \*********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".info-wifi-list{\r\n    width: 100%;\r\n    margin-top: 5px;\r\n    text-align: left;\r\n    padding: 5px;\r\n }\r\n"

/***/ }),

/***/ "./src/app/connectivity/connectivity.component.html":
/*!**********************************************************!*\
  !*** ./src/app/connectivity/connectivity.component.html ***!
  \**********************************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\r\n    text-align: center;\r\n    margin-top: 10px;\r\n    border: 1px solid var(--boha-neutral-gray-medium);\r\n    border-radius: 5px;\r\n    min-height: 300px;\r\n    width: 100%;\r\n\r\n    h3\r\n    font-size: 1.75rem;\r\n    /* background-color: var(--boha-primary-light-blue); */\r\n    border-radius: 5px 5px 0 0;\r\n    border-bottom: 1px solid var(--boha-neutral-gray-medium);\r\n    padding: 5px;\r\n\r\n-->\r\n<div class=\"boha-child-page\">\r\n    <pop-up-message>\r\n    \r\n    </pop-up-message>\r\n    <modal-confirmation>\r\n\r\n    </modal-confirmation>\r\n    <!---------------AP CONNECTIVITY-------------------------->\r\n  <div class=\"boha-info-panel\" >\r\n    <div class=\"boha-relative-internal-container\">\r\n        <div class=\"row\" style=\"line-height: 40px; height: 40px;margin-top:5px\">\r\n            <div class=\"col\" >\r\n                <i class=\"fas fa-slash fa-lg\" *ngIf=\"!apConnectionOn && !wifiNetworksRefreshing\"></i>\r\n                <i class=\"fas fa-router fa-lg\" *ngIf=\"apConnectionOn && !wifiNetworksRefreshing\"></i>\r\n                <i class=\"fad fa-spinner-third fa-spin fa-lg\" *ngIf=\"wifiNetworksRefreshing\"></i>\r\n                &nbsp;&nbsp;\r\n                <span style=\"font-weight: 700;\">\r\n                    {{apConnectionSSID}}\r\n                    <span *ngIf=\"apDeviceMissingError\" style=\"color:var(--boha-secondary-red)\">&nbsp;AP device information is missing.</span>\r\n                </span>\r\n            </div>\r\n        </div>\r\n        <div class=\"row boha-input-row\" >\r\n            <div class=\"col-2\"> \r\n                SSID:\r\n            </div>\r\n            <div class=\"col-10\">\r\n                <input type=\"text\" placeholder=\"Broadcast Name\" \r\n                aria-label=\"Broadcast Name\" class=\"form-control\" [(ngModel)]=\"apConnectionSSID\" (keydown)=\"apFieldKeyEvent($event)\"/>\r\n            \r\n            </div>\r\n        </div>\r\n        <div class=\"row boha-input-row\">\r\n            <div class=\"col-2\"> \r\n                Password:\r\n            </div>\r\n            <div class=\"col-10\">\r\n                <input type=\"password\" placeholder=\"Enter Password\" \r\n                aria-label=\"Password\" class=\"form-control\" [(ngModel)]=\"apConnectionPassword1\" (keydown)=\"apFieldKeyEvent($event)\"/>\r\n            \r\n            </div>\r\n        </div>\r\n        <div class=\"row boha-input-row\">\r\n            <div class=\"col-2\"> \r\n                Re-enter:\r\n            </div>\r\n            <div class=\"col-10\">\r\n                <input type=\"password\" placeholder=\"Re-enter Password\" \r\n                aria-label=\"Re-enter\" class=\"form-control\" [(ngModel)]=\"apConnectionPassword2\" (keydown)=\"apFieldKeyEvent($event)\"/>\r\n               \r\n            </div>\r\n        </div>\r\n        <div class=\"row boha-input-row\" style=\"margin-top: 5px; min-height: 50px;\">\r\n            <div class=\"col\">\r\n                <a (click)=\"deleteConnection('ap')\" style=\"float:left\" *ngIf=\"apIPAddress != '' && apConnectionOn\"><i class=\"fad fa-trash fa-lg\"></i></a>\r\n                <button class=\"btn boha-btn-primary\" style=\"float: right; min-width: 100px;\" \r\n                (click)=\"preCheckConnectionData('ap')\" [disabled]=\"!apConnectionSettingsTouched\"><i class=\"fad fa-save\"></i>&nbsp;Save</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n  </div>\r\n   <!---------------INFRASTRUCTURE CONNECTIVITY-------------------------->\r\n   <div class=\"boha-info-panel\">\r\n    <div class=\"info-wifi-list\">\r\n        <div class=\"row\">\r\n            <div class=\"col\">\r\n                <span>\r\n                    <i class=\"fas fa-wifi-slash fa-lg\" *ngIf=\"!isConnectionOn && !wifiNetworksRefreshing\"></i>\r\n                    <i class=\"fas fa-wifi fa-lg\" *ngIf=\"isConnectionOn && !wifiNetworksRefreshing\"></i>\r\n                    <i class=\"fad fa-spinner-third fa-spin fa-lg\" *ngIf=\"wifiNetworksRefreshing\"></i>\r\n                    &nbsp;\r\n                    <span style=\"font-weight: 700;\">{{isConnectionSSID}}</span>\r\n                    <span *ngIf=\"isDeviceMissingError\" style=\"color:var(--boha-secondary-red)\">&nbsp;IS device information is missing.</span>\r\n                    <a style=\"float: right;margin-right: 10px;\" (click)=\"getWifiNetworks(isDevice)\"><i class=\"fad fa-sync\"></i>&nbsp;Refresh</a>\r\n                </span>\r\n            </div>\r\n        </div>\r\n    </div>\r\n    <div class=\"info-wifi-list\">\r\n        <div class=\"row boha-input-row\" style=\"overflow: visible\">\r\n            <div class=\"col-2\">\r\n                SSID\r\n            </div>\r\n            <div class=\"col-10\">\r\n                <div class=\"input-group mb-3\">\r\n                    <input type=\"text\" class=\"form-control\" placeholder=\"WiFi SSID\" aria-label=\"Wifi SSID\" aria-describedby=\"basic-addon2\" \r\n                    [(ngModel)]=\"wifissid\" [disabled]=\"!isConnectionWifi\" (keydown)=\"isSSIDKeyEvent($event)\">\r\n                    <div class=\"input-group-append\">\r\n                        <span class=\"input-group-text boha-editable-drop-down-button\" id=\"wifi-input-drop-down-button\"  \r\n                        (click)=\"dropDownWifiSelection(!wifidropdown)\" >\r\n                            <i class=\"fad fa-arrow-alt-circle-down\"></i>\r\n                        </span>\r\n                    </div>\r\n                    <div  class=\"boha-input-container\" *ngIf=\"wifidropdown\" style=\"border-radius: none;\">\r\n                        <div class=\"boha-input-container-header\">\r\n                            <div class=\"boha-input-container-col\" style=\"width: 50%;\">SSID</div>\r\n                            <div class=\"boha-input-container-col\" style=\"width: 20%;\">Signal</div>\r\n                            <div class=\"boha-input-container-col\" style=\"width: 25%;\">Security</div>\r\n                        </div>\r\n                        <div class=\"boha-input-scroll\">\r\n                            <ul >\r\n                                <li *ngFor=\"let network of networks\" >\r\n                                    <a (click)=\"selectWifiNetwork(network.ssid)\" style=\"width:100%; overflow: hidden;\">\r\n                                        <div class=\"boha-input-container-col\" style=\"width: 50%;\">\r\n                                            <i class=\"fas fa-lg \"  [ngClass]=\"{'fa-signal-slash' : network.bars == 0, 'fa-signal-1' : network.bars == 1, \r\n                                            'fa-signal-2' : network.bars == 2, 'fa-signal-3' : network.bars  == 3, 'fa-signal-4' : network.bars == 4}\"></i>\r\n                                            &nbsp;{{network.ssid == '' ? 'NONE' : network.ssid}}\r\n                                        </div>\r\n                                        <div class=\"boha-input-container-col\" style=\"width: 20%;\">{{network.signal_level}}</div>\r\n                                        <div class=\"boha-input-container-col\"  style=\"width: 25%;\">{{network.security == '' ? 'NA' : network.security}}</div>\r\n                                    </a>\r\n                                </li>\r\n                            </ul>\r\n                        </div>\r\n                        \r\n                    </div>\r\n                    \r\n                </div>\r\n            </div>\r\n            \r\n        </div>\r\n        <div class=\"row boha-input-row\">\r\n            <div class=\"col-2\">\r\n                Password\r\n            </div>\r\n            <div class=\"col-10\">\r\n                <div class=\"input-group mb-3\">\r\n                    <input type=\"password\" class=\"form-control\" placeholder=\"WiFi Password\" aria-label=\"Wifi Password\" \r\n                    [(ngModel)]=\"wifipassword\" id=\"input-wifi-password\" [disabled]=\"!isConnectionWifi\">\r\n                    <div class=\"input-group-append\">\r\n                        <span class=\"input-group-text boha-editable-password-button\" id=\"wifi-input-password-button\"  (click)=\"onShowPassword(!showPassword)\">\r\n                            <i class=\"fad fa-eye\" *ngIf=\"showPassword\"></i>\r\n                            <i class=\"fad fa-eye-slash\" *ngIf=\"!showPassword\"></i>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n            </div>\r\n        </div>\r\n        <div class=\"row boha-input-row\" style=\"margin-top: 5px; min-height: 50px;\">\r\n          <div class=\"col\">\r\n            <a (click)=\"deleteConnection('is')\" style=\"float:left\" *ngIf=\"isIPAddress != '' && isConnectionOn\"><i class=\"fad fa-trash fa-lg\"></i></a> \r\n            <button class=\"btn boha-btn-primary\" style=\"float: right; min-width: 100px;\" \r\n            (click)=\"preCheckConnectionData('is')\" [disabled]=\"!isConnectionWifi || !isConnectionSettingsTouched\"><i class=\"fad fa-plug\"></i>&nbsp;Connect</button>\r\n          </div>\r\n        </div>\r\n    </div>\r\n</div>\r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/connectivity/connectivity.component.ts":
/*!********************************************************!*\
  !*** ./src/app/connectivity/connectivity.component.ts ***!
  \********************************************************/
/*! exports provided: ConnectivityComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "ConnectivityComponent", function() { return ConnectivityComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_services_settings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/services/settings.service */ "./src/app/common/services/settings.service.ts");
/* harmony import */ var _common_services_connectivity_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/services/connectivity.service */ "./src/app/common/services/connectivity.service.ts");
/* harmony import */ var _common_data_wifi_credentials___WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/data/wifi.credentials. */ "./src/app/common/data/wifi.credentials..ts");
/* harmony import */ var _common_services_loading_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/services/loading.service */ "./src/app/common/services/loading.service.ts");
/* harmony import */ var _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/services/page.header.service */ "./src/app/common/services/page.header.service.ts");
/* harmony import */ var _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/ui-widgets/pop-up-message/pop-up-message.component */ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts");
/* harmony import */ var _common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../common/services/global-helper-functions */ "./src/app/common/services/global-helper-functions.ts");
/* harmony import */ var _common_data_connection_info__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common/data/connection.info */ "./src/app/common/data/connection.info.ts");
/* harmony import */ var _common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/ui-widgets/modal-confirmation/modal-confirmation.component */ "./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var ConnectivityComponent = /** @class */ (function () {
    function ConnectivityComponent(settingsService, connectivityService, loadingService, pageHeaderService) {
        this.settingsService = settingsService;
        this.connectivityService = connectivityService;
        this.loadingService = loadingService;
        this.pageHeaderService = pageHeaderService;
        this.NOTCONNECTED = "Not connected";
        this.wifidropdown = false;
        this.isConnectionWifi = false;
        this.isConnectionSSID = "Not connected";
        this.isConnectionOn = false;
        this.isConnection = null;
        this.isConnectionSettingsTouched = false;
        this.isDeviceMissingError = false;
        this.apConnectionSettingsTouched = false;
        this.apConnectionOn = false;
        this.apConnection = null;
        this.apDeviceMissingError = false;
        this.networks = null;
        this.showPassword = false;
        this.wifiNetworksRefreshing = false;
        this.longPollHandle = null;
    }
    ConnectivityComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Home init....");
        this.getConnectionInfo();
        this.pageHeaderService.autoRefreshClicked.subscribe(function (refresh) {
            console.log(refresh);
            if (refresh && _this.longPollHandle == null) {
                _this.longPollHandle = setInterval(function () {
                    if (!_this.wifidropdown) {
                        _this.getConnectionInfo();
                    }
                }, 5000);
            }
            else {
                if (_this.longPollHandle != null) {
                    clearInterval(_this.longPollHandle);
                }
            }
        });
    };
    ConnectivityComponent.prototype.ngAfterViewInit = function () {
        console.log("Connectivity view init....");
        this.pageHeaderService.setHeaderTitle("Connectivity", "fa-wifi");
    };
    ConnectivityComponent.prototype.ngDestroy = function () {
        if (this.longPollHandle) {
            clearInterval(this.longPollHandle);
        }
    };
    ConnectivityComponent.prototype.resetISConnectionInfoData = function () {
        this.isConnectionMode = "";
        this.isConnectionWifi = false;
        this.isDevice = "";
        this.isConnectionSSID = "NONE";
        this.isConnectionId = "";
        this.isConnectionOn = false;
        this.isConnection = null;
        this.apIPAddress = "";
    };
    ConnectivityComponent.prototype.resetAPConnectionInfoData = function () {
        this.apConnectionMode = "";
        this.apConnectionType = "";
        this.apDevice = "";
        this.apConnectionSSID = "NONE";
        this.apConnectionId = "";
        this.apConnectionSSIDOriginal = "";
        this.apConnectionOn = false;
        this.apConnection = null;
        this.isIPAddress = "";
    };
    ConnectivityComponent.prototype.getConnectionInfo = function () {
        return __awaiter(this, void 0, void 0, function () {
            var connsettings, devicesInfo, _a, error_1;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 6, 7, 8]);
                        this.loadingService.setWaiting(true, "Retrieving connection info....");
                        return [4 /*yield*/, this.settingsService.connections_async()];
                    case 1:
                        connsettings = _b.sent();
                        return [4 /*yield*/, this.connectivityService.devices_async()];
                    case 2:
                        devicesInfo = _b.sent();
                        this.resetISConnectionInfoData();
                        this.resetAPConnectionInfoData();
                        _a = this;
                        return [4 /*yield*/, this.connectivityService.hostConnectionEcho_async()];
                    case 3:
                        _a.hostIPAddress = _b.sent();
                        return [4 /*yield*/, this.getISConnectionInfo(devicesInfo, connsettings)];
                    case 4:
                        _b.sent();
                        return [4 /*yield*/, this.getAPConnectionInfo(devicesInfo, connsettings)];
                    case 5:
                        _b.sent();
                        return [3 /*break*/, 8];
                    case 6:
                        error_1 = _b.sent();
                        console.log(error_1);
                        return [3 /*break*/, 8];
                    case 7:
                        this.loadingService.setWaiting(false, "");
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.getISConnectionInfo = function (devicesInfo, connsettings) {
        return __awaiter(this, void 0, void 0, function () {
            var isConn_1, isdevice, isConnnection, isCondetails, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        this.isDeviceMissingError = false;
                        isConn_1 = connsettings.find(function (conn) { return conn.mode == "is"; });
                        console.log(isConn_1);
                        if (isConn_1 != null) {
                            this.isConnectionMode = isConn_1.mode;
                            this.isConnectionType = isConn_1.type;
                            this.isConnectionWifi = (isConn_1.type == "wifi");
                        }
                        isdevice = devicesInfo.find(function (device) { return device.device == isConn_1.adapter; });
                        if (!(isdevice != null)) return [3 /*break*/, 3];
                        this.isDevice = isdevice.device;
                        isConnnection = isdevice.connection;
                        if (!(isConnnection != null && isConnnection != undefined && isConnnection.length != 0 && isConnnection != "--")) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.connectivityService.connectionDetails_async(isConnnection)];
                    case 1:
                        isCondetails = _a.sent();
                        this.isConnectionSSID = (isCondetails.ssid != null ? isCondetails.ssid.toString() : "NONE");
                        this.isConnectionId = isCondetails.id;
                        this.isConnectionOn = isCondetails.ssid != null;
                        this.isConnection = isCondetails;
                        this.wifissid = isCondetails.ssid.toString();
                        this.isIPAddress = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["parsePrimaryIp"])(isCondetails.primaryAddress);
                        _a.label = 2;
                    case 2:
                        this.getWifiNetworks(this.isDevice);
                        return [3 /*break*/, 4];
                    case 3:
                        this.isDeviceMissingError = true;
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.getAPConnectionInfo = function (devicesInfo, connsettings) {
        return __awaiter(this, void 0, void 0, function () {
            var apConn_1, apdevice, apConnnection, apCondetails, error_3;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 5, , 6]);
                        this.apDeviceMissingError = false;
                        apConn_1 = connsettings.find(function (conn) { return conn.mode == "ap"; });
                        console.log(apConn_1);
                        this.isConnectionMode = apConn_1.mode;
                        this.isConnectionType = apConn_1.type;
                        apdevice = devicesInfo.find(function (device) { return device.device == apConn_1.adapter; });
                        if (!(apdevice != null)) return [3 /*break*/, 3];
                        this.apDevice = apdevice.device;
                        apConnnection = apdevice.connection;
                        console.log(apConnnection);
                        if (!(apConnnection != null && apConnnection != undefined && apConnnection.length != 0 && apConnnection != "--")) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.connectivityService.connectionDetails_async(apConnnection)];
                    case 1:
                        apCondetails = _a.sent();
                        this.apConnectionSSID = (apCondetails.ssid != null ? apCondetails.ssid : "NONE");
                        this.apConnectionId = apCondetails.id;
                        console.log(this.apConnectionId);
                        this.apConnectionSSIDOriginal = this.apConnectionSSID;
                        this.apConnectionOn = apCondetails.ssid != null;
                        this.apConnection = apCondetails;
                        this.apIPAddress = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["parsePrimaryIp"])(apCondetails.primaryAddress);
                        _a.label = 2;
                    case 2: return [3 /*break*/, 4];
                    case 3:
                        this.apDeviceMissingError = true;
                        _a.label = 4;
                    case 4: return [3 /*break*/, 6];
                    case 5:
                        error_3 = _a.sent();
                        console.log(error_3);
                        return [3 /*break*/, 6];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.getWifiNetworks = function (adapter) {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_4;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, 3, 4]);
                        this.wifiNetworksRefreshing = true;
                        _a = this;
                        return [4 /*yield*/, this.connectivityService.networks_async(adapter)];
                    case 1:
                        _a.networks = _b.sent();
                        return [3 /*break*/, 4];
                    case 2:
                        error_4 = _b.sent();
                        console.log(error_4);
                        return [3 /*break*/, 4];
                    case 3:
                        this.wifiNetworksRefreshing = false;
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.selectWifiNetwork = function (ssid) {
        this.dropDownWifiSelection(false);
        this.wifissid = ssid;
        this.isConnectionSettingsTouched = true;
    };
    ConnectivityComponent.prototype.dropDownWifiSelection = function (drop) {
        this.wifidropdown = drop;
    };
    ConnectivityComponent.prototype.connectToWifiNetwork = function () {
        return __awaiter(this, void 0, void 0, function () {
            var connsettings, credentials, isConn, error_5, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, 5, 6]);
                        this.loadingService.setWaiting(true, "Attempting to connect....");
                        return [4 /*yield*/, this.settingsService.connections_async()];
                    case 1:
                        connsettings = _a.sent();
                        credentials = new _common_data_wifi_credentials___WEBPACK_IMPORTED_MODULE_3__["WifiCredentials"];
                        credentials.ssid = this.wifissid;
                        credentials.password = this.wifipassword;
                        isConn = connsettings.find(function (conn) { return conn.mode == "is" && conn.type == "wifi"; });
                        credentials.adapter = isConn.adapter; //this.isDevice
                        return [4 /*yield*/, this.connectivityService.connectWifi_async(credentials)];
                    case 2:
                        _a.sent();
                        return [4 /*yield*/, this.getConnectionInfo()];
                    case 3:
                        _a.sent();
                        this.popup.displayMessage(true, "Successfully connected to network " + this.wifissid + ".");
                        return [3 /*break*/, 6];
                    case 4:
                        error_5 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["getFormattedErrorMessage"])("Failed connecting to " + this.wifissid + ".", error_5);
                        console.log(message);
                        this.popup.displayMessage(false, message);
                        return [3 /*break*/, 6];
                    case 5:
                        this.loadingService.setWaiting(false);
                        this.isConnectionSettingsTouched = false;
                        return [7 /*endfinally*/];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.onShowPassword = function (show) {
        this.showPassword = show;
        var element = document.getElementById("input-wifi-password");
        if (this.showPassword) {
            element.type = "text";
        }
        else {
            element.type = "password";
        }
    };
    ConnectivityComponent.prototype.saveAPSettings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var credentials, connected, error_6, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, 3, 4]);
                        credentials = new _common_data_wifi_credentials___WEBPACK_IMPORTED_MODULE_3__["WifiCredentials"]();
                        credentials.ssid = this.apConnectionSSID.toString();
                        credentials.password = this.apConnectionPassword1.toString();
                        credentials.adapter = this.apDevice.toString();
                        //let connsettings = await this.settingsService.connections_async();
                        //let apConn : Connection = connsettings.find(conn=>conn.type == "ap");
                        this.loadingService.setWaiting(true, "Attempting to save AP settings....");
                        return [4 /*yield*/, this.connectivityService.apWifiConnection_async(credentials)];
                    case 1:
                        connected = _a.sent();
                        console.log(connected);
                        this.apConnectionSettingsTouched = false;
                        this.popup.displayMessage(true, "Successfully updated AP settigns.");
                        return [3 /*break*/, 4];
                    case 2:
                        error_6 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["getFormattedErrorMessage"])("An error occured while trying to save AP settings. ", error_6);
                        console.log(message);
                        return [3 /*break*/, 4];
                    case 3:
                        this.loadingService.setWaiting(false);
                        return [7 /*endfinally*/];
                    case 4: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.apFieldKeyEvent = function (event) {
        this.apConnectionSettingsTouched = true;
    };
    ConnectivityComponent.prototype.isSSIDKeyEvent = function (event) {
        this.isConnectionSettingsTouched = true;
    };
    ConnectivityComponent.prototype.preCheckConnectionData = function (connectionMode) {
        return __awaiter(this, void 0, void 0, function () {
            var response, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(connectionMode == "ap")) return [3 /*break*/, 4];
                        if (!(this.apIPAddress == this.hostIPAddress)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirmation", false)];
                    case 1:
                        response = _a.sent();
                        if (response == 2 /* Yes */) {
                            this.saveAPSettings();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        if (!Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["isStringEmpty"])(this.apConnectionPassword1) && !Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["isStringEmpty"])(this.apConnectionPassword2)) {
                            if (this.apConnectionPassword1 != this.apConnectionPassword2) {
                                this.popup.displayMessage(false, "Passwords must match exactly. Please verify.");
                                console.log("Passwords don't match.");
                            }
                            else {
                                this.saveAPSettings();
                            }
                        }
                        else {
                            this.popup.displayMessage(false, "Password(s) can't be left blank and must match exactly.");
                        }
                        _a.label = 3;
                    case 3: return [3 /*break*/, 7];
                    case 4:
                        if (!(connectionMode == "is")) return [3 /*break*/, 7];
                        if (!(this.isIPAddress == this.hostIPAddress)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirm change", false)];
                    case 5:
                        response = _a.sent();
                        if (response == 2 /* Yes */) {
                            this.connectToWifiNetwork();
                        }
                        return [3 /*break*/, 7];
                    case 6:
                        this.connectToWifiNetwork();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.deleteConnection = function (connectionMode) {
        return __awaiter(this, void 0, void 0, function () {
            var connectionInfo, response, error_7, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, 5, 6]);
                        connectionInfo = new _common_data_connection_info__WEBPACK_IMPORTED_MODULE_8__["ConnectionInfo"]();
                        if (connectionMode == "ap") {
                            if (!Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["isStringEmpty"])(this.apConnectionId)) {
                                connectionInfo.name = this.apConnectionId;
                            }
                            else {
                                console.log("Connection ID can't be empty.");
                            }
                        }
                        else if (connectionMode == "is") {
                            if (!Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["isStringEmpty"])(this.isConnectionId)) {
                                connectionInfo.name = this.isConnectionId;
                            }
                            else {
                                console.log("Connection ID can't be empty.");
                            }
                        }
                        return [4 /*yield*/, this.confirmation.showDialog("You are about to delete the network connection " + connectionInfo.name + ". Do you wish to continue?", "Confirm delete " + connectionInfo.name, false)];
                    case 1:
                        response = _a.sent();
                        if (!(response == 2 /* Yes */)) return [3 /*break*/, 3];
                        this.loadingService.setWaiting(true, "Attempting to delete connection " + connectionInfo.name);
                        return [4 /*yield*/, this.connectivityService.connectionDelete_async(connectionInfo)];
                    case 2:
                        _a.sent();
                        this.popup.displayMessage(true, "Successfully delete connection " + connectionInfo.name);
                        _a.label = 3;
                    case 3: return [3 /*break*/, 6];
                    case 4:
                        error_7 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_7__["getFormattedErrorMessage"])("An error occured while trying to delete the connection. ", error_7);
                        this.popup.displayMessage(false, message);
                        return [3 /*break*/, 6];
                    case 5:
                        this.loadingService.setWaiting(false);
                        return [7 /*endfinally*/];
                    case 6: return [2 /*return*/];
                }
            });
        });
    };
    ConnectivityComponent.prototype.documentClickEvent = function (target) {
        var drowdownTarget = false;
        if (target != null) {
            if (target.id === 'wifi-input-drop-down-button') {
                drowdownTarget = true;
            }
            else {
                if (target.parentElement != null) {
                    if (target.parentElement.id === 'wifi-input-drop-down-button') {
                        drowdownTarget = true;
                    }
                }
            }
        }
        else {
            drowdownTarget = false;
        }
        if (!drowdownTarget) {
            this.dropDownWifiSelection(false);
        }
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_6__["PopUpMessageComponent"]),
        __metadata("design:type", _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_6__["PopUpMessageComponent"])
    ], ConnectivityComponent.prototype, "popup", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_9__["ModalConfirmationComponent"]),
        __metadata("design:type", _common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_9__["ModalConfirmationComponent"])
    ], ConnectivityComponent.prototype, "confirmation", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["HostListener"])('document:click', ['$event.target']),
        __metadata("design:type", Function),
        __metadata("design:paramtypes", [HTMLElement]),
        __metadata("design:returntype", void 0)
    ], ConnectivityComponent.prototype, "documentClickEvent", null);
    ConnectivityComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-connect',
            template: __webpack_require__(/*! ./connectivity.component.html */ "./src/app/connectivity/connectivity.component.html"),
            styles: [__webpack_require__(/*! ./connectivity.component.css */ "./src/app/connectivity/connectivity.component.css")]
        }),
        __metadata("design:paramtypes", [_common_services_settings_service__WEBPACK_IMPORTED_MODULE_1__["SettingsService"],
            _common_services_connectivity_service__WEBPACK_IMPORTED_MODULE_2__["ConnectivityService"], _common_services_loading_service__WEBPACK_IMPORTED_MODULE_4__["LoadingService"],
            _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_5__["PageHeaderService"]])
    ], ConnectivityComponent);
    return ConnectivityComponent;
}());



/***/ }),

/***/ "./src/app/home/home.component.css":
/*!*****************************************!*\
  !*** ./src/app/home/home.component.css ***!
  \*****************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".connected-on{\r\n    border: 2px solid rgba(50, 205, 50, .55);\r\n}\r\n\r\n.configure-link{\r\n    position: relative;\r\n    color:var(--boha-primary-medium-blue); \r\n    line-height: 40px; \r\n    width:100%; \r\n    height: 100%;\r\n    font-weight: 700;\r\n}\r\n\r\n.connected-icon{\r\n    float:right; \r\n    margin-right: 5px; \r\n    color:var(--boha-secondary-green)\r\n}\r\n\r\n.configure-link:hover{\r\n    background-color: var(--boha-primary-natural-blue);\r\n    color: var(--boha-neutral-gray-very-light);\r\n    /*#1D5DCB;*/\r\n }\r\n\r\n.configure-link:active{\r\n    background-color: var(--boha-primary-light-blue);\r\n    color: var(--boha-neutral-gray-very-light);\r\n    border: none;\r\n    /*#1A98FF*/\r\n }"

/***/ }),

/***/ "./src/app/home/home.component.html":
/*!******************************************!*\
  !*** ./src/app/home/home.component.html ***!
  \******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\r\n    text-align: center;\r\n    margin-top: 10px;\r\n    border: 1px solid var(--boha-neutral-gray-medium);\r\n    border-radius: 5px;\r\n    min-height: 300px;\r\n    width: 100%;\r\n\r\n    h3\r\n    font-size: 1.75rem;\r\n    /* background-color: var(--boha-primary-light-blue); */\r\n    border-radius: 5px 5px 0 0;\r\n    border-bottom: 1px solid var(--boha-neutral-gray-medium);\r\n    padding: 5px;\r\n\r\n-->\r\n<div class=\"boha-child-page\">\r\n     <!--AP MODE DETAILS-->\r\n     <div class=\"boha-info-panel\"  [ngClass]=\"{'connected-on' : apIPAddress == hostIPAddress}\">\r\n        <div class=\"boha-info-list\" >\r\n            <!--<h3>Home - Active Settings</h3>-->\r\n         \r\n            <div class=\"boha-relative-internal-container\" >\r\n                <div class=\"boha-overlay-action-panel\" *ngIf=\"!refreshing && !apIPAddress.length > 0\">\r\n                    <a class=\"btn configure-link\" href=\"#connect\">\r\n                        <i class=\"fas fa-router fa-lg\"></i>&nbsp;\r\n                        Click here to setup the Workstation's Access Point\r\n                    </a>\r\n                </div>\r\n                <div class=\"row\" style=\"height:40px\">\r\n                    <div class=\"col\">\r\n                        <i class=\"fad fa-lg\" *ngIf=\"!refreshing\" \r\n                        [ngClass]=\"{'fa-router' : apConnDetails.type =='802-11-wireless', 'fa-ethernet' : apConnDetails.type =='802-3-ethernet'}\"></i>\r\n                        <i class=\"fad fa-spinner-third fa-spin\" *ngIf=\"refreshing\"></i>\r\n                        &nbsp;&nbsp;\r\n                        <span style=\"font-weight: 700;\"><a (click)=\"navigate('ap')\">Access Point/{{apDevice}}</a></span>\r\n                        <span class=\"connected-icon\" *ngIf=\"apConnDetails != null && apConnDetails.primaryAddress != null\">\r\n                            <i class=\"fad fa-network-wired fa-lg\" *ngIf=\"connectedOn(apConnDetails)\"></i>\r\n                            <i class=\"fad fa-check-circle fa-lg\" *ngIf=\"!connectedOn(apConnDetails)\"></i>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n    \r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">SSID&nbsp;\r\n                        [<i class=\"fad fa-lock-alt\"></i>]:</div>\r\n                    <div class=\"col-10\">{{apConnDetails.ssid != null ? apConnDetails.ssid : 'NONE'}}&nbsp;[{{apConnDetails.security == null ? 'NONE' : apConnDetails.security}}]</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">Type:</div>\r\n                    <div class=\"col-10\">{{apConnDetails.type}}</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">Mode:</div>\r\n                    <div class=\"col-10\">{{apConnDetails.mode}}</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">MAC:</div>\r\n                    <div class=\"col-10\">{{apConnDetails.mac}}</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">IP:</div>\r\n                    <div class=\"col-10\">{{apConnDetails.primaryAddress}}&nbsp;[{{apConnDetails.method == 'auto' ? 'DHCP' : 'Static'}}]</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">Broadcast:</div>\r\n                    <div class=\"col-10\">{{apConnDetails.broadcast_address}}</div>\r\n                </div>    \r\n            </div>            \r\n        </div>\r\n    </div>\r\n    <!--END AP MODE-->\r\n    <div class=\"boha-info-panel\" [ngClass]=\"{'connected-on' : isIPAddress == hostIPAddress}\">\r\n        <!--<h3>Home - Active Settings</h3>-->\r\n        <!--INFRASTRUCTURE DETAILS-->\r\n        <div class=\"boha-info-list\">\r\n            <div class=\"boha-relative-internal-container\">\r\n                <div class=\"boha-overlay-action-panel\" *ngIf=\"!refreshing && !isIPAddress.length > 0\">\r\n                    <a class=\"btn configure-link\" href=\"#connect\">\r\n                        <i class=\"fas fa-house-signal fa-lg\"></i>&nbsp;\r\n                        Click here to connect your Workstation to a WiFi Network\r\n                    </a>\r\n                </div>\r\n                <div class=\"row\" style=\"height:40px\">\r\n                    <div class=\"col\">\r\n                        <i class=\"fad fa-lg\" *ngIf=\"!refreshing\" \r\n                        [ngClass]=\"{'fa-wifi' : iStructureDetails.type =='802-11-wireless', 'fa-ethernet' : iStructureDetails.type =='802-3-ethernet'}\"></i>\r\n                        <i class=\"fad fa-spinner-third fa-spin\" *ngIf=\"refreshing\"></i>\r\n                        &nbsp;&nbsp;\r\n                            <span style=\"font-weight: 700;\"><a (click)=\"navigate('is')\">Infrastructure/{{isDevice}}</a></span>\r\n                            <span class=\"connected-icon\" *ngIf=\"iStructureDetails != null && iStructureDetails.primaryAddress != null\">\r\n                            <i class=\"fad fa-network-wired fa-lg\" *ngIf=\"connectedOn(iStructureDetails)\"></i>\r\n                            <i class=\"fad fa-check-circle fa-lg\" *ngIf=\"!connectedOn(iStructureDetails)\"></i>\r\n                        </span>\r\n                    </div>\r\n                </div>\r\n     \r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">SSID&nbsp;\r\n                        [<i class=\"fad fa-lock-alt\"></i>]:</div>\r\n                    <div class=\"col-10\">{{iStructureDetails.ssid != null ? iStructureDetails.ssid : 'NONE' }}&nbsp;[{{iStructureDetails.security == null ? 'NONE' : iStructureDetails.security}}]</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">Type:</div>\r\n                    <div class=\"col-10\">{{iStructureDetails.type}}</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">Mode:</div>\r\n                    <div class=\"col-10\">{{iStructureDetails.mode}}</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">MAC:</div>\r\n                    <div class=\"col-10\">{{iStructureDetails.mac}}</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">IP:</div>\r\n                    <div class=\"col-10\">{{iStructureDetails.primaryAddress}}&nbsp;[{{iStructureDetails.method == 'auto' ? 'DHCP' : 'Static'}}]</div>\r\n                </div>\r\n                <div class=\"row\">\r\n                    <div class=\"col-2\">Broadcast:</div>\r\n                    <div class=\"col-10\">{{iStructureDetails.broadcast_address}}</div>\r\n                </div>    \r\n            </div>\r\n           \r\n        \r\n       </div>\r\n    </div>\r\n    <div class=\"boha-info-panel boha-info-list\" style=\"min-height: 60px;\">\r\n        <div class=\"row boha-info-panel-header-row\">\r\n            <div class=\"col\">\r\n                <i class=\"fad fa-magic fa-lg\"></i>&nbsp;&nbsp;Avahi \r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-2\">Name:</div>\r\n            <div class=\"col-10\">{{avahiService.serviceName}}</div>\r\n        </div>\r\n        \r\n    </div>\r\n    <div class=\"boha-info-panel boha-info-list\" style=\"min-height: 60px;\" *ngIf=\"isTestMode\">\r\n        <div class=\"row boha-info-panel-header-row\">\r\n            <div class=\"col\">\r\n                Connectivity Testing \r\n            </div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-3\">hrefs</div>\r\n            <div class=\"col-3\">Requests</div>\r\n            <div class=\"col-3\">Bytes Received</div>\r\n            <div class=\"col-3\">Errors</div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col-3\">{{windowLoads}}</div>\r\n            <div class=\"col-3\">{{remoteRequestCount}}</div>\r\n            <div class=\"col-3\">{{remoteByteCount}}</div>\r\n            <div class=\"col-3\">{{remoteErrorCount}}</div>\r\n        </div>\r\n        <div class=\"row\">\r\n            <div class=\"col\"><button class=\"btn btn-primary\" style=\"float: right\" (click)=\"startStopTest()\">{{testInterval != null ? 'Stop' : 'Start'}}</button></div>\r\n        </div>\r\n    </div>\r\n    \r\n</div>\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/home/home.component.ts":
/*!****************************************!*\
  !*** ./src/app/home/home.component.ts ***!
  \****************************************/
/*! exports provided: HomeComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "HomeComponent", function() { return HomeComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_services_settings_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/services/settings.service */ "./src/app/common/services/settings.service.ts");
/* harmony import */ var _common_services_connectivity_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/services/connectivity.service */ "./src/app/common/services/connectivity.service.ts");
/* harmony import */ var _common_services_loading_service__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/services/loading.service */ "./src/app/common/services/loading.service.ts");
/* harmony import */ var _common_data_connection_details__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/data/connection.details */ "./src/app/common/data/connection.details.ts");
/* harmony import */ var _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/services/page.header.service */ "./src/app/common/services/page.header.service.ts");
/* harmony import */ var _common_data_avahi_service__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/data/avahi.service */ "./src/app/common/data/avahi.service.ts");
/* harmony import */ var _common_services_system_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../common/services/system.service */ "./src/app/common/services/system.service.ts");
/* harmony import */ var rxjs__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! rxjs */ "./node_modules/rxjs/_esm5/index.js");
/* harmony import */ var _common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/services/global-helper-functions */ "./src/app/common/services/global-helper-functions.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};










var HomeComponent = /** @class */ (function () {
    /////////////////////////////////////////////////////////////
    function HomeComponent(settingsService, connectivityService, loadingService, pageHeaderService, systemConfigService) {
        this.settingsService = settingsService;
        this.connectivityService = connectivityService;
        this.loadingService = loadingService;
        this.pageHeaderService = pageHeaderService;
        this.systemConfigService = systemConfigService;
        this.NOTCONNECTED = "Not connected";
        this.refreshing = false;
        this.longPollHandle = null;
        ///////////////Connectivity Testing///////////////////////////
        this.remoteByteCount = 0;
        this.remoteErrorCount = 0;
        this.remoteRequestCount = 0;
        this.windowLoads = 0;
        this.testInterval = null;
        this.isTestMode = false;
        this.iStructureDetails = new _common_data_connection_details__WEBPACK_IMPORTED_MODULE_4__["ConnectionDetails"]();
        this.apConnDetails = new _common_data_connection_details__WEBPACK_IMPORTED_MODULE_4__["ConnectionDetails"]();
        this.connectionMode = 0 /* NONE */;
        this.refreshing = false;
        this.avahiService = new _common_data_avahi_service__WEBPACK_IMPORTED_MODULE_6__["AvahiService"]();
    }
    ;
    ;
    HomeComponent.prototype.ngOnInit = function () {
        var _this = this;
        console.log("Home init....");
        this.updateConnectionData();
        this.updateAvahiData();
        this.getMode();
        this.pageHeaderService.autoRefreshClicked.subscribe(function (refresh) {
            if (refresh && _this.longPollHandle == null) {
                _this.longPollHandle = setInterval(function () {
                    console.log("Refreshing....");
                    _this.updateConnectionData();
                }, 5000);
            }
            else {
                if (_this.longPollHandle != null) {
                    clearInterval(_this.longPollHandle);
                    _this.longPollHandle = null;
                }
            }
        });
        if (this.isTestMode) {
            this.startConnectivityTest();
        }
    };
    HomeComponent.prototype.ngAfterViewInit = function () {
        console.log("Home view init....");
        this.pageHeaderService.setHeaderTitle("Home (configuration overview)", "fa-home-alt");
    };
    HomeComponent.prototype.ngDestroy = function () {
        this.pageHeaderService.autoRefreshClicked.unsubscribe();
        if (this.longPollHandle) {
            clearInterval(this.longPollHandle);
        }
    };
    ///////////////Connectivity Testing///////////////////////////
    HomeComponent.prototype.getMode = function () {
        return __awaiter(this, void 0, void 0, function () {
            var mode, error_1;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 2, , 3]);
                        return [4 /*yield*/, this.settingsService.discreteSetting_async("/mode")];
                    case 1:
                        mode = _a.sent();
                        this.isTestMode = (mode != null && mode.data == "debug");
                        return [3 /*break*/, 3];
                    case 2:
                        error_1 = _a.sent();
                        console.log(error_1);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.startStopTest = function () {
        if (this.testInterval == null) {
            this.startConnectivityTest();
        }
        else {
            this.stopConnectivityTest();
        }
    };
    HomeComponent.prototype.stopConnectivityTest = function () {
        if (this.testInterval) {
            clearInterval(this.testInterval);
            this.testInterval = null;
        }
    };
    HomeComponent.prototype.startConnectivityTest = function () {
        var _this = this;
        if (this.testInterval == null) {
            console.log("Starting connectivity test...");
            var testwindow_1 = window.open("about:blank");
            var urls_1 = ["http://www.google.com", "http://www.microsoft.com", "http://www.boston.com", "https://www.raspberrypi.org/"];
            this.testInterval = setInterval(function () {
                var count = Math.floor(Math.random() * Math.floor(5));
                var urlindex = Math.floor(Math.random() * Math.floor(3));
                var reloadWindow = Math.floor(Math.random() * Math.floor(10)) % 2 == 0;
                if (reloadWindow) {
                    var url = urls_1[urlindex];
                    testwindow_1.location.href = url;
                    _this.windowLoads++;
                }
                for (var index = 0; index < count; index++) {
                    _this.remoteRequestCount++;
                    _this.connectivityService.uplinkTest("https://cors-anywhere.herokuapp.com/").subscribe(function (resp) {
                        _this.remoteByteCount += resp.length;
                    }, function (err) {
                        _this.remoteErrorCount++;
                        console.log(err);
                    });
                }
            }, 20000);
        }
    };
    /////////////////////////////////////////////////////////////
    HomeComponent.prototype.evaluateConnectivityMode = function (devices) {
        return __awaiter(this, void 0, void 0, function () {
            var mode, index, device, details, ipAddress, error_2;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        mode = 0 /* NONE */;
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 6, 7, 8]);
                        index = 0;
                        _a.label = 2;
                    case 2:
                        if (!(index < devices.length)) return [3 /*break*/, 5];
                        device = devices[index];
                        console.log(device);
                        if (!(device.state.toLowerCase() === "connected")) return [3 /*break*/, 4];
                        return [4 /*yield*/, this.connectivityService.deviceDetails_async(device.device)];
                    case 3:
                        details = _a.sent();
                        ipAddress = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_9__["parsePrimaryIp"])(details.ip4address_1);
                        if (ipAddress == this.hostIPAddress) {
                            if (this.isDevice == device.device) {
                                if (device.type == "wifi") {
                                    mode = 2 /* Wifi */;
                                }
                                else if (device.type == "ethernet") {
                                    mode = 3 /* Ethernet */;
                                }
                            }
                            else if (this.apDevice == device.device) {
                                if (device.type == "wifi") {
                                    mode = 1 /* AP */;
                                }
                                else if (device.type == "ethernet") {
                                    mode = 3 /* Ethernet */;
                                }
                            }
                            else {
                                if (device.type == "wifi") {
                                    mode = 2 /* Wifi */;
                                }
                                else if (device.type == "ethernet") {
                                    mode = 3 /* Ethernet */;
                                }
                            }
                        }
                        _a.label = 4;
                    case 4:
                        index++;
                        return [3 /*break*/, 2];
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        error_2 = _a.sent();
                        console.log(error_2);
                        return [3 /*break*/, 8];
                    case 7: return [2 /*return*/, mode];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.getAvahiServiceName = function () {
        var _this = this;
        return new Promise(function (resolve, reject) {
            _this.systemConfigService.getAvahiName().subscribe(function (resp) {
                resolve(resp);
            }, function (err) {
                reject(err);
            });
        });
    };
    HomeComponent.prototype.updateAvahiData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, error_3;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 2, , 3]);
                        _a = this;
                        return [4 /*yield*/, this.getAvahiServiceName()];
                    case 1:
                        _a.avahiService = _b.sent();
                        return [3 /*break*/, 3];
                    case 2:
                        error_3 = _b.sent();
                        console.log("An error occured while retrieving the avahi service name. " + error_3.error);
                        return [3 /*break*/, 3];
                    case 3: return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.updateConnectionData = function () {
        return __awaiter(this, void 0, void 0, function () {
            var devices, connectionSettings, hostIPAddress, error_4, mode;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        devices = [];
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 7, 8, 10]);
                        this.refreshing = true;
                        return [4 /*yield*/, this.settingsService.connections_async()];
                    case 2:
                        connectionSettings = _a.sent();
                        return [4 /*yield*/, this.connectivityService.devices_async()];
                    case 3:
                        devices = _a.sent();
                        return [4 /*yield*/, this.connectivityService.hostConnectionEcho_async()];
                    case 4:
                        hostIPAddress = _a.sent();
                        this.hostIPAddress = hostIPAddress;
                        return [4 /*yield*/, this.updateAPConnectionData(devices, connectionSettings)];
                    case 5:
                        _a.sent();
                        return [4 /*yield*/, this.updateISConnectionData(devices, connectionSettings)];
                    case 6:
                        _a.sent();
                        return [3 /*break*/, 10];
                    case 7:
                        error_4 = _a.sent();
                        if (error_4.error == undefined) {
                            console.log(error_4);
                        }
                        else {
                            console.log("An error occured while updating the data. " + error_4.error);
                        }
                        return [3 /*break*/, 10];
                    case 8: return [4 /*yield*/, this.evaluateConnectivityMode(devices)];
                    case 9:
                        mode = _a.sent();
                        this.setConnectivityMode(mode);
                        this.refreshing = false;
                        return [7 /*endfinally*/];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.updateISConnectionData = function (devices, connections) {
        return __awaiter(this, void 0, void 0, function () {
            var isConnection_1, isDevice, isConnectionDetails, error_5;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        this.isIPAddress = "";
                        this.isDevice = "";
                        this.iStructureDetails = new _common_data_connection_details__WEBPACK_IMPORTED_MODULE_4__["ConnectionDetails"]();
                        if (!(devices != null && devices != undefined)) return [3 /*break*/, 3];
                        isConnection_1 = connections.find(function (connsetting) { return connsetting.mode == "is"; });
                        isDevice = devices.find(function (d) { return d.device == isConnection_1.adapter; });
                        console.log(isConnection_1);
                        if (!(isDevice != undefined && isDevice != null)) return [3 /*break*/, 2];
                        this.isDevice = isDevice.device;
                        return [4 /*yield*/, this.connectivityService.connectionDetails_async(isDevice.connection)];
                    case 1:
                        isConnectionDetails = _a.sent();
                        this.iStructureDetails = isConnectionDetails;
                        this.isIPAddress = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_9__["parsePrimaryIp"])(this.iStructureDetails.primaryAddress);
                        return [3 /*break*/, 3];
                    case 2:
                        Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["throwError"])("IS Device NOT FOUND!");
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        error_5 = _a.sent();
                        if (error_5.error == undefined) {
                            console.log(error_5);
                        }
                        else {
                            console.log("An error occured while updating the data. " + error_5.error);
                        }
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.updateAPConnectionData = function (devices, connections) {
        return __awaiter(this, void 0, void 0, function () {
            var apConnection_1, apDevice, apConnectionDetails, error_6;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        this.apIPAddress = "";
                        this.apDevice = null;
                        this.apConnDetails = new _common_data_connection_details__WEBPACK_IMPORTED_MODULE_4__["ConnectionDetails"]();
                        if (!(devices != null && devices != undefined)) return [3 /*break*/, 3];
                        apConnection_1 = connections.find(function (connsetting) { return connsetting.mode == "ap"; });
                        console.log(apConnection_1);
                        apDevice = devices.find(function (d) { return d.device == apConnection_1.adapter; });
                        if (!(apDevice != undefined && apDevice != null)) return [3 /*break*/, 2];
                        this.apDevice = apDevice.device;
                        return [4 /*yield*/, this.connectivityService.connectionDetails_async(apDevice.connection)];
                    case 1:
                        apConnectionDetails = _a.sent();
                        this.apConnDetails = apConnectionDetails;
                        this.apIPAddress = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_9__["parsePrimaryIp"])(this.apConnDetails.primaryAddress);
                        return [3 /*break*/, 3];
                    case 2:
                        Object(rxjs__WEBPACK_IMPORTED_MODULE_8__["throwError"])({ error: "AP Device NOT FOUND!" });
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        error_6 = _a.sent();
                        if (error_6.error == undefined) {
                            console.log(error_6);
                        }
                        else {
                            console.log("An error occured while updating the data. " + error_6.error);
                        }
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    HomeComponent.prototype.setConnectivityMode = function (mode) {
        this.connectionMode = mode;
        console.log("Connection mode");
        console.log(mode);
        this.pageHeaderService.setConnectivityIconState(mode);
    };
    HomeComponent.prototype.connectedOn = function (details) {
        if (details != null && details != undefined) {
            return (Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_9__["parsePrimaryIp"])(details.primaryAddress) == this.hostIPAddress);
        }
        else {
            return false;
        }
    };
    HomeComponent.prototype.navigate = function (to) {
        if (to === "is") {
            var location_1 = "http://" + this.isIPAddress + ":1337";
            window.location.href = location_1;
        }
        else if (to === "ap") {
            var location_2 = "http://" + this.apIPAddress + ":1337";
            window.location.href = location_2;
        }
    };
    HomeComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-home',
            template: __webpack_require__(/*! ./home.component.html */ "./src/app/home/home.component.html"),
            styles: [__webpack_require__(/*! ./home.component.css */ "./src/app/home/home.component.css")]
        }),
        __metadata("design:paramtypes", [_common_services_settings_service__WEBPACK_IMPORTED_MODULE_1__["SettingsService"],
            _common_services_connectivity_service__WEBPACK_IMPORTED_MODULE_2__["ConnectivityService"], _common_services_loading_service__WEBPACK_IMPORTED_MODULE_3__["LoadingService"],
            _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_5__["PageHeaderService"], _common_services_system_service__WEBPACK_IMPORTED_MODULE_7__["SystemService"]])
    ], HomeComponent);
    return HomeComponent;
}());



/***/ }),

/***/ "./src/app/login/login.component.css":
/*!*******************************************!*\
  !*** ./src/app/login/login.component.css ***!
  \*******************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ".login-container{\r\n    margin-top: 10px;\r\n    /*new*/\r\n    /*max-width: 380px*/\r\n    /*margin-left: calc(25% - 170px)*/\r\n    margin-top: 10px;\r\n    box-shadow: -2px 4px 3px var(--boha-neutral-gray-light);\r\n    background-color:  white;\r\n    border: 1px solid var(--boha-neutral-gray-light);\r\n    border-radius: 5px;\r\n    padding: 20px;\r\n  }\r\n  \r\n  .error{\r\n    color: var(--boha-secondary-red); \r\n    font-size: 12px; \r\n    padding-top: 4px\r\n  }"

/***/ }),

/***/ "./src/app/login/login.component.html":
/*!********************************************!*\
  !*** ./src/app/login/login.component.html ***!
  \********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<div class=\"boha-child-page\">\r\n  <pop-up-message>\r\n    \r\n  </pop-up-message>\r\n  <div class=\"container\">\r\n      <form name=\"login-form\">\r\n        <div class=\"login-container\">\r\n         <div class=\"row\">\r\n            <div class=\"col\">\r\n              <div class=\"form-group\">\r\n                <i class=\"fas fa-user fa-lg\"></i>&nbsp;<label id=\"login-username-label\" style=\"font-weight: 600;\">Username</label>\r\n                <input type=\"text\" name=\"login-username\" [(ngModel)]=\"username\" required class=\"form-control\" placeholder=\"username\">\r\n                <div *ngIf=\"missingusername\" class=\"error\" id=\"login-missing-username-error\">Missing the username!</div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n          <div class=row>\r\n            <div class=\"col\">\r\n              <div class=\"form-group\">\r\n                <i class=\"fas fa-key fa-lg\"></i>&nbsp;<label id=\"login-password-label\" style=\"font-weight: 600;\">Password</label>\r\n                <input type=\"password\" name=\"login-password\" [(ngModel)]=\"password\" required class=\"form-control\" placeholder=\"password\">\r\n                <div *ngIf=\"missingpassword\" class=\"error\" id=\"missing-password-error\">Missing the password!</div>\r\n              </div>\r\n            </div>\r\n          </div>  \r\n          <div class=\"row\">\r\n              <div class=\"col\">\r\n              <div class=\"form-group\">\r\n                <button (click)=\"authenticate($event)\"  class=\"btn boha-btn-primary\" style=\"float: right;\">Login</button><!--[disabled]=\"!verifyInputs()\" -->\r\n                <div class=\"clearfix\"></div>\r\n              </div>\r\n            </div>\r\n          </div>\r\n        \r\n        </div>\r\n      </form>\r\n    </div>\r\n</div>\r\n"

/***/ }),

/***/ "./src/app/login/login.component.ts":
/*!******************************************!*\
  !*** ./src/app/login/login.component.ts ***!
  \******************************************/
/*! exports provided: LoginComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "LoginComponent", function() { return LoginComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_services_auth_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/services/auth.service */ "./src/app/common/services/auth.service.ts");
/* harmony import */ var _common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/services/route.guard.service */ "./src/app/common/services/route.guard.service.ts");
/* harmony import */ var _common_data_user_credentials__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/data/user.credentials */ "./src/app/common/data/user.credentials.ts");
/* harmony import */ var _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/services/page.header.service */ "./src/app/common/services/page.header.service.ts");
/* harmony import */ var _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/ui-widgets/pop-up-message/pop-up-message.component */ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};






var LoginComponent = /** @class */ (function () {
    function LoginComponent(authService, routeGaurdService, pageHeaderService) {
        this.pageHeaderService = pageHeaderService;
        this.authenticationService = authService;
        this.routerGuardService = routeGaurdService;
        this.missingusername = false;
        this.missingpassword = false;
        this.submitted = false;
        this.username = "";
        this.password = "";
    }
    LoginComponent.prototype.ngOnInit = function () {
        this.routerGuardService.setAuthenticationFlag("admin", "", false, false);
    };
    LoginComponent.prototype.ngAfterViewInit = function () {
        console.log("Login view init....");
        this.pageHeaderService.setHeaderTitle("Login", 'fa-user');
    };
    LoginComponent.prototype.ngDestroy = function () {
    };
    LoginComponent.prototype.clearErrorStatus = function () {
        this.missingpassword = false;
        this.missingusername = false;
    };
    //call the authenicate API from the auth service and set the appropriate memeber variables 
    LoginComponent.prototype.authenticate = function (event) {
        var _this = this;
        event.preventDefault();
        this.missingusername = this.username === '';
        this.missingpassword = this.password === '';
        try {
            if (!this.missingpassword && !this.missingpassword) {
                var credentials = new _common_data_user_credentials__WEBPACK_IMPORTED_MODULE_3__["UserCredentials"];
                credentials.username = this.username;
                credentials.password = this.password;
                this.authenticationService.authenticate(credentials).subscribe(function (resp) {
                    console.log(_this.user);
                    _this.user = resp;
                    _this.routerGuardService.setAuthenticationFlag(_this.user.username, _this.user.sessionToken, _this.user.isAuthenticated, true);
                }, function (err) {
                    _this.routerGuardService.setAuthenticationFlag("", "", false, false);
                    _this.missingpassword = false;
                    _this.missingusername = false;
                    _this.popup.displayMessage(false, "Error authenticating the supplied user. Invalid credentials. " + err.error);
                    console.log("error authenticating");
                    setTimeout(function () {
                        _this.clearErrorStatus();
                    }, 5000);
                });
            }
            else {
                this.popup.displayMessage(false, "Username and password are required!");
                setTimeout(function () {
                    _this.clearErrorStatus();
                }, 5000);
            }
            console.log(event);
        }
        catch (err) {
            console.log(err);
        }
    };
    //Deprcated function
    LoginComponent.prototype.verifyInputs = function () {
        //Depricated for now
        return !(this.missingusername || this.missingpassword);
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_5__["PopUpMessageComponent"]),
        __metadata("design:type", _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_5__["PopUpMessageComponent"])
    ], LoginComponent.prototype, "popup", void 0);
    LoginComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-login',
            template: __webpack_require__(/*! ./login.component.html */ "./src/app/login/login.component.html"),
            styles: [__webpack_require__(/*! ./login.component.css */ "./src/app/login/login.component.css")]
        }),
        __metadata("design:paramtypes", [_common_services_auth_service__WEBPACK_IMPORTED_MODULE_1__["AuthService"],
            _common_services_route_guard_service__WEBPACK_IMPORTED_MODULE_2__["RouteGuardService"], _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_4__["PageHeaderService"]])
    ], LoginComponent);
    return LoginComponent;
}());



/***/ }),

/***/ "./src/app/system/system.component.css":
/*!*********************************************!*\
  !*** ./src/app/system/system.component.css ***!
  \*********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = ""

/***/ }),

/***/ "./src/app/system/system.component.html":
/*!**********************************************!*\
  !*** ./src/app/system/system.component.html ***!
  \**********************************************/
/*! no static exports found */
/***/ (function(module, exports) {

module.exports = "<!--\r\n    text-align: center;\r\n    margin-top: 10px;\r\n    border: 1px solid var(--boha-neutral-gray-medium);\r\n    border-radius: 5px;\r\n    min-height: 300px;\r\n    width: 100%;\r\n\r\n    h3\r\n    font-size: 1.75rem;\r\n    /* background-color: var(--boha-primary-light-blue); */\r\n    border-radius: 5px 5px 0 0;\r\n    border-bottom: 1px solid var(--boha-neutral-gray-medium);\r\n    padding: 5px;\r\n\r\n-->\r\n<div class=\"boha-child-page\">\r\n  <pop-up-message>\r\n  </pop-up-message>\r\n  <modal-confirmation>\r\n\r\n    </modal-confirmation>\r\n  <div class=\"boha-info-panel form-group\" >\r\n      <div class=\"boha-info-panel-input-container\">\r\n          <div class=\"row\" class=\"boha-info-panel-header-row\">\r\n              <div class=\"col\" style=\"padding: 0px;\" >\r\n                  Proxy\r\n              </div>\r\n        </div>\r\n          <div class=\"row boha-info-panel-input-row\">\r\n              <div class=\"col-3 boha-info-panel-label-col\"> \r\n              Proxy:\r\n              </div>\r\n              <div class=\"col-9\">\r\n                    <input type=\"text\" placeholder=\"Proxy Server\" \r\n                    aria-label=\"Proxy Server Label Label\" class=\"form-control\" [(ngModel)]=\"proxySetting\" (keydown)=\"proxySettingKeyEvent($event)\"/>\r\n              </div>\r\n          </div>\r\n          <div class=\"row\" style=\"line-height: 55px; height: 55px;\">\r\n              <div class=\"col\" style=\"margin-top: 10px;\">\r\n                  <button class=\"btn boha-btn-primary\" type=\"submit\"  style=\"float: right; min-width: 100px;\" \r\n                  (click)=\"saveProxySettings()\" [disabled]=\"!proxySettingsTouched\"><i class=\"fad fa-save\" ></i>&nbsp;Save</button>\r\n              </div>\r\n          </div>\r\n      </div>\r\n     \r\n  </div>\r\n  <!--------------------------------AP Mode------------------------->\r\n  <div class=\"boha-info-panel form-group\">\r\n    <div class=\"boha-info-panel-input-container\">\r\n        <div class=\"row\" class=\"boha-info-panel-header-row\">\r\n            <div class=\"col\" style=\"padding: 0px;\">\r\n                (AP) Networking\r\n                <a (click)=\"toggleAPManual()\" style=\"float: right;position:relative\" id=\"ap-manual-toggle\">\r\n                    <span style=\"font-weight: 400;position: absolute;left:-70px;top:-5px\" *ngIf=\"apManualIP\">Manual&nbsp;&nbsp;</span>\r\n                    <span style=\"font-weight: 400;position: absolute;left:-70px;top:-5px\" *ngIf=\"!apManualIP\">Dynamic&nbsp;&nbsp;</span>\r\n                   <i class=\"fad fa-2x fa-toggle-on\" *ngIf=\"apManualIP\" style=\"color: var( --boha-primary-natural-blue);\"></i> \r\n                    <i class=\"fad fa-2x fa-toggle-off\" *ngIf=\"!apManualIP\" style=\"color:var(--boha-neutral-gray-medium);\"></i> \r\n                </a>\r\n            </div>\r\n        </div>\r\n        <div class=\"row boha-info-panel-input-row\">\r\n            <div class=\"col-3 boha-info-panel-label-col\"> \r\n            IP Address:\r\n            </div>\r\n            <div class=\"col-9\">\r\n                <input type=\"text\" placeholder=\"###.###.###.###\" \r\n                aria-label=\"AP IP Address Label\" class=\"form-control\" [(ngModel)]=\"apIPAddress\" \r\n                (keydown)=\"apAddressKeyEvent($event)\" id=\"system-ap-ip-address\"\r\n                [disabled]=\"!apManualIP\" [ngClass]=\"{'boha-input-error' : apIPFieldError}\"/>\r\n\r\n            </div>\r\n            \r\n        </div>\r\n        <!--ERROR ROW-->\r\n        <div class=\"row\" style=\"color: var(--boha-primary-red)\" *ngIf=\"apIPFieldError\">\r\n            <div class=\"col-3\">**********</div><div class=\"col-9\" >Invalid IP address format detected.</div>\r\n        </div>\r\n        <div class=\"row boha-info-panel-input-row\">\r\n            <div class=\"col-3 boha-info-panel-label-col\"> \r\n            Subnet:\r\n            </div>\r\n            <div class=\"col-9\">\r\n                <input type=\"text\" placeholder=\"###.###.###.###\" \r\n                aria-label=\"AP IP Address Label\" class=\"form-control\" [(ngModel)]=\"apSubnetAddress\"\r\n                (keydown)=\"apAddressKeyEvent($event)\"  \r\n                [disabled]=\"!apManualIP\" [ngClass]=\"{'boha-input-error' : apSubnetFieldError}\"/>\r\n            </div>\r\n        </div>\r\n        <!--ERROR ROW-->\r\n        <div class=\"row\" style=\"color: var(--boha-primary-red)\" *ngIf=\"apSubnetFieldError\">\r\n            <div class=\"col-3\">**********</div><div class=\"col-9\" >Invalid subnet address format detected.</div>\r\n        </div>\r\n        <div class=\"row boha-info-panel-input-row\">\r\n            <div class=\"col-3 boha-info-panel-label-col\"> \r\n            Default Gateway:\r\n            </div>\r\n            <div class=\"col-9\">\r\n                <input type=\"text\" placeholder=\"###.###.###.###\" \r\n                aria-label=\"AP IP Address Label\" class=\"form-control\" [(ngModel)]=\"apGatewayAddress\"\r\n                 (keydown)=\"apAddressKeyEvent($event)\"  \r\n                [disabled]=\"!apManualIP\" [ngClass]=\"{'boha-input-error' : apGatewayFieldError}\"/>\r\n            </div>\r\n        </div>\r\n        <!--ERROR ROW-->\r\n        <div class=\"row\" style=\"color: var(--boha-primary-red)\" *ngIf=\"apGatewayFieldError\">\r\n            <div class=\"col-3\">**********</div><div class=\"col-9\" >Invalid gateway address format detected.</div>\r\n        </div>\r\n        <div class=\"row\" style=\"line-height: 55px; height: 55px;\">\r\n            <div class=\"col\" style=\"margin-top: 10px;\">\r\n                <button class=\"btn boha-btn-primary\" type=\"submit\"  style=\"float: right; min-width: 100px;\" \r\n                (click)=\"preCheckConnectionData('ap')\" [disabled]=\"!apNetworkingSettingsTouched\"><i class=\"fad fa-save\" ></i>&nbsp;Save</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n  </div>\r\n  <!--------------------------------Infrastructure Mode------------------------->\r\n  <div class=\"boha-info-panel form-group\">\r\n    <div class=\"boha-info-panel-input-container\">\r\n        <div class=\"row\" class=\"boha-info-panel-header-row\">\r\n            <div class=\"col\" style=\"padding: 0px;\">\r\n                (IS) Networking\r\n                <a (click)=\"toggleISManual()\" style=\"float: right;position:relative\" id=\"is-manual-toggle\">\r\n                    <span style=\"font-weight: 400;position: absolute;left:-70px;top:-5px\" *ngIf=\"isManualIP\">Manual&nbsp;&nbsp;</span>\r\n                    <span style=\"font-weight: 400;position: absolute;left:-70px;top:-5px\" *ngIf=\"!isManualIP\">Dynamic&nbsp;&nbsp;</span>\r\n                    <i class=\"fad fa-2x fa-toggle-on\" *ngIf=\"isManualIP\" style=\"color: var( --boha-primary-natural-blue);\"></i> \r\n                    <i class=\"fad fa-2x fa-toggle-off\" *ngIf=\"!isManualIP\" style=\"color:var(--boha-neutral-gray-medium);\"></i> \r\n                </a>\r\n            \r\n            </div>\r\n    \r\n        </div>\r\n        <div class=\"row boha-info-panel-input-row\">\r\n            <div class=\"col-3 boha-info-panel-label-col\"> \r\n            IP Address:\r\n            </div>\r\n            <div class=\"col-9\">\r\n                <input type=\"text\" placeholder=\"###.###.###.###\" \r\n                aria-label=\"AP IP Address Label\" class=\"form-control\" [(ngModel)]=\"isIPAddress\" \r\n                 (keydown)=\"isAddressKeyEvent($event, isIPAddress, setISIPAddress)\"\r\n                [disabled]=\"!isManualIP\" [ngClass]=\"{'boha-input-error' : isIPFieldError}\"/>\r\n            </div>\r\n        </div>\r\n        <!--ERROR ROW-->\r\n        <div class=\"row\" style=\"color: var(--boha-primary-red)\" *ngIf=\"isIPFieldError\">\r\n            <div class=\"col-3\">**********</div><div class=\"col-9\" >Invalid IP address format detected.</div>\r\n        </div>\r\n        <div class=\"row boha-info-panel-input-row\">\r\n            <div class=\"col-3 boha-info-panel-label-col\"> \r\n            Subnet:\r\n            </div>\r\n            <div class=\"col-9\">\r\n                <input type=\"text\" placeholder=\"###.###.###.###\" \r\n                aria-label=\"AP IP Address Label\" class=\"form-control\" [(ngModel)]=\"apSubnetAddress\" \r\n                (keydown)=\"isAddressKeyEvent($event)\"\r\n                [disabled]=\"!isManualIP\" [ngClass]=\"{'boha-input-error' : isSubnetFieldError}\"/>\r\n            </div>\r\n        </div>\r\n        <!--ERROR ROW-->\r\n        <div class=\"row\" style=\"color: var(--boha-primary-red)\" *ngIf=\"isSubnetFieldError\">\r\n            <div class=\"col-3\">**********</div><div class=\"col-9\" >Invalid subnet address format detected.</div>\r\n        </div>\r\n        <div class=\"row boha-info-panel-input-row\">\r\n            <div class=\"col-3 boha-info-panel-label-col\"> \r\n            Default Gateway:\r\n            </div>\r\n            <div class=\"col-9\">\r\n                <input type=\"text\" placeholder=\"###.###.###.###\" \r\n                aria-label=\"AP IP Address Label\" class=\"form-control\" [(ngModel)]=\"isGatewayAddress\" \r\n                (keydown)=\"isAddressKeyEvent($event)\"  \r\n                [disabled]=\"!isManualIP\" [ngClass]=\"{'boha-input-error' : isGatewayFieldError}\"/>\r\n            </div>\r\n        </div>\r\n        <!--ERROR ROW-->\r\n        <div class=\"row\" style=\"color: var(--boha-primary-red)\" *ngIf=\"isGatewayFieldError\">\r\n            <div class=\"col-3\">**********</div><div class=\"col-9\" >Invalid gateway address format detected.</div>\r\n        </div>\r\n        <div class=\"row\" style=\"line-height: 55px; height: 55px;\">\r\n            <div class=\"col\" style=\"margin-top: 10px;\">\r\n                <button class=\"btn boha-btn-primary\" type=\"submit\"  style=\"float: right; min-width: 100px;\" \r\n                (click)=\"preCheckConnectionData('is')\" [disabled]=\"!isNetworkingSettingsTouched\"><i class=\"fad fa-save\" ></i>&nbsp;Save</button>\r\n            </div>\r\n        </div>\r\n    </div>\r\n  </div>\r\n  \r\n</div>\r\n\r\n\r\n\r\n\r\n"

/***/ }),

/***/ "./src/app/system/system.component.ts":
/*!********************************************!*\
  !*** ./src/app/system/system.component.ts ***!
  \********************************************/
/*! exports provided: SystemComponent */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "SystemComponent", function() { return SystemComponent; });
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _common_services_page_header_service__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! ../common/services/page.header.service */ "./src/app/common/services/page.header.service.ts");
/* harmony import */ var _common_services_system_service__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ../common/services/system.service */ "./src/app/common/services/system.service.ts");
/* harmony import */ var _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ../common/ui-widgets/pop-up-message/pop-up-message.component */ "./src/app/common/ui-widgets/pop-up-message/pop-up-message.component.ts");
/* harmony import */ var _common_data_http_proxy__WEBPACK_IMPORTED_MODULE_4__ = __webpack_require__(/*! ../common/data/http.proxy */ "./src/app/common/data/http.proxy.ts");
/* harmony import */ var _common_services_connectivity_service__WEBPACK_IMPORTED_MODULE_5__ = __webpack_require__(/*! ../common/services/connectivity.service */ "./src/app/common/services/connectivity.service.ts");
/* harmony import */ var _common_data_ip_connection_method__WEBPACK_IMPORTED_MODULE_6__ = __webpack_require__(/*! ../common/data/ip.connection.method */ "./src/app/common/data/ip.connection.method.ts");
/* harmony import */ var _common_services_settings_service__WEBPACK_IMPORTED_MODULE_7__ = __webpack_require__(/*! ../common/services/settings.service */ "./src/app/common/services/settings.service.ts");
/* harmony import */ var _common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__ = __webpack_require__(/*! ../common/services/global-helper-functions */ "./src/app/common/services/global-helper-functions.ts");
/* harmony import */ var _common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_9__ = __webpack_require__(/*! ../common/ui-widgets/modal-confirmation/modal-confirmation.component */ "./src/app/common/ui-widgets/modal-confirmation/modal-confirmation.component.ts");
/* harmony import */ var _common_services_loading_service__WEBPACK_IMPORTED_MODULE_10__ = __webpack_require__(/*! ../common/services/loading.service */ "./src/app/common/services/loading.service.ts");
var __decorate = (undefined && undefined.__decorate) || function (decorators, target, key, desc) {
    var c = arguments.length, r = c < 3 ? target : desc === null ? desc = Object.getOwnPropertyDescriptor(target, key) : desc, d;
    if (typeof Reflect === "object" && typeof Reflect.decorate === "function") r = Reflect.decorate(decorators, target, key, desc);
    else for (var i = decorators.length - 1; i >= 0; i--) if (d = decorators[i]) r = (c < 3 ? d(r) : c > 3 ? d(target, key, r) : d(target, key)) || r;
    return c > 3 && r && Object.defineProperty(target, key, r), r;
};
var __metadata = (undefined && undefined.__metadata) || function (k, v) {
    if (typeof Reflect === "object" && typeof Reflect.metadata === "function") return Reflect.metadata(k, v);
};
var __awaiter = (undefined && undefined.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __generator = (undefined && undefined.__generator) || function (thisArg, body) {
    var _ = { label: 0, sent: function() { if (t[0] & 1) throw t[1]; return t[1]; }, trys: [], ops: [] }, f, y, t, g;
    return g = { next: verb(0), "throw": verb(1), "return": verb(2) }, typeof Symbol === "function" && (g[Symbol.iterator] = function() { return this; }), g;
    function verb(n) { return function (v) { return step([n, v]); }; }
    function step(op) {
        if (f) throw new TypeError("Generator is already executing.");
        while (_) try {
            if (f = 1, y && (t = y[op[0] & 2 ? "return" : op[0] ? "throw" : "next"]) && !(t = t.call(y, op[1])).done) return t;
            if (y = 0, t) op = [0, t.value];
            switch (op[0]) {
                case 0: case 1: t = op; break;
                case 4: _.label++; return { value: op[1], done: false };
                case 5: _.label++; y = op[1]; op = [0]; continue;
                case 7: op = _.ops.pop(); _.trys.pop(); continue;
                default:
                    if (!(t = _.trys, t = t.length > 0 && t[t.length - 1]) && (op[0] === 6 || op[0] === 2)) { _ = 0; continue; }
                    if (op[0] === 3 && (!t || (op[1] > t[0] && op[1] < t[3]))) { _.label = op[1]; break; }
                    if (op[0] === 6 && _.label < t[1]) { _.label = t[1]; t = op; break; }
                    if (t && _.label < t[2]) { _.label = t[2]; _.ops.push(op); break; }
                    if (t[2]) _.ops.pop();
                    _.trys.pop(); continue;
            }
            op = body.call(thisArg, _);
        } catch (e) { op = [6, e]; y = 0; } finally { f = t = 0; }
        if (op[0] & 5) throw op[1]; return { value: op[0] ? op[1] : void 0, done: true };
    }
};











var SystemComponent = /** @class */ (function () {
    function SystemComponent(pageHeaderService, systemService, connectivityService, cd, settingsService, loadingService) {
        this.pageHeaderService = pageHeaderService;
        this.systemService = systemService;
        this.connectivityService = connectivityService;
        this.cd = cd;
        this.settingsService = settingsService;
        this.loadingService = loadingService;
        this.proxySetting = "";
        this.apIPAddress = "";
        this.apSubnetAddress = "";
        this.apGatewayAddress = "";
        this.apMethod = "auto";
        this.apNetworkingSettingsTouched = false;
        this.apIPFieldError = false;
        this.apSubnetFieldError = false;
        this.apGatewayFieldError = false;
        this.isIPAddress = "";
        this.isSubnetAddress = "";
        this.isGatewayAddress = "";
        this.isMethod = "auto";
        this.isNetworkingSettingsTouched = false;
        this.isIPFieldError = false;
        this.isSubnetFieldError = false;
        this.isGatewayFieldError = false;
        this.proxySettingsTouched = false;
        this.apManualIP = false;
        this.isManualIP = false;
    }
    SystemComponent.prototype.ngOnInit = function () {
        var _this = this;
        this.systemService.getProxySetting().subscribe(function (resp) {
            _this.proxySetting = resp.httpProxy;
            var parts = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getProxyParts"])(_this.proxySetting.toLowerCase());
            console.log(parts);
            var proxy = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["assembleProxyAddress"])(parts);
            console.log(proxy);
        }, function (err) {
            console.log("Error retrieving proxy setting " + err.error);
        });
        this.getConnectionNames();
    };
    SystemComponent.prototype.ngAfterViewInit = function () {
        console.log("System view init....");
        this.pageHeaderService.setHeaderTitle("Networking and System Configuration", "fa-network-wired");
    };
    SystemComponent.prototype.ngDestroy = function () {
    };
    SystemComponent.prototype.getAPConnectionInfo = function (deviceInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var apConnnection, apcondetails, error_1, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        if (!(deviceInfo != null || deviceInfo != undefined)) return [3 /*break*/, 2];
                        apConnnection = deviceInfo.connection;
                        return [4 /*yield*/, this.connectivityService.connectionDetails_async(apConnnection)];
                    case 1:
                        apcondetails = _a.sent();
                        /**************AP CONNECTIVITY DETAILS*************************** */
                        this.apConnectionName = apcondetails.id;
                        this.apIPAddress = (apcondetails.primaryAddress != null ? Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["parsePrimaryIp"])(apcondetails.primaryAddress) : "NONE");
                        this.apMethod = apcondetails.method;
                        this.apSubnetAddress = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getMaskFromSubnetDot"])(Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["parsePrimaryIp"])(apcondetails.primaryAddress, false).toString());
                        this.apGatewayAddress = apcondetails.gateway;
                        this.apConnectionMethod = apcondetails.method;
                        this.apManualIP = (this.apMethod == "shared" || this.isMethod == "manual");
                        this.apNetworkingSettingsTouched = false; //this.apManualIP;
                        return [3 /*break*/, 3];
                    case 2:
                        console.log("Device info is missing...unable get AP connection info");
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        error_1 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getFormattedErrorMessage"])("An error was returned while getting AP connection info.", error_1);
                        console.log(message);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    SystemComponent.prototype.getISConnectionInfo = function (deviceInfo) {
        return __awaiter(this, void 0, void 0, function () {
            var isConnnection, iscondetails, error_2, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 4, , 5]);
                        if (!(deviceInfo != null || deviceInfo != undefined)) return [3 /*break*/, 2];
                        isConnnection = deviceInfo.connection;
                        return [4 /*yield*/, this.connectivityService.connectionDetails_async(isConnnection)];
                    case 1:
                        iscondetails = _a.sent();
                        this.isConnectionName = iscondetails.id;
                        this.isIPAddress = (iscondetails.primaryAddress != null ? Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["parsePrimaryIp"])(iscondetails.primaryAddress) : "NONE");
                        this.isMethod = iscondetails.method;
                        this.isSubnetAddress = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getMaskFromSubnetDot"])(Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["parsePrimaryIp"])(iscondetails.primaryAddress, false).toString());
                        this.isGatewayAddress = iscondetails.gateway;
                        this.isManualIP = (this.isMethod == "shared" || this.isMethod == "manual");
                        this.isNetworkingSettingsTouched = false; //this.isManualIP;== "manual");
                        this.apNetworkingSettingsTouched = false; //this.apManualIP;
                        return [3 /*break*/, 3];
                    case 2:
                        console.log("Device info is missing...unable get IS connection info");
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        error_2 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getFormattedErrorMessage"])("An error was returned while getting IS connection info.", error_2);
                        console.log(message);
                        return [3 /*break*/, 5];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    SystemComponent.prototype.getConnectionNames = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _a, connsettings, apconn_1, isconn_1, devicesInfo, apdevice, isdevice, error_3, message;
            return __generator(this, function (_b) {
                switch (_b.label) {
                    case 0:
                        _b.trys.push([0, 8, 9, 10]);
                        this.loadingService.setWaiting(true, "Retrieving connection information....");
                        _a = this;
                        return [4 /*yield*/, this.connectivityService.hostConnectionEcho_async()];
                    case 1:
                        _a.hostIPAddress = _b.sent();
                        return [4 /*yield*/, this.settingsService.connections_async()];
                    case 2:
                        connsettings = _b.sent();
                        this.apManualIP = false;
                        this.isManualIP = false;
                        apconn_1 = connsettings.find(function (conn) { return conn.mode == "ap"; });
                        isconn_1 = connsettings.find(function (conn) { return conn.mode == "is"; });
                        if (!(apconn_1 != null && isconn_1 != null)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.connectivityService.devices_async()];
                    case 3:
                        devicesInfo = _b.sent();
                        apdevice = devicesInfo.find(function (device) { return device.device == apconn_1.adapter; });
                        isdevice = devicesInfo.find(function (device) { return device.device == isconn_1.adapter; });
                        return [4 /*yield*/, this.getAPConnectionInfo(apdevice)];
                    case 4:
                        _b.sent();
                        return [4 /*yield*/, this.getISConnectionInfo(isdevice)];
                    case 5:
                        _b.sent();
                        return [3 /*break*/, 7];
                    case 6:
                        console.log("Error: connection information is missing.");
                        _b.label = 7;
                    case 7: return [3 /*break*/, 10];
                    case 8:
                        error_3 = _b.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getFormattedErrorMessage"])("An error was returned while getting connection info.", error_3);
                        console.log(message);
                        return [3 /*break*/, 10];
                    case 9:
                        this.loadingService.setWaiting(false, "");
                        return [7 /*endfinally*/];
                    case 10: return [2 /*return*/];
                }
            });
        });
    };
    /************************************PROXY*************************************** */
    SystemComponent.prototype.saveProxySettings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var httpProxySettings, resp, error_4, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        httpProxySettings = new _common_data_http_proxy__WEBPACK_IMPORTED_MODULE_4__["HttProxy"]();
                        _a.label = 1;
                    case 1:
                        _a.trys.push([1, 3, 4, 5]);
                        this.loadingService.setWaiting(true, "Saving Proxy settings.....");
                        httpProxySettings.httpProxy = this.proxySetting;
                        return [4 /*yield*/, this.systemService.setProxySetting_async(httpProxySettings)];
                    case 2:
                        resp = _a.sent();
                        this.proxySettingsTouched = false;
                        this.popup.displayMessage(true, "Successfully saved proxy settings.");
                        return [3 /*break*/, 5];
                    case 3:
                        error_4 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getFormattedErrorMessage"])("Unable to save the proxy settings.", error_4);
                        this.popup.displayMessage(false, message);
                        console.log(message);
                        return [3 /*break*/, 5];
                    case 4:
                        this.loadingService.setWaiting(false, "");
                        return [7 /*endfinally*/];
                    case 5: return [2 /*return*/];
                }
            });
        });
    };
    SystemComponent.prototype.proxySettingKeyEvent = function (event) {
        this.proxySettingsTouched = true;
    };
    /************************************GENERIC FUNCTIONS************************** */
    /**************************AP Mode******************************/
    SystemComponent.prototype.resetAPIPFieldErrors = function () {
        this.apIPFieldError = false;
        this.apSubnetFieldError = false;
        this.apGatewayFieldError = false;
    };
    SystemComponent.prototype.validateAPNetworSettings = function () {
        this.apIPFieldError = !Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["validateIPAddress"])(this.apIPAddress);
        this.apSubnetFieldError = !Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["validateIPAddress"])(this.apSubnetAddress);
        this.apGatewayFieldError = !Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["validateIPAddress"])(this.apGatewayAddress);
        return (!this.apIPFieldError && !this.apGatewayFieldError && !this.apSubnetFieldError);
    };
    SystemComponent.prototype.saveAPNetworSettings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var proceed, dot, method, error_5, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, 7, 8]);
                        this.loadingService.setWaiting(true, "Saving AP settings.....");
                        proceed = this.validateAPNetworSettings();
                        if (!proceed) return [3 /*break*/, 4];
                        dot = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getSubnetFromMask"])(this.apSubnetAddress);
                        if (!(dot != -1)) return [3 /*break*/, 2];
                        method = new _common_data_ip_connection_method__WEBPACK_IMPORTED_MODULE_6__["IPConnectionMethod"]();
                        method.gateway = this.apGatewayAddress;
                        method.ip = this.apIPAddress + "/" + dot.toString();
                        method.subnet = this.apSubnetAddress;
                        method.method = this.apMethod;
                        method.connection = this.apConnectionName;
                        return [4 /*yield*/, this.connectivityService.setConnectionMethod_async(method)];
                    case 1:
                        _a.sent(); //.subscribe((resp)=>{
                        this.apNetworkingSettingsTouched = false;
                        this.popup.displayMessage(true, "Successfully saved AP settings.");
                        return [3 /*break*/, 3];
                    case 2:
                        this.popup.displayMessage(false, "Subnet formation appears to be invalid.");
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        setTimeout(function () { _this.resetAPIPFieldErrors(); }, 5000);
                        this.popup.displayMessage(false, "Invalid IP address format detected.");
                        _a.label = 5;
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        error_5 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getFormattedErrorMessage"])("An error occured while saving AP network settings", error_5);
                        this.popup.displayMessage(false, message);
                        setTimeout(function () { _this.resetAPIPFieldErrors(); }, 5000);
                        console.log(message);
                        return [3 /*break*/, 8];
                    case 7:
                        this.loadingService.setWaiting(false, "Saving IS settings.....");
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    /**************************Infrastructure Mode******************************/
    SystemComponent.prototype.resetISIPFieldErrors = function () {
        this.isIPFieldError = false;
        this.isSubnetFieldError = false;
        this.isGatewayFieldError = false;
    };
    SystemComponent.prototype.validateISNetworSettings = function () {
        this.isIPFieldError = !Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["validateIPAddress"])(this.isIPAddress);
        this.isSubnetFieldError = !Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["validateIPAddress"])(this.isSubnetAddress);
        this.isGatewayFieldError = !Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["validateIPAddress"])(this.isGatewayAddress);
        return (!this.isIPFieldError && !this.isGatewayFieldError && !this.isSubnetFieldError);
    };
    SystemComponent.prototype.saveISNetworSettings = function () {
        return __awaiter(this, void 0, void 0, function () {
            var _this = this;
            var proceed, dot, method, error_6, message;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        _a.trys.push([0, 6, 7, 8]);
                        this.loadingService.setWaiting(true, "Saving IS settings.....");
                        proceed = this.validateISNetworSettings();
                        if (!proceed) return [3 /*break*/, 4];
                        dot = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getSubnetFromMask"])(this.isSubnetAddress);
                        if (!(dot != -1)) return [3 /*break*/, 2];
                        method = new _common_data_ip_connection_method__WEBPACK_IMPORTED_MODULE_6__["IPConnectionMethod"]();
                        method.gateway = this.isGatewayAddress;
                        method.ip = this.isIPAddress + "/" + dot.toString();
                        method.subnet = this.isSubnetAddress;
                        method.method = this.isMethod;
                        method.connection = this.isConnectionName;
                        return [4 /*yield*/, this.connectivityService.setConnectionMethod_async(method)];
                    case 1:
                        _a.sent();
                        this.isNetworkingSettingsTouched = false;
                        this.popup.displayMessage(true, "Successfully saved AP settings.");
                        return [3 /*break*/, 3];
                    case 2:
                        this.popup.displayMessage(false, "Subnet formation appears to be invalid.");
                        _a.label = 3;
                    case 3: return [3 /*break*/, 5];
                    case 4:
                        setTimeout(function () { _this.resetISIPFieldErrors(); }, 5000);
                        this.popup.displayMessage(false, "Invalid IP address format detected.");
                        _a.label = 5;
                    case 5: return [3 /*break*/, 8];
                    case 6:
                        error_6 = _a.sent();
                        message = Object(_common_services_global_helper_functions__WEBPACK_IMPORTED_MODULE_8__["getFormattedErrorMessage"])("An error occured while saving IS network settings", error_6);
                        setTimeout(function () { _this.resetISIPFieldErrors(); }, 5000);
                        this.popup.displayMessage(false, message);
                        console.log(message);
                        return [3 /*break*/, 8];
                    case 7:
                        this.loadingService.setWaiting(false, "");
                        return [7 /*endfinally*/];
                    case 8: return [2 /*return*/];
                }
            });
        });
    };
    SystemComponent.prototype.apAddressKeyEvent = function (event) {
        this.apNetworkingSettingsTouched = true;
    };
    SystemComponent.prototype.isAddressKeyEvent = function (event) {
        this.isNetworkingSettingsTouched = true;
    };
    SystemComponent.prototype.toggleISManual = function () {
        var temp = this.isManualIP;
        this.isManualIP = !this.isManualIP;
        console.log(this.isManualIP);
        this.isMethod = (this.isManualIP ? "manual" : "auto");
        this.cd.markForCheck();
        this.isNetworkingSettingsTouched = true;
    };
    SystemComponent.prototype.toggleAPManual = function () {
        this.apManualIP = !this.apManualIP;
        console.log(this.apManualIP);
        this.apMethod = (this.apManualIP ? "shared" : "auto");
        this.cd.markForCheck();
        this.apNetworkingSettingsTouched = true;
    };
    SystemComponent.prototype.preCheckConnectionData = function (connectionMode) {
        return __awaiter(this, void 0, void 0, function () {
            var response, response;
            return __generator(this, function (_a) {
                switch (_a.label) {
                    case 0:
                        if (!(connectionMode == "ap")) return [3 /*break*/, 4];
                        if (!(this.apIPAddress == this.hostIPAddress)) return [3 /*break*/, 2];
                        return [4 /*yield*/, this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirmation", false)];
                    case 1:
                        response = _a.sent();
                        if (response == 2 /* Yes */) {
                            this.saveAPNetworSettings();
                        }
                        return [3 /*break*/, 3];
                    case 2:
                        this.saveAPNetworSettings();
                        _a.label = 3;
                    case 3: return [3 /*break*/, 7];
                    case 4:
                        if (!(connectionMode == "is")) return [3 /*break*/, 7];
                        if (!(this.isIPAddress == this.hostIPAddress)) return [3 /*break*/, 6];
                        return [4 /*yield*/, this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirm change", false)];
                    case 5:
                        response = _a.sent();
                        if (response == 2 /* Yes */) {
                            this.saveISNetworSettings();
                        }
                        return [3 /*break*/, 7];
                    case 6:
                        this.saveISNetworSettings();
                        _a.label = 7;
                    case 7: return [2 /*return*/];
                }
            });
        });
    };
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_3__["PopUpMessageComponent"]),
        __metadata("design:type", _common_ui_widgets_pop_up_message_pop_up_message_component__WEBPACK_IMPORTED_MODULE_3__["PopUpMessageComponent"])
    ], SystemComponent.prototype, "popup", void 0);
    __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["ViewChild"])(_common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_9__["ModalConfirmationComponent"]),
        __metadata("design:type", _common_ui_widgets_modal_confirmation_modal_confirmation_component__WEBPACK_IMPORTED_MODULE_9__["ModalConfirmationComponent"])
    ], SystemComponent.prototype, "confirmation", void 0);
    SystemComponent = __decorate([
        Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["Component"])({
            selector: 'app-system',
            template: __webpack_require__(/*! ./system.component.html */ "./src/app/system/system.component.html"),
            styles: [__webpack_require__(/*! ./system.component.css */ "./src/app/system/system.component.css")],
        }),
        __metadata("design:paramtypes", [_common_services_page_header_service__WEBPACK_IMPORTED_MODULE_1__["PageHeaderService"], _common_services_system_service__WEBPACK_IMPORTED_MODULE_2__["SystemService"],
            _common_services_connectivity_service__WEBPACK_IMPORTED_MODULE_5__["ConnectivityService"], _angular_core__WEBPACK_IMPORTED_MODULE_0__["ChangeDetectorRef"],
            _common_services_settings_service__WEBPACK_IMPORTED_MODULE_7__["SettingsService"], _common_services_loading_service__WEBPACK_IMPORTED_MODULE_10__["LoadingService"]])
    ], SystemComponent);
    return SystemComponent;
}());



/***/ }),

/***/ "./src/environments/environment.ts":
/*!*****************************************!*\
  !*** ./src/environments/environment.ts ***!
  \*****************************************/
/*! exports provided: environment */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony export (binding) */ __webpack_require__.d(__webpack_exports__, "environment", function() { return environment; });
// This file can be replaced during build by using the `fileReplacements` array.
// `ng build ---prod` replaces `environment.ts` with `environment.prod.ts`.
// The list of file replacements can be found in `angular.json`.
var environment = {
    production: false
};
/*
 * In development mode, to ignore zone related error stack frames such as
 * `zone.run`, `zoneDelegate.invokeTask` for easier debugging, you can
 * import the following file, but please comment it out in production mode
 * because it will have performance impact when throw error
 */
// import 'zone.js/dist/zone-error';  // Included with Angular CLI.


/***/ }),

/***/ "./src/main.ts":
/*!*********************!*\
  !*** ./src/main.ts ***!
  \*********************/
/*! no exports provided */
/***/ (function(module, __webpack_exports__, __webpack_require__) {

"use strict";
__webpack_require__.r(__webpack_exports__);
/* harmony import */ var _angular_core__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! @angular/core */ "./node_modules/@angular/core/fesm5/core.js");
/* harmony import */ var _angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! @angular/platform-browser-dynamic */ "./node_modules/@angular/platform-browser-dynamic/fesm5/platform-browser-dynamic.js");
/* harmony import */ var _app_app_module__WEBPACK_IMPORTED_MODULE_2__ = __webpack_require__(/*! ./app/app.module */ "./src/app/app.module.ts");
/* harmony import */ var _environments_environment__WEBPACK_IMPORTED_MODULE_3__ = __webpack_require__(/*! ./environments/environment */ "./src/environments/environment.ts");




if (_environments_environment__WEBPACK_IMPORTED_MODULE_3__["environment"].production) {
    Object(_angular_core__WEBPACK_IMPORTED_MODULE_0__["enableProdMode"])();
}
Object(_angular_platform_browser_dynamic__WEBPACK_IMPORTED_MODULE_1__["platformBrowserDynamic"])().bootstrapModule(_app_app_module__WEBPACK_IMPORTED_MODULE_2__["AppModule"])
    .catch(function (err) { return console.log(err); });


/***/ }),

/***/ 0:
/*!***************************!*\
  !*** multi ./src/main.ts ***!
  \***************************/
/*! no static exports found */
/***/ (function(module, exports, __webpack_require__) {

module.exports = __webpack_require__(/*! C:\Users\nortonj\Documents\Projects\web-portal\transact-ps-portal\src\main.ts */"./src/main.ts");


/***/ })

},[[0,"runtime","vendor"]]]);
//# sourceMappingURL=main.js.map