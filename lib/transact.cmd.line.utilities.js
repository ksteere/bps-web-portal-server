//This module provides global methods for handling command line operations and other data manipulation provided to the 
//the process by the user, scripts etc
module.exports={
    // |Name:commandLineToObject
    // |Summary:reads the command line and parses input in
    // |the form of name=value<space> and sets it equal to the 
    // |supplied objects appropriate attribute
    // |Note: Object hierarchy needs to be represented by a '.' on the command line
    // |e.g. object.attribute
    // |Parmameters:[any js object]
    // |Return:
    // |Author:
    commandLineToObject(jsobject = null, nvdelimiter = "=", exlusions = [])
    {
        try{
            if(jsobject != null){
                process.argv.forEach(function(val, index, array) {
                    if(index > 1){
                    /*Object.keys(settings).forEach(function(key,index) {
                            // key: the name of the object key
                            // index: the ordinal position of the key within the object 
                            console.log(settings[key]);
                        });*/
                        if(exlusions.findIndex(exclude=>exclude == val) == -1){
                            var nameValue = val.split(nvdelimiter);
                            if((nameValue && nameValue) && nameValue.length==2){
                                var levels = nameValue[0].split(".");
                                if(levels.length > 0){
                                    var currentLevel = jsobject;
                                    for(var index = 0; index < levels.length -1; index ++ ){
                                        var levelkey = levels[index];
                                        currentLevel = currentLevel[levelkey];
                                    }
                                    var finalLevelKey = levels[levels.length - 1]
                                    currentLevel[finalLevelKey] = nameValue[1];
                                }
                            }
                        }
         
                    }
                    
                });
            }
            else{
                throw "Object can't be null";
            }
        }
        catch(error){
            throw error;
        }
    },
    //|Name:getArgumentValue
    //|Summary:if it's not obvious...you probably should find another career
    //|-----------------------------
    //|-----------------------------
    //|-----------------------------
    //|-----------------------------
    //|Parmameters:[argument - that is needed (if it exists), the delimiter used to seperate the value ]
    //|Return: the arguments value
    //|Author:
    getArgumentValue(argname, nvdelimiter = "="){
        try{
            var returnValue = null;
            process.argv.forEach(function(val, index, array) {
                if(index > 1){
                    var nameValue = val.split(nvdelimiter);
                    if((nameValue && nameValue) && nameValue.length == 2 && nameValue[0].localeCompare(argname) == 0){
                        returnValue = nameValue[1];
                        return;
                    }
                }
            });
            return returnValue;
        }
        catch(error){
            throw error;
        }

    },
}