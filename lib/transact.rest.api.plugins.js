var fs = require('fs');
var path = require('path');
var restapiplugins = module.exports = {
    initialized : false,
    filename : "plugins.json",
    plugins : {
        relativeDirectory : "./bash-scripts", 
        map : 
        [
            {uri:"/api/v1/connectivity/devices", verb : "GET", command : "get-basic-device-info.sh", 
            pathType:"relative", category :"connectivity", params : []},
            {uri:"/api/v1/connectivity/connections", verb : "GET", command : "get-basic-connection-info.sh", 
            pathType:"relative", category :"connectivity", params : []},
            {uri:"/api/v1/connectivity/wifinetworks", verb : "GET", command : "scan-wifi-networks.sh", 
            pathType:"relative", category : "connectivity", params : []},
            {uri:"/api/v1/connectivity/wifi/connect", verb : "POST", command : "connect-to-wifi-network.sh", 
            pathType:"relative", category:"connectivity", params : []},
            {uri:"/api/v1/connectivity/device/details", verb : "GET", command : "get-device-details.sh", 
            pathType:"relative", category:"connectivity", params : []},
            {uri:"/api/v1/connectivity/connection/details", verb : "GET", command : "get-connection-details.sh", 
            pathType:"relative", category:"connectivity", params : []},
            {uri:"/api/v1/connectivity/connection/setting", verb : "POST", command : "set-connection-setting.sh", 
			pathType:"relative", category:"connectivity", params : []},
			{uri:"/api/v1/connectivity/connection/ip/method", verb : "POST", command : "set-ip-connection-method.sh", 
			pathType:"relative", category:"connectivity", params : []},
			{uri:"/api/v1/connectivity/connection/delete", verb : "POST", command : "delete-connection.sh", 
			pathType:"relative", category:"connectivity", params : []},
			{uri:"/api/v1/connectivity/ap/connect", verb : "POST", command : "create-wifi-ap.sh", 
            pathType:"relative", category:"connectivity", params : []},
            {uri:"/api/v1/system/proxy/setting", verb : "DELETE", command : "clear-http-proxy.sh", 
			/* 	JRN Per JIRA WWP-3 the file name for proxy setting wil later 
				be changed to a system-wide setting file instead of /home/ubuntu/.profile
			*/
			pathType:"relative", category:"system", params : ["/home/ubuntu/.profile"]},
            {uri:"/api/v1/system/proxy/setting", verb : "GET", command : "get-http-proxy.sh", 
            pathType:"relative", category:"system", params : ["/home/ubuntu/.profile"]},
            {uri:"/api/v1/system/proxy/setting", verb : "POST", command : "set-http-proxy.sh", 
            pathType:"relative", category:"system", params : ["/home/ubuntu/.profile"]},
            {uri:"/api/v1/system/avahi/name", verb : "POST", command : "set-avahi-service-name.sh", 
            pathType:"relative", category:"system", params : ["/etc/avahi/services/printer.service"]},
            {uri:"/api/v1/system/avahi/name", verb : "GET", command : "get-avahi-service-name.sh", 
            pathType:"relative", category:"system", params : ["/etc/avahi/services/printer.service"]},
        ]
    },
    init(){
		this.initialized = true;
		
    },
    persistPluginsMetaData:function(storagelocation, object = null)
		{
			let succeeded = false;
			try{
				this.init();
				var tmplocation = path.normalize(storagelocation);
				var fileLocation = path.format({
					root: '/ignored',
					dir: tmplocation,
					base: this.filename
				})
				var directory = path.dirname(fileLocation);
				if (!fs.existsSync(directory)){
					fs.mkdirSync(directory, { recursive: true });
				}
				var tempplugins = null;
				if(object != null){
					tempplugins = object;
				}
				else{
					tempplugins = this.plugins;
				}
				fs.writeFileSync(fileLocation, JSON.stringify(tempplugins));
				succeeded = true;
			}
			catch(error){
				console.log("Failed writting the plugins file %s %s was returned.", this.filename, error);
				succeeded = false;
			
			}
			finally{
				return succeeded;
			}
			
		},
		loadPluginsMetadata:function(directory = null, create = false){
			let succeeded = false;
			try
			{
				this.init();
				var tmplocation = path.normalize(directory);
				var fileLocation = path.format({
					root: '/ignored',
					dir: tmplocation,
					base:this.filename
				})
				var fileExists = fs.existsSync(fileLocation);
				if(fileExists){
					var data = fs.readFileSync(fileLocation);
					this.root = JSON.parse(data);
					succeeded = true;
				}
				else{
					if(create){
						succeeded = this.persistPluginsMetaData(directory);
						
					}
				}
			}
			catch(error)
			{
				console.log("Failed reading supplied plugins file %s %s was returned.", this.filename, error);
				succeeded = false;
			}
			finally{
				return succeeded;
			}
		
		}
}