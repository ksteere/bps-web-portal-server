/**
 * New node file
 */
var httputils = require("./transact.http.utilities");
var routemanager = module.exports = {
	routemap : {}, 
	sessionmanager : null,
	getSessionManager(){
		return this.sessionmanager;
	},
	init(sessionmanager = null){
		this.sessionmanager = sessionmanager;
	},
	makeKey(datapath, method){
		var key = datapath+':'+(method == null ? 'GET' : method.toUpperCase());
		return key;
	},
	setRoute : function(datapath, method = null, callback = null)
	{
		key = this.makeKey(datapath, method);
		this.routemap[key] = callback;	
	},
	getRoute : function(datapath, method)
	{
		key = this.makeKey(datapath, method);
		return this.routmap[key];
	},
		
	removeRoute : function(datapath, method)
	{
		key = this.makeKey(datapath, method);
		delete this.routemap[key];
	},
	exportRoutes : function(){
		var routes = [];
		for (let [key, value] of Object.entries(this.routemap)) {
			var namevalue = key.split(":");
			routes.push({apiPath : namevalue[0], verb : namevalue[1], type: "object"});
		}
		return routes;
	},
	executeRoute : function(datapath, req, res, byPassValidation = false)
	{
		key = this.makeKey(datapath, req.method);
		var obj = this.routemap[key];
		if(obj != null || obj != "undefined")
		{
			if(byPassValidation || this.sessionmanager == null || this.sessionmanager.validateSession(datapath, req)){
				var type = typeof obj;
				if(type == "function"){
					obj(datapath, req, res);
				}
				else if(type == "string"){
					httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension(".txt"), obj, HTTP_RETURN_CODES.SUCCESS);
				}
				else if(type == "boolean" || type == "bigint" || type == "number"){
					httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension(".txt"), obj.toString(), HTTP_RETURN_CODES.SUCCESS);
				}
				else if(type == "object"){
					var json = JSON.stringify(obj);
					httputils.writeSuccessResponse(res,httputils.getContentTypeFromExtension(".json"), json, HTTP_RETURN_CODES.SUCCESS);
				}
				return true;
			}
			else{
				httputils.writeErrorResponse(res, HTTP_RETURN_CODES.NOTAUTHORIZED);
				return true;
			}
		}
		else
		{
			return false;
		}
	

	},
	
}