var process = require('process');
var execFile = require('child_process').execFile;
var execFileSync = require('child_process').execFileSync;
//Platform utilities is a catch all for forked, spawned or shelled system commands - this file really should be broken up into seperate modules
//and this module should be renamed linux.network.manager as well as trimmed down but the interfaces have been developed in such a way that they
//were originally intended to be cross platform (w32/linux/mac etc) and contain many system wide commands. Additionally all promise functions 
//should be refactored to employ try/catch statements so that if the provided invokable function fails or throws and exception we can revover more
//gracefully i.e the resolve and reject should be contained in try/catches
//TODO: 
//1. Break up module
//2.
var platformutliities = module.exports = {
    /*******************************************************************/
    /**************************PLATFORM INDEPENDENT*********************/
    isWin32() {
        return (process.platform == 'win32');
    },
    getOSPlatform() {
        var OS = "";
        switch (process.platform) {
            case 'darwin':
                OS = 'mac';
                break;
            case 'sunos':
            case 'freebsd':
            case 'linux':
            case 'openbsd':
            case 'aix':
                OS = 'linux';
                break;
            case 'win32':
                OS = 'windows';
                break;
        }
        return OS;

    },
    getNetworkConfiguration(interface = null) {
        var platform = this.getOSPlatform();
        if (platform == 'windows') {
            return this.getNetworkConfiguration_Win32(interface);
        }
        else if (platform == 'linux') {
            return this.getNetworkConfiguration_Linux(interface);
        }
    },
    /*******************************************************************/
    /**************************HELPER FUNCTIONS*************************/
async spwanExecutableScriptOrProcess(execpath, args = []){
     return new Promise((resolve, reject)=>{
        execFile(execpath, args, null, function(err, resp, stderr){
            if(err){
                
                reject({error : err, stderror : stderr});
            }
            else{
                resolve(resp);
            }

        });
        
     });
        
    },
    async getNetworkConfiguration_Win32(interface = null) {
        var args = [];
        var responses = [];
        args.push('interface');
        args.push('ip');
        args.push('show');
        args.push('addresses');
        //If no interface is supplied get data for ALL interfaces
        if (interface != null) {
            args.push(interface);
        }
        return new Promise((resolve, reject) => {
            execFile('netsh', args, null, function (err, resp, stderr) {
                if (err) {
                    //if the process/shell returns an error respond right away - no need to return as this is an async call with a callback
                    var error = platformutliities.parseNMCLIStdError(stderr, err);
                    reject(error);
                }
                else {
                    //this is a bit of a kludge but works
                    //split the response into an array by return and newline
                    var parts = resp.split('\r\n');
                    //this will be pushed into an array of responses 
                    var data = null;
                    parts.forEach(element => {
                        //iterate over each element, if the length is not 0 or null continue
                        if (element.length > 0) {
                            //this is the start of a new adapter so if we detect it push a new data object into the array
                            var regx = element.match("Configuration for interface \".*\"");
                            if (regx != null && regx.length > 0 && regx.index >= 0) {
                                data = { output: "", ipadddres: "", subnet: "", dhcpEnabled: false, gateway: "", gatewayMetric: 0, interfaceMetric: 0 };
                                responses.push(data);
                            }
                            //clean up the string and split each line, if applicable, into an array
                            var tmp = element.replace(",", "").trim().split(':');
                            if (tmp.length == 1) {
                                //if its a lone line it's likely the 'header' that we detected in the regex
                                data.output = tmp[0].trim();
                            }
                            else if (tmp.length == 2) {
                                //here is the meat and the individual elements
                                //disconnected adpaters won't have all this information availble, for example, ip-address
                                var name = tmp[0].trim();
                                var value = tmp[1].trim();
                                if (name.indexOf("DHCP enabled") != -1) {
                                    data.dhcpEnabled = value;
                                }
                                else if (name.indexOf("Default Gateway") != -1) {
                                    data.gateway = value;
                                }
                                else if (name.indexOf("Gateway Metric") != -1) {
                                    data.gatewayMetric = value;
                                }
                                else if (name.indexOf("IP Address") != -1) {
                                    data.ipadddres = value;
                                }
                                else if (name.indexOf("Subnet Prefix") != -1) {
                                    data.subnet = value;
                                }
                                else if (name.indexOf("InterfaceMetric") != -1) {
                                    data.interfaceMetric = value;
                                }
                            }
                        }

                    });

                    resolve(responses);

                }

                // Errors from nmcli came from stdout, we test presence of 'Error: ' string
            });
        });
        //Win32 we use netsh as it is much more robust and detailed with higher resolution than ipconfig alone
    },
    async getHostName() {
        return new Promise((resolve, reject) => {
            var args = [];
            args.push('g');
            args.push('hostname');
            var host = execFileSync('nmcli', args, {});
            if (host != null) {
                resolve({ "result": "success", "host": host });
            }
            else {
                reject({ "result": "error", "error": resp });
            }
        });

        return { hostName: host };
    },
    
}