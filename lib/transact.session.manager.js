/**
 * New node file
 */
const http = require('http');
var sessionmanager = module.exports = {
    excludedroutes : [], 
    sessionmap : new Map(), 
    kickSessionKey(key){
        this.sessionmap[key] = Date.now();
        
    },
    makeKey(datapath, method){
        var key = datapath+':'+(method == null ? 'GET' : method.toUpperCase());
        return key;
    },
    createSession(){
        var partOne = Date.now().toString();
        var partTwo = (Math.floor(Math.random() * 100)).toString();
        var key = partOne + partTwo;
        this.sessionmap[key] = Date.now();
        return key;

    },
    isSessionTokenValid(token = null){
        if(token != null && token != undefined){
            var value = this.sessionmap[token];
            if(value != null && value != undefined){
                return true;
            }
            else{
                return false;
            }
        }
        else{
            return false;
        }
        
    },
    validateSession(datapath, req){
        var method = req.method;
        var routekey = this.makeKey(datapath, method);
        var excluded = this.excludedroutes.includes(routekey);	
        if(!excluded){
            var sessionToken = req.headers.authorization;
            if(sessionToken != undefined){
                var value = this.sessionmap[sessionToken];
                return (value != undefined);
            }
            else{
                return false;
            }
            
        }
        else{
            return true;

        }
        
    },
    excludeRoute : function(datapath, method = null)
    {
        key = this.makeKey(datapath, method);
        this.excludedroutes.push(key);
      
    },
    removeExpiredSessions(delta_ms){
        var expireTime = Date.now();
        for(var key in this.sessionmap){
            var value = this.sessionmap[key];
            maxTime =  value + delta_ms;
            if(maxTime <= expireTime){
                console.log("Evicting/expiring session for %s", key);
                delete this.sessionmap[key];

            }
        }
        
        this.sessionmap.forEach(function(value, key){
          

        });
     
    }
    
}