var path = require('path');
var process = require('process');
var logging_utils = require('./transact.logging.utilities');

var http_utils = module.exports = {
    const : HTTP_RETURN_CODES = {SUCCESS:200,SUCCESS_CREATE:201, ACCEPTED:202, NOTAUTHORIZED:401, FORBIDDEN:403, NOTFOUND:404,PRECONDITIONFAILED:412, BADENTITY:422, SERVERERROR:500, NOTIMPLEMENTED:503},
    writeSuccessResponse : function(res, contentType, contents, status = HTTP_RETURN_CODES.SUCCESS){
        try{
            res.writeHead(status, {'Content-Type': contentType, "Access-Control-Allow-Origin" : "*",
            "Access-Control-Allow-Methods" : "GET,POST,PATCH,DELETE,PUT,OPTIONS",
            "Access-Control-Allow-Headers" : "Origin, X-Requested-With, Content-Type, Accept"});
            res.statusCode = status;
            res.end(contents, "utf=8");
        }
        catch(err){
            logging_utils.LogMessage("Error while sending respons %s", [err]);
        }
        
    },
    writeErrorResponse:function(res, status = HTTP_RETURN_CODES.NOTFOUND, message = null)
    {
        try{
            res.writeHead(status, {'Content-Type': 'text', "Access-Control-Allow-Origin" : "*",
            "Access-Control-Allow-Methods" : "GET,POST,PATCH,DELETE,PUT,OPTIONS", 
            "Access-Control-Allow-Headers" : "Origin, X-Requested-With, Content-Type, Accept"});
            res.statusCode = status;
            var errorstatus = null;
            if(message != null){
                var errorstatus = "Error: [status code] " +  status.toString();
                var errorstatus = "Error: [status code] " + status.toString() +", [message]:" +   message;
            }
            else{
           
            }
            res.end(errorstatus, "utf-8");	
        }
        catch(err){
            logging_utils.LogMessage("An error occurred %s", [err]);
        }
        
    },
    localizeUri:function(baseDirectory, uri){
        var truePath = "";
        try{
            var truePath = (process.platform == 'win32' ? ".\\" : "./") + path.join(baseDirectory, path.normalize(uri));
            console.log("True path %s",truePath);
        }
        catch(err){
            logging_utils.LogMessage("Error while sending respons %s", [err]);
        }
        finally{
            return truePath;
        }
        
    },
    getContentTypeFromExtension:function(extname){
        var contentType = "";
        switch(extname)
        {
            case '.htm':
            case '.html':
                contentType = "text/html";
                break;
            case '.js':
                contentType = "text/javascript";
                break;
            case '.css':
                contentType = "text/css";
                break;
            case '.jpg':
                contentType = "img/jpg";
                break;
            case '.png':
                contentType = "img/png"; 
                break;
            case '.svg':
                contentType = 'img/svg+xml';
                break;
            case '.woff2':
                contentType = 'font/woff2';
            break;
            case '.woff':
                contentType = 'font/woff';
            break;
            case '.tff':
                contentType = 'font/tff';
            break;
            case '.bin':
                contentType = 'application/octet-stream';
            break;
            case '.bmp':
                contentType = 'image/bmp';
            break;
            case '.csv':
                contentType = 'text/csv';
            break;
            case '.jar':
                contentType = 'application/java-archive';
            break;
            case '.json':
                contentType = 'application/json';
            break;
            case '.gif':
                contentType = 'image/gif';
            break;
            case '.mp3':
                contentType = 'audio/mpeg';
            break;
            case '.pdf':
                contentType = 'application/pdf';
            break;
            case '.otf':
                contentType = 'font/otf';
            break;
            case '.xml':
                contentType = 'text/xml';
            break;
            case '.zip':
                contentType = 'application/zip';
            break;
            case '.tiff':
            case '.tif':
                contentType = 'image/tiff';
            break;
            case '.ttf':
                contentType = 'font/ttf';
            break;
            default:
                contentType = 'text/plain';
            break;
        }
        return contentType;
    },
    JSONStringToObject(data, output = null){
        var isJSON = false;
        try{
            //let's double check
            console.log(data);
            var obj = JSON.parse(data);
            if(output != null || output != undefined){
                output.object = obj;
                output.isJSON = true;
                
            }
            isJSON = true;
        }
        catch(error){
            output = null;
            isJSON = false;
        }
        finally{
            return isJSON;
        }
    },
    normalizeURIPath(path){
        if(path != null && path != undefined){
            var length = path.length;
            if(length >0){
                if(path[length - 1] == "/"){
                    var temp = path.substr(0, length - 1);
                    return (temp[0] == "." ? temp.substr(1) : temp);
                }
                else{

                    return (path[0] == "." ? path.substr(1) : path);
                }
            }
            else{
                return "";
            }
        }
        else{
            return "";
        }
        
    }
};
