/**
 * New node file
 */
var restcontainer = module.exports = {
	restdatamap : {}, 
	eventListeners : [],
	addEventListener(callback){
		this.eventListeners.push(callback);
	},
	keyExists(key){
		console.log(key);
		var index = Object.keys(this.restdatamap).findIndex(x=>x == key);
		return index >= 0;
	
	},
	setData : function(datapath, dataObj)
	{	
		var exists = this.keyExists(datapath);
		if(exists){
			this.restdatamap[datapath] = dataObj;	
			this.eventListeners.forEach(callback=>{
				callback(datapath, dataObj);
			});
			return true;
		}
		else{
			return false;
		}
		
	},
	mapObject : function(basePath, dataObj){
		var completePath = "";
		if(dataObj != null && dataObj != undefined ){
			var objType = typeof dataObj;
			if(objType == "object"){
				for (let [key, value] of Object.entries(dataObj)) {
					completePath = (basePath + key) + "/";
					this.mapObject(completePath, value);
				}
			}
			else{
				var mappedPath = basePath.slice(0, basePath.length - 1);
				this.restdatamap[mappedPath] = dataObj;
			}
		}
	},			
	getJsonData : function(datapath)
	{
		var obj = this.restdatamap[datapath];
		var jsonString = '';
		if(obj != null)
		{
			jsonString = JSON.stringify(obj);
		}
		return jsonString;
	},
	
	getData : function(datapath)
	{
		if(this.keyExists(datapath)){
			var data = this.restdatamap[datapath];
			return JSON.stringify({"data":data, "type": typeof data});
		
		}
		else{
			return null;
		}
	},
	removeData : function(datapath)
	{
		delete this.restdatamap[datapath];
	},
	exportRoutes : function(){
		var routes = [];
		for (let [key, value] of Object.entries(this.restdatamap)) {
			var xtype = typeof value;
			routes.push({apiPath : key, verb : "GET", type: xtype});
			routes.push({apiPath : key, verb : "PUT", type: xtype});
		}
		return routes;
	},
}
