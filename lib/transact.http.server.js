/*packages**************************************************************/
var fs = require('fs');
var path = require('path');
var http = require('http');
var url = require('url');
var httputils = require("./transact.http.utilities");
/*packages**************************************************************/
//Let's check for the existance of the file 
//if it's there let's read it
//if not let's create it and write out the default settings so we have the persisted

//helper functions.....
function handleGetDiskFile(uri, req, res)
{
	var handled = false;
	try{
		var contentType = 'text/html';
		var extname = path.extname(uri);
		var contentType = httputils.getContentTypeFromExtension(extname);
		var fileExists = fs.existsSync(uri);
		var contents = '';
		if(fileExists)
		{
			contents = fs.readFileSync(uri);
			httputils.writeSuccessResponse(res, contentType, contents);
			handled = true;
		}
	}
	catch(err){
		httpserver.getLogger().LogMessage("An error occurred %s", [err]);
	}
	finally{
		return handled;
	}

	
}
function handleGetRESTDataDirect(uri,req, res)
{
	var handled = false;
	var pathname = null;
	try{
		var parsedurl = url.parse(uri, true);
		var pathname = httputils.normalizeURIPath(parsedurl.pathname);
		//we shoudl probably consider doing this further up the chain - not include the query params but it is a must for rest data
		var contents = httpserver.getDirectRESTData().getData(pathname);
		if(contents != null && contents != '')
		{
			var authorized = true;
			if(httpserver.getSessionManager()){
				authorized = httpserver.getSessionManager().validateSession(parsedurl, req);
			}
			if(authorized){
				var contentType = httputils.getContentTypeFromExtension('.json');
				httputils.writeSuccessResponse(res,contentType, contents);
			}
			else{
				httputils.writeErrorResponse(res, HTTP_RETURN_CODES.NOTAUTHORIZED);
			}
			handled = true;
		}
	}		
	catch(err)
	{
		httpserver.getLogger().LogMessage("Failed handleGetRESTDataDirect %s %s", [pathname, err]);
	}
	finally
	{
		return handled;
	}

}
function handleFileGet(uri, req, res)
{
	var handled = false;
	try{
		var path = httputils.localizeUri(httpserver.getContentDirectory(), uri);
		handled = handleGetDiskFile(path, req, res)
	
	}
	catch(err){
		httpserver.getLogger().LogMessage("Failed handle file get %s", [err]);
	}
	finally{
		return handled;
	}
	
	
}
function handleRESTAPI(uri, req, res){
	var handled = false;
	try{
		//we shoudl probably consider doing this further up the chain - not include the query params but it is a must for rest data
		var parsedurl = url.parse(uri, false);
		handled = httpserver.getRouteManager().executeRoute(parsedurl.pathname, req, res);
	}
	catch(err){
		httpserver.getLogger().LogMessage("Failed handleRESTAPI %s", [err]);
		
	}
	finally{

		return handled;
	}
}
function handlePut(uri,req, res)
{
	var handled = false;
	try{
		//we shoudl probably consider doing this further up the chain - not include the query params but it is a must for rest data
		var parsedurl = url.parse(uri, true);
		var authorized = true;
		var finalPath = httputils.normalizeURIPath(parsedurl.pathname);
		var keyExits = httpserver.getDirectRESTData().keyExists(finalPath);
		if(keyExits){
			handled = true;
			if(httpserver.getSessionManager()){
				authorized = httpserver.getSessionManager().validateSession(parsedurl, req);
			}
			if(authorized){
				var body = "";
				req.on('data', chunk => {
					body += chunk.toString(); // convert Buffer to string
				});
				req.on('end', ()=>{
					var value = JSON.parse(body);
					if(value != null){
						httpserver.getDirectRESTData().setData(finalPath, value.data);
						var contentType = httputils.getContentTypeFromExtension('.txt');//just send the ok, we don't care about any json or metadata
						httputils.writeSuccessResponse(res,contentType, "");
					}
					else{
						httputils.writeErrorResponse(res, HTTP_RETURN_CODES.SERVERERROR, "Body doesn't contain JSON data");
					}
					//var test = httpserver.getDirectRESTData().getData(finalPath);
					//console.log(test);
				});

			}
			else{
				httputils.writeErrorResponse(res, HTTP_RETURN_CODES.NOTAUTHORIZED);
				handled = true;
			}
		}
		else{
			handled = false;
		}
	}
	catch(err){
		httpserver.getLogger().LogMessage("Failed handlePut %s", [err]);
		if(keyExits){
			httputils.writeErrorResponse(res, HTTP_RETURN_CODES.SERVERERROR, err);
		}
		
	}
	finally{
		return handled;
	}
	
}
function handler(req, res)
{
	try{
		httpserver.getLogger().LogMessage("[debug] http request started\r\n");
		var filerequested;
		var handled = false;
		if(req.url[0] == '/')
			filerequested = "." + req.url;
		else
			filerequested = "./" + req.url;
		
		if(filerequested == './')
		{
			
			filerequested = "./index.html";
		}
		if(req.method == 'PUT')
		{
			handled = handlePut(filerequested, req, res);
		}
		else if(req.method == 'GET')
		{
			handled = handleGetRESTDataDirect(req.url, req, res);
			if(!handled){
				handled = handleFileGet(filerequested, req, res);
				
			}
			
		}
		if(!handled){
			handled = handleRESTAPI(req.url, req, res);
		}
		
		if(!handled)
		{
			httputils.writeErrorResponse(res);
			
		}
		httpserver.getLogger().LogMessage('[debug] http request %s - finished: url requested: %s path: %s', [req.method, req.url,filerequested ]);
	}
	catch(err){
		httpserver.getLogger().LogMessage('Failed handle request', [err]);
		
	}
	
}
var httpserver = module.exports = {
	logger : null,
	port : 80,
	host : "http://localhost",
	http_server : null,
	routemgr : null,
	contentDirectory : "./",
	directRESTData : null,
	sessionManager : null,
	httpHandler : null,
	getSessionManager(){
		return this.sessionManager;
	},
	getHTTPServerInstance(){
		return this.http_server;
	},
	getDirectRESTData(){
		return this.directRESTData;
	},
	getContentDirectory(){
		return this.contentDirectory;
	},
	getLogger(){
		if(this.logger == null){
			return {LogMessage : function(message){
				console.log(message);
			}};
		}
		else{
			return this.logger;
		}
	},
	getRouteManager(){
		return this.routemgr;
	},
	startHttpServer(){
		this.http_server = http.createServer(handler).listen(this.port);
		this.logger.LogMessage('Http server started successfully and running at %s on %s', [this.host, this.port]);
	},
	init(contentLocation="./", port=1337, host = "http://localhost", sessionManager = null, routemgr=null, directRESTData = null, logging = null, start = true){
		try
		{
			this.contentDirectory = contentLocation;
			this.directRESTData = directRESTData;
			this.logger = logging;
			this.host = host;
			this.port = port;
			this.routemgr = routemgr;
			this.sessionManager = sessionManager;
			this.httpHandler = handler;
			if(start){
				this.http_server = this.startHttpServer();
			}
			return this.http_server;
		}
		catch(err)
		{
			this.getLogger().LogMessage("Failed to start the http server" + err + " was returned.");
		}
	}
}

	



