#!/bin/bash
echo  Initializing Network Manager devices and connections - wlan0, wlan1
sudo nmcli radio wifi on
sudo nmcli radio all on
sudo nmcli d connect wlan0
sudo nmcli d connect wlan1
