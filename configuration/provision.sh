#Portal Provisioning STEPS/SCRIPT on the Pi 3B +
#------------------------------------------------------------------------------------------------------------------------------------
#RASPBIAN
#this should be run with elevated privlages sudo
#make sure the system is updated just as a precaution
#STEP 1 - UPGRADE
sudo apt-get update
#STEP 2 - STOP SYSTEM RELEVANT SERVICES
#this might be superfulous
sudo systemctl stop dhcpcd.service
sudo systemctl disable dhcpcd.service
#STEP 3 - INSTALL NODE
#get the node setup - in this case we are using node 10x but we COULD use the latest version. This should work on versions as old as 9.x
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
apt install nodejs
#STEP 4 - INSTALL NWM (if we continue to use it)
#see the following: https://www.raspberrypi.org/forums/viewtopic.php?t=223329
apt-get install network-manager
apt install network-manager network-manager-gnome openvpn openvpn-systemd-resolved network-manager-openvpn network-manager-openvpn-gnome
#STEP 6 REMOVE SERVICES/DAEMONS OR JUST KEEP THEM DISABLED - if the latter we can skip this
#These services could interfer with NWM so it is recommeneded we remove them. NWM employs its one libaries and helper daemons to perform these operations
sudo apt purge openresolv 
sudo apt purge dhcpcd5 # Disabling this in step 1 should be good enough so this is an extra precaution we could skip this 
#STEP 7
#https://wiki.debian.org/NetworkManager
#NetworkManager will only handle interfaces not declared in /etc/network/interfaces (see README file).
#https://wiki.gnome.org/Projects/NetworkManager
ln -sf /lib/systemd/resolv.conf /etc/resolv.conf
service network-manager restart
#STEP 8 SET DEVICES TO MANAGED AND AUTOCONNECT
sudo nmcli d set eth0 managed yes autoconnect yes
sudo nmcli d set wlan0 managed yes autoconnect yes
sudo nmcli d set eth1 managed yes autoconnect yes
#STEP 9 SCP THE ARCHIVE bps-web-portal-server.tar TO Pi (user home directory) 
scp <user>@<ipaddress> -f bps-web-portal-server.tar
#STEP 10 - SSH swtich to user directory where tarball has been copied
sudo mkdir /usr/bin/portal/
cd ~
#STEP 11 UNTAR/UNPACKAGE THE PORTAL PACAKGE
sudo tar -xvf  bps-web-portal-server.tar -C /usr/bin/portal/
#STEP 12- MARK THE SUPPORT/CGI SCRIPTS AS EXECUTABLE
sudo chmod +x /usr/bin/portal/bash-scripts/*.sh
#STEP 13 -  COPY SERVICE FILE
sudo cp /usr/bin/portal/configuration/boha.portal.service /etc/systemd/system/.
#STEP 14 -  ADD THE STARTUP SCRIPT TO rc.local
sudo sudo
echo /usr/bin/portal/configuration/boha.portal.service.sh >> /etc/rc.local
#STEP 15 -  Verify NetwowrkManager.conf
#Manually check that following is correct
sudo nano /etc/NetworkManager/NetworkManager.conf
#verify the following is correct if not change (under ifupdown) managed=true
#[ifupdown]
#managed=true
#STEP 16 - Verify 10-globally-managed-devices.conf
sudo nano /usr/lib/NetworkManager/conf.d/10-globally-managed-devices.conf
#verify the following is correct if not change - make sure that we can manager the ethernet device except:type:ethernet
#[keyfile]
#unmanaged-devices=*,except:type:ethernet,except:type:wifi,except:type:gsm,except:type:cdma
#STEP 17 = RESTART NWM
sudo service network-manager restart
#STEP 18 - REAPPLY SETTINGS TO MAKE SURE #16 TAKES HOLD
sudo nmcli d reapply eht0  
sudo nmcli d reapply wlan1  
#STEP 19 DELETE dev eth0 as default route
sudo ip route del default dev eth0
#STEP 20 - MAKE SURE THE ETHERNET DEVICE ISN'T THE DEFAULT ROUTE 
sudo nmcli c modify eth0 ipv4.never-default yes
#STEP 21 - REBOOT
sudo shutdown -r now

#------------------------------------------------------------------------------------------------------------------------------------
#THE FOLLOWING ARE FOR U-CORE CLASSIC
#STEP 1 - INSTALL CLASSIC
sudo snap install classic --devmode --edge
#STEP 2 - SWITCH TO CLASSIC
sudo classic
#STEP 3 - INSTALL NETWORK MANAGER
sudo apt-get install network-manager
#STEP 4 - INSTALL NODE (same as step 3 under raspbian)
#get the node setup - in this case we are using node 10x but we COULD use the latest version. This should work on versions as old as 9.x
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt install nodejs
#FOLLOW STEPS 8 - 19 UNDER RASPBIAN INSTALL (These steps are the same)

#------------------------------------------------------------------------------------------------------------------------------------
#THE FOLLOWING ARE FOR Ubuntu SERVER
#STEP 1 INSTALL NWM
sudo apt install network-manager
#STEP 2 INSTALL NETWORK TOOLS - THIS IS AN OPTIONAL STEP
sudo apt install net-tools
#STEP 3 - INSTALL NODE (same as step 3 under raspbian)
#get the node setup - in this case we are using node 10x but we COULD use the latest version. This should work on versions as old as 9.x
curl -sL https://deb.nodesource.com/setup_10.x | sudo bash -
sudo apt install nodejs
#FOLLOW STEPS 8 - 19 UNDER RASPBIAN INSTALL (These steps are the same) 
