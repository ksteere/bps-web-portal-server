/*Global node modules*/
var WebSocketServer = require('./lib/ws').Server;
var settings = require("./service-modules/transact.ps.settings");
/*generic library/modules*/ 
/*************************************************************************/
var routemgr = require("./lib/transact.route.manager");
var restdata = require("./lib/transact.rest.containers");
var loggingutils = require("./lib/transact.logging.utilities");
var sessionmanager = require("./lib/transact.session.manager");
var transacthttpserver = require("./lib/transact.http.server");
var commandline = require("./lib/transact.cmd.line.utilities");
var restapiplugins = require("./lib/transact.rest.api.plugins");
/*************************************************************************/
/*Service modules*/ 
/*************************************************************************/
var controllers = require("./service-modules/transact.ps.api.controllers");
/*Other globals**************************************************************/ 
/*************************************************************************/
var settignslocation = "./storage/settings";
var pluginslocation = "./storage/plugins";
//Below is the suggested location but we will still make this configurable through the command line argumentsu
///usr/local/transact/printstation/conf
var contentDirectory = "./content";
var authcontroller  = null; 
var settingscontroller = null;
var apiPluginsController = null;
var connectivityController = null;
//Test controller - not for use in production
var testController = null;
/*websocket for client*/
var ws_socket_tunnel = null;/*websocket tunnel*/
var wss =null;
var mode = null;
/*************************************************************************/
//|Name:startService
//|Summary:function is responsible for the nuts and bolts of
//|bootstrapping the module
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|Parmameters:[]
//|Return:
//|Author:
function startService(){
    //some classes are designed as modules which act like classes but are designed to be more like static implementations
    //and as such we call their init (we could do this through a constructor but it would be less naturall and be a bit of an anti pattern)
    //in init we inject our dependencies
    //Map any rest objects but we will need to handle 
    restdata.mapObject("/api/v1/setting/",settings.root);
    restdata.addEventListener(restDataChanged); 
    routemgr.init(sessionmanager);
    //These controllers are solution specific and thus ok to be intantiated like a normal class, in some cases we may want to have may instances though
    //controllers aren't the best example of that - we could've implemented these like modules
    settingscontroller = new controllers.SettingsController(routemgr, settings.root, loggingutils, settingsObjectChanged);
    authcontroller = new controllers.AuthenticationController(routemgr, settings.root, loggingutils);
    apiPluginsController = new controllers.APIAddPluginsController(routemgr, restdata,"/api/v1/setting/", restapiplugins.plugins, loggingutils);
    connectivityController = new controllers.ConnectivityController(routemgr, sessionmanager, loggingutils);
    //Run the system/integration tests if so desired. This should and will be decided from a commmand line option mode=test
    if(mode == "test"){
        transacthttpserver.init(contentDirectory, settings.root.http.port, settings.root.http.host, sessionmanager, routemgr, restdata, loggingutils, false);
        testController = new controllers.TestController(transacthttpserver, sessionmanager, loggingutils);   
        runTests(16,true, function(tests){
            console.log("%d tests completed", tests);
            process.exit(0);

        });
    }
    else if(mode == "info"){//Print out information about this app mode=info
        printOutAPIs();
        process.exit(0);
    }
    else{//Run the app normally
        //Another "static" like implementation here for the servers
        var httpserver = transacthttpserver.init(contentDirectory, settings.root.http.port, settings.root.http.host, sessionmanager, routemgr, restdata, loggingutils);
        testController = new controllers.TestController(transacthttpserver, sessionmanager, loggingutils);     
        //Web sockets are really for later use
        wss = new WebSocketServer({server: httpserver, port: settings.websocket.port});
        wss.on('connection', webSocketConnected);
        //this is our session managment code
        //the library/module is used to operate and store the session data as well as the magic that goes into identifying a session
        //but we need to invoke its eviction logic as this module doesn't perform any async or self timed operations
        //and nor it should. the boot strap module should be responsible for doing ths
        setInterval(() => {
        sessionmanager.removeExpiredSessions(settings.root.sessiontoken.expirationDelta);
        }, settings.root.sessiontoken.evictionInterval);
        
    }
   
}
//|Name:settingsObjectChanged
//|Summary:event handler for when a (settings) controller updates the settings object or needs to
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|Parmameters:[]
//|Return:
//|Author:
function settingsObjectChanged(newsettings){
    let succeded = false;
    try{
        //***NOTE****
        //This could be an issue because we are setting the object in memory so if the following line fails then
        //we will be out of sync. This was due to the fact that using the class this way was not initially considered. Perhaps having a function that 
        //wraps persist file that both saves then sets the internal object makes more sense. Something like setAndPersistSettingsOjbect
        succeded = settings.persistSettingsObject(settignslocation, newsettings);
        if(succeded){
            settings.root = newsettings; //rememeber we are saving only the root mamember variable to which the entire (internal) object is
            //constructed. All dependent classes are passed settings.root NOT the settings object
            restdata.mapObject(settings.root); //lets update the restdata object and remap the data
        }
        
    }
    catch(error){
        loggingutils.LogMessage("An error occurred while updating new settings data %s.", [error]);
    }
    finally{
        return succeded;
    }
}
//|Name:setObjectValue
//|Summary:sets an objects value
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|Parmameters:[]
//|Return:
//|Author:
function setObjectValue(property, current, object, value){
    var keys = Object.keys(object);
    for(var index = 0; index < keys.length; index ++){
        var key = keys[index];
        var tempCurrent = current + key;
        var  currentObject = object[key];
        if(property.toLowerCase() ==  tempCurrent.toLowerCase()){
            //console.log("Found it! " + property + " = " + tempCurrent + ", value = " + object[key].toString() +" " + value);
            Object.defineProperty(object, key,{value: value, writable: true}); 
            return true;
        }
        else{
            if(currentObject != null && typeof currentObject  == 'object' ){
                var found = setObjectValue(property, tempCurrent + "/", currentObject, value);
                if(found){
                    return found;
                }
                
            }
        }
        
    }
    return false;
    
}
//|Name:restDataChanged
//|Summary:callback event handler for data when it has changed
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|Parmameters:[path to the rest data, the data that was changed]
//|Return:
//|Author:
function restDataChanged(path,data){
    try{
        console.log("The rest data @ path %s has changed.", path);
        var regex = (/(?<=\/api\/v1\/setting\/).*/i).exec(path);
        if(regex!=null){
            var object = regex[0];
            setObjectValue(object, "",settings.root, data);
            settings.persistSettingsObject(settignslocation);
        }
    }
    catch(error){
        loggingutils.LogMessage("An error occurred while handling settings data changes %s.", [error]);
    }
   
}
//|Name:webSocketConnected
// |Summary:called when a client connects to a ws tunnel
// |this is an event handler
// |-----------------------------
// |-----------------------------
// |-----------------------------
// |Parmameters:[a tunnel/socket/client connection]
// |Return:
// |Author:
function webSocketConnected(ws)
{
    //in order to handle this appropriately we need to create a map of client sockets and their session information
    //but no time for that now and since this isn't really being used we can implment this later when real time data is needed
    //or long polling won't do
    ws_socket_tunnel = ws;
	 ws.on('message', webSocketMessage);
	 loggingutils.LogMessage('[debug] web socket connected...');
	
}
// |Name:webSocketMessage
// |Summary:when a WS message is received from the client
// |-----------------------------
// |-----------------------------
// |-----------------------------
// |-----------------------------
// |Parmameters:[string message]
// |Return:
// |Author:
function webSocketMessage(message)
{
    loggingutils.LogMessage('received: %s', message);
}
// |Name:run
// |Summary:Main entry point for this module and 
// |the application/service
// |-----------------------------
// |-----------------------------
// |-----------------------------
// |Parmameters:[]
// |Return:
// |Author:

function run(){
    //initalize the settings module first as the members are all nulled out
    //and set to their defaults
    restapiplugins.init();
    var tmpsettings = commandline.getArgumentValue("settings-location");
    var tmpplugins = commandline.getArgumentValue("plugins-location");
    mode = commandline.getArgumentValue("mode");
    if(tmpsettings != null){
        settignslocation = tmpsettings;
    }
    if(tmpplugins != null){
        pluginslocation = tmpplugins;
    }
    //next read the settings files so that we can read any saved settings from the file
    settings.readSettingsFile(settignslocation, true);
    restapiplugins.loadPluginsMetadata(pluginslocation, true);
    //finally read the command line for any temporary settings or dynamic settings - these settings won't be saved
    //UNLESS we call persist then what ever is in memory will be saved
    commandline.commandLineToObject(settings.root, ["settings-location", "mode","plugins-location"]);
    //start the main service and it's child service libraries like the http server
    startService();
};
//|Name:runTests
//|Summary: Run tests from the command line
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|-----------------------------
//|Parmameters:[]
//|Return:
//|Author:
function runTests(test = 1, verbose=true, callback=null){
    
    var restpath = "/api/v1/setting/authentication/username";
    var tests = 19;
    var testname = "";
    var data = null;
    try{
        switch(test){
            case 1:
                testname = "Delete proxy";
                testController.testAPI("/api/v1/system/proxy/setting", "DELETE", verbose);
                break;
            case 2:
                testname = "Set proxy";
                testController.testAPI("/api/v1/system/proxy/setting", "POST", verbose, "http://nutsacks.com");
                break;
            case 3:
                testname = "Set avahi service name";
                testController.testAPI("/api/v1/system/avahi/name", "POST", verbose, "Donkeys");
                break;
            case 4:
                testname = "Get avahi service name";
                testController.testAPI("/api/v1/system/avahi/name", "GET", verbose);
                break;
            case 5:
                testname = "Get proxy setting";
                testController.testAPI("/api/v1/system/proxy/setting", "GET", verbose);
                break;
            case 7:
                testname = "Get all settings";
                testController.testAPI("/api/v1/settings", "GET", verbose);
                break;
            case 8:
                testname = "Get connections";
                testController.testAPI("/api/v1/connectivity/connections", "GET", verbose);
                break;
            case 9:
                testname = "Get connection settings";
                testController.testAPI("/api/v1/connectivity/connection/setting", "POST", verbose, 
                JSON.stringify({connection:"poop", name:"connection.id", value:"Orbital", recycle: false}));
                break;
            case 10:
                testname = "Get REST (object) data";
                testController.testAPI(restpath, "GET", verbose, null, verbose);
                break;
            case 11:
                testname = "Set REST (object) data";
                testController.testAPI(restpath+"/admin-new", "PUT", verbose, null, verbose);
                break;
            case 12:
                testname = "Get REST (object) data";
                testController.testAPI(restpath, "GET", verbose, null, verbose);
                break;
            case 13:
                testname = "Get settings.connections";
                testController.testAPI("/api/v1/settings/connections", "GET", verbose);
                break;
            case 14:
                testname = "Authentication";
                testController.testAPI("/api/v1/user/authenticate", "POST", verbose, 
                JSON.stringify({username: "admin-new", password: "password"}));
                break;
            case 15:
                testname = "Connect";
                testController.testAPI("/api/v1/connectivity/connect", "POST", verbose, 
                JSON.stringify({ssid: "Orbital", password: "MercuryVenusEarthMars", adapter : "wlan0"}));
            break;
            case 16:
                testname = "Get second adapter from connections";
                testController.testAPI("/api/v1/setting/connections/1/adapter", "GET", verbose);
            break;
            case 17:
                testname = "Update admin password via POST";
                testController.testAPI("/api/v1/settings/user/admin/password", "POST", verbose, 
                JSON.stringify({username:"admin", password:"password1", newpassword:"password"}));
            break;
            case 18:
                testname = "Get admin password via GET and REST";
                testController.testAPI("/api/v1/setting/authentication/password", "GET", verbose);
            break;
            case 19:
                testname = "GET REST DATA connections type";
                testController.testAPI("api/v1/setting/6connections/0/type", "GET", verbose);
            break;
           
        }
        loggingutils.LogMessage("Running test %s %s", [testname, data == null ? "" : data]);
    
    }
    catch(error){
        loggingutils.LogMessage("Failed running test %s - %s.", [testname, error]);
     
    }
    if(test < tests){
        setTimeout(()=>{
            runTests(++test, verbose, callback);
        }, 1000);
    }
    else{
        if(callback){
            callback(test);
        }
        
    }
 
    
}
function printOutAPIs(){
    var apiroutes = restdata.exportRoutes().concat(routemgr.exportRoutes());
    console.log("----------------------------------------------------------------------------------------");
    console.log("--------------------------------Available API's-----------------------------------------");
    console.log("----------------------------------------------------------------------------------------");
    var index =1;
    apiroutes.forEach(item=>{
        console.log("%d.) path: %s, verb: %s, type: %s", index++, item.apiPath, item.verb, item.type);
    });
} 
//This is what starts it all off
run();