import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';
import { AppComponent } from './app.component';
import { HomeComponent } from './home/home.component';
import { AppRoutingModule } from './app-routing.module';
import { ConnectivityComponent } from './connectivity/connectivity.component';
import { LoginComponent } from './login/login.component';
import { RouteGuardService } from './common/services/route.guard.service';
import { FormsModule } from '@angular/forms';
import { TokenInterceptor } from './common/services/session.token.intercepter.service';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { AdminComponent } from './admin/admin.component';
import { SystemComponent } from './system/system.component';
import { AvahiComponent } from './avahi/avahi.component';
import { PopUpMessageComponent } from './common/ui-widgets/pop-up-message/pop-up-message.component';
import { ModalConfirmationComponent } from './common/ui-widgets/modal-confirmation/modal-confirmation.component';
@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ConnectivityComponent,
    LoginComponent,
    AdminComponent,
    SystemComponent,
    AvahiComponent,
    PopUpMessageComponent,
    ModalConfirmationComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    BrowserAnimationsModule
  ],
  providers: [RouteGuardService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: TokenInterceptor,
      multi : true
    },
    PopUpMessageComponent
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor(private routeGuard : RouteGuardService){
    
  }
  ngInit(){


  }


}
