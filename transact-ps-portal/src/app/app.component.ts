import { Component } from '@angular/core';
import { fadeTransition } from './common/services/route-animation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css'],
  animations: [ fadeTransition ]
})
export class AppComponent {
  title = 'BOHA! Workstation';
  constructor() {
  }
}
