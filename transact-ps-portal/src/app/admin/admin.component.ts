import { Component, OnInit, ViewChild } from '@angular/core';
import { PageHeaderService } from '../common/services/page.header.service';
import { SettingsService } from '../common/services/settings.service';
import { UserInformation } from '../common/data/settings.user.information';
import { PopUpMessageComponent } from '../common/ui-widgets/pop-up-message/pop-up-message.component';

@Component({
  selector: 'app-admin',
  templateUrl: './admin.component.html',
  styleUrls: ['./admin.component.css'],

})

export class AdminComponent implements OnInit {
  private userPassword1 : String = "";
  private userPassword2 : String = "";
  private currentPassword : String = "";
  private userName : String = "admin";
  private userInfoTouched : Boolean = false;
  @ViewChild(PopUpMessageComponent) popup : PopUpMessageComponent
  constructor(private pageHeaderService : PageHeaderService, private settingsService : SettingsService) { }

  ngOnInit() {
  }
  ngAfterViewInit(){
    console.log("Admin view init....");
    this.pageHeaderService.setHeaderTitle("User Administration", "fa-user");
  }
  ngDestroy(){
    
  }

  private clearUserCredentials(){
    this.currentPassword = "";
    this.userPassword1 = "";
    this.userPassword2 = "";
  }
  saveUserInformation(){
    let userInfo : UserInformation = new UserInformation();
    userInfo.newpassword = this.userPassword1.toString();
    userInfo.password = this.currentPassword.toString();
    userInfo.username = this.userName.toString();
    if(this.userPassword1 == this.userPassword2){
      this.settingsService.adminPassword(userInfo).subscribe((resp)=>{
        console.log(resp);
        this.popup.displayMessage(true, "Successfully updated user information.");
        this.clearUserCredentials();
        this.userInfoTouched = false;
      },(err)=>{
        this.popup.displayMessage(false, "An error occured while attempting to update the user. " + err.error);
      });
    }
    else{
      this.popup.displayMessage(false, "Passwords must match. Please verify.");
    }

  }
  public userPasswordKeyEvent(event : Event){
    this.userInfoTouched = true;
  }
}
