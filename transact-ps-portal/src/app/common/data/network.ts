export class Network{
    state: string;
    ssid: string;
    channel: number;
    frequency: string; // in MHz
    signal_level: number; // in dB
    bars : number;
    quality: number; // same as signal level but in %
    security: string; // format depending on locale for open networks in Windows
    device : string;
    constructor(){

    }
}