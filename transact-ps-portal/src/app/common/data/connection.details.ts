import { e } from "@angular/core/src/render3";

export class ConnectionDetails{
    hostname: String;
    id: String;
    type: String;
    method: String;
    mac: String;
    security: String;
    mode: String;
    hidden: String;
    primaryAddress: String;
    secondaryAddress : String;
    dns: String;
    gateway: String;
    requested_subnet_mask: String;
    requested_rfc3442_classless_static_routes: String;
    subnet_mask: String;
    domain_name_servers: String;
    ip_address: String;
    requested_static_routes: String;
    dhcp_server_identifier: String;
    requested_time_offset: String;
    broadcast_address: String;
    requested_interface_mtu: String;
    dhcp_rebinding_time: Number;
    requested_domain_name_servers: String;
    dhcp_message_type: String;
    requested_broadcast_address: String;
    routers: String;
    wpad: String;
    dhcp_renewal_time: Number;
    requested_domain_name: String;
    requested_routers: String;
    expiry: Date;
    requested_wpad: String;
    host_name: String;
    requested_netbios_scope: String;
    requested_ms_classless_static_routes: String;
    requested_netbios_name_servers: String;
    network_number: String;
    requested_domain_search: String;
    requested_ntp_servers: String;
    next_server: String;
    requested_host_name: String;
    dhcp_lease_time: Number;
    ssid: String;

    constructor(){

    }
}
