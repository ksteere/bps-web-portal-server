export const enum ConnectivityMethod {
    NONE = 0,
    AP = 1,
    Wifi = 2,
    Ethernet = 3,
    LTE = 4
  }