
export class DeviceDetails{
    hostname:String;
    device:String;
    type:String;
    mac:String;
    mtu:String;
    state:String;
    connection:String;
    dbus:String;
    ip4address_1:String;
    ip4gateway:String;
    ip4route_1:String;
    ip4dns_1:String;
    ip6address_1:String;
    ip6gateway: String;
    constructor(){

    }
}

