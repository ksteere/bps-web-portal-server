export class IPConnectionMethod{
    ip : String;
    subnet : String;
    gateway : String;
    method : String; //auto, link-local, manual, shared, disabled
    connection: String;
    constructor(){

    }
}