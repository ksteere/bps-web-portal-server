import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators'
import { throwError } from 'rxjs';
import { Network } from '../data/network';
import { WifiCredentials } from '../data/wifi.credentials.';
import { ConnectionDetails } from '../data/connection.details';
import { DeviceDetails } from '../data/device.details';
import { ClientConnectionEcho } from '../data/client.connection.echo';
import { AvahiService } from '../data/avahi.service';
import { HttProxy } from '../data/http.proxy';
import { resolve } from 'url';
@Injectable({
  providedIn: 'root'
})
export class SystemService {
  baseUrl: string = "/api/v1/system/";
  httpHeaders = {
    headers: new HttpHeaders({
    'Accept': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }
  public getAvahiName() : Observable<AvahiService>
  {
    let url = this.baseUrl + "avahi/name";
     return this.http.get<AvahiService>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
      
  }
  public setAvahiName(serviceData : AvahiService) : Observable<any>{
    let url = this.baseUrl + "avahi/name";
    return this.http.post<any>(url,serviceData,this.httpHeaders).pipe(
      catchError(this.handleError)
    );
  }
  
  public setProxySetting(proxySetting : HttProxy) : Observable<any>{
    let url = this.baseUrl + "proxy/setting";

    // JRN JIRA WWP-4 Make changing proxy setting persistent
    // make return type of post call match the Observable type
    return this.http.post<any>(url,proxySetting,this.httpHeaders).pipe(
      catchError(this.handleError)
    );
  }

  public getProxySetting() : Observable<HttProxy>
  {
    let url = this.baseUrl + "proxy/setting";   
    return this.http.get<HttProxy>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
      
  }
  ///////////////ASYNC FUNCTION///////////////////////////
  getProxySetting_async() : Promise<HttProxy>{
    return new Promise((resolve, reject)=>{
      this.getProxySetting().subscribe((resp)=>{
        resolve(resp);
      },(err)=>{
        reject(err);
      })
    });
  }

public setProxySetting_async(proxySetting : HttProxy) : Promise<any>{
  return new Promise((resolve, reject)=>{
    this.setProxySetting(proxySetting).subscribe((resp)=>{resolve(resp);}, (err)=>{reject(err)});
  });

}
public getAvahiName_sync() : Promise<AvahiService>
{
  return new Promise((resolve, reject)=>{
    this.getAvahiName().subscribe((resp)=>{resolve(resp);}, (err)=>{reject(err)});
  });
    
}
public setAvahiName_async(serviceData : AvahiService) : Promise<any>{
  return new Promise((resolve, reject)=>{
    this.setAvahiName(serviceData).subscribe((resp)=>{resolve(resp);}, (err)=>{reject(err)});
  });
}

  handleError(error: HttpErrorResponse){
      console.log(error);
      return throwError(error);
    }
}
