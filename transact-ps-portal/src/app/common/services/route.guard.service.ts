import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Injectable} from '@angular/core';
@Injectable({
    providedIn: 'root'
  })
export class RouteGuardService implements CanActivate
{
  private circularMenuId : string = "index-circular-menu";
  private isAuthenticated : boolean;
  private activeUser : string;
  private router : Router;
  private sessionToken : string;
  private sessionStoreUserKey = "boha-portal:user";
  private sessionStoreTokenKey = "boha-portal:token";
  constructor(_router : Router) 
  {    
      this.isAuthenticated = false;
      this.router = _router;
      console.log(_router);
      this.setAuthenticationFlag("", "", false, false);
      _router.events.subscribe((event)=>{
        //console.log(event);
      
      });
  }
  public checkForStoredSessionData(){
    let token : string = sessionStorage.getItem(this.sessionStoreUserKey);
    let user : string  = sessionStorage.getItem(this.sessionStoreTokenKey);
    if(user != null && token != null){
      this.setAuthenticationFlag(user, token, true, false);
    }
  }
  public getActiveUser(){
    return this.activeUser;
  }
  public getSessionToken(){
    return this.sessionToken;
  }
  public getIsAuthenticated(){
    return this.isAuthenticated;
  }
  public setAuthenticationFlag(user : string, token : string, authenticated : boolean, routehome : boolean){
    this.activeUser = user;
    this.sessionToken = token;
    this.isAuthenticated = authenticated;
    if(this.isAuthenticated){
     
      if(routehome){
        this.router.navigate(["home"]);
      }
      this.makeMenuAvailable(true);
      sessionStorage.setItem(this.sessionStoreUserKey, user);
      sessionStorage.setItem(this.sessionStoreTokenKey, token);
    }
    else{
      //sessionStorage.clear();
      sessionStorage.removeItem(this.sessionStoreUserKey);
      sessionStorage.removeItem(this.sessionStoreTokenKey);
      this.makeMenuAvailable(false);
    }
  }
  public makeMenuAvailable(available){
    if(available){
      document.getElementById(this.circularMenuId).style.display = "block";
    }
    else{
      document.getElementById(this.circularMenuId).style.display = "none";
    }
  }
  public canActivate(route: ActivatedRouteSnapshot, state: RouterStateSnapshot) : boolean
  {
    if(!this.isAuthenticated){
        console.log("Not authenticated redirected...");
        console.log(this.isAuthenticated);
        this.router.navigate(["login"]);
    }
    return this.isAuthenticated;
  }
}
