
import { Injectable, HostListener } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import {ConnectivityMethod} from '../data/global.enumerations'
import { removeClass, addClass } from "./global-helper-functions";
@Injectable({
    providedIn: 'root'
})
export class PageHeaderService {
    private headerElement: any;
    private autoRefreshButton : any;
    private autoRefreshOn : boolean;
    private autoRefreshOnIcon : any;
    private autoRefreshOffIcon : any;
    private menuItemOpen : any;
    private connectivityState : ConnectivityMethod;
    private navConnectionModeIcon : any;
    public autoRefreshClicked: BehaviorSubject<boolean> = new BehaviorSubject(false);
    constructor() {
        this.headerElement = document.getElementById("main-page-header");
        this.autoRefreshButton = document.getElementById("main-page-auto-refresh");
        this.autoRefreshOnIcon = document.getElementById("main-page-auto-refresh-on");
        this.autoRefreshOffIcon = document.getElementById("main-page-auto-refresh-off");
        this.menuItemOpen = document.getElementById("menu-open");
        this.navConnectionModeIcon = document.getElementById("main-page-connection-icon");
        document.addEventListener('click', (event : any)=>{
            if(event.target.id != "menu-open" && event.target.id != "menu-open-label"){
                this.menuItemOpen.checked = false;
            }
        });
        this.autoRefreshButton.addEventListener('click', (event)=>{
            this.autoRefreshOn = !this.autoRefreshOn;
            if(this.autoRefreshOn){
                this.autoRefreshOnIcon.style.display = "inline";
                this.autoRefreshOffIcon.style.display = "none";
            }
            else{
                this.autoRefreshOnIcon.style.display = "none";
                this.autoRefreshOffIcon.style.display = "inline";  
            }
            this.autoRefreshClicked.next (this.autoRefreshOn);
        });

        
    }
    public getAutoRefresh(){
        return this.autoRefreshOn;
    }
    public setHeaderTitle(title: string, iconClass : string = null) {
        if(iconClass != null){
            let spacing = "&nbsp;&nbsp;"
            let faIcon : string = "<i class=\"fad fa-lg " + iconClass + "\"></i>";
            this.headerElement.innerHTML = faIcon + spacing + title;
        }
        else{
            this.headerElement.innerHTML = title;
        }
    }

    public setConnectivityIconState(state : ConnectivityMethod  = ConnectivityMethod.AP){
        removeClass(this.navConnectionModeIcon, "fa-wifi-1");
        removeClass(this.navConnectionModeIcon, "fa-signal");
        removeClass(this.navConnectionModeIcon, "fa-wifi");
        removeClass(this.navConnectionModeIcon, "fa-router");
        removeClass(this.navConnectionModeIcon, "fa-ethernet");
        if(state == ConnectivityMethod.AP){
            addClass(this.navConnectionModeIcon,"fa-router");
        }
        else if(state == ConnectivityMethod.Wifi){
            addClass(this.navConnectionModeIcon, "fa-wifi");
        }
        else if(state == ConnectivityMethod.Ethernet){
            addClass(this.navConnectionModeIcon, "fa-ethernet");
        }
        else if(state == ConnectivityMethod.LTE){
            addClass(this.navConnectionModeIcon, "fa-signal");
        }
        else{
            addClass(this.navConnectionModeIcon, "fa-wifi-1");
        }
        this.connectivityState = state;
    }
    public setHeaderIcon(iconClass: string, concat : boolean = false) {
        if(concat){
            let spacing = "&nbsp;&nbsp;"
            let faIcon : string = spacing + "<i class=\"fad fa-lg " + iconClass + "\"></i>" ;
            this.headerElement.innerHTML += faIcon;
        }
        else{
            let spacing = "&nbsp;&nbsp;"
            let faIcon : string = "<i class=\"fad fa-lg " + iconClass + "\"></i>" + spacing;
            this.headerElement.innerHTML = faIcon;
        }
       
    }
}
