import { HttpErrorResponse } from "@angular/common/http";

export function removeClass(element : any, className : String){
    if(element != null){
        var tempclasses = element.className.replace(className);
        if(tempclasses != null && tempclasses != undefined){
            element.className = tempclasses;
        }
    }
}
export function addClass(element : any, className : String){
    if(element != null){
        element.className += " " + className;
    }
    
}

export function parsePrimaryIp(ipAddress : String, network : Boolean = true){
    if(ipAddress != null && ipAddress != undefined){
        let parts =ipAddress.split("/");
        if(parts != null && parts.length == 2){
            if(network){
                return parts[0];
            }
            else{
                return parts[1];
            }
            
        }
        else{
            return ipAddress
        }
    }
    else{
        return null;
    }

}

export function isStringEmpty(value : String){
    if(!isNullOrUndefined(value)){
        return value.length <= 0;
    }
    else{
        return true;
    }
}
export function isNullOrUndefined(object : any){

    let sanityCheck : boolean = (object == undefined || object == null); 
    return sanityCheck;
 
}
export function isValidOctet(octet : Number){
    console.log(octet);
    return (octet != NaN && octet <= 255)
  }
  export function validateIPRegEx(ip : String) {
    const first3BytesRg = /^(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?))$/;
    const fourthByteRg = /^(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)$/;
    var octets = ip.split(".");
    var nb = octets.length;
    
    if (nb > 4) return false;
    if (octets[nb - 2] == "") return false;
    if (octets[nb - 1] == "") return true;
    
    
    if (nb < 4) {
      return first3BytesRg.test(octets[nb - 1]);
    }
    
    return fourthByteRg.test(octets[nb - 1]);
  }
export function validateIPAddress(address : String){
    let addressParts = address.split(".");
    let isOK :boolean = true; 
    if(addressParts.length == 4){
      let part1 = Number.parseInt(addressParts[0]);
      let part2 = Number.parseInt(addressParts[1]);
      let part3 = Number.parseInt(addressParts[2]);
      let part4 = Number.parseInt(addressParts[3]);
      isOK = isValidOctet(part1) &&  isValidOctet(part2) && 
      isValidOctet(part3) && isValidOctet(part4);
    }
    else if(address ==""){
      isOK = true;
    }
    else{
        isOK = false;
    }
    return isOK;
  }
  export function getSubnetFromMask(address : String){
    let addressParts = address.split(".");
    if(addressParts.length == 4){
      let part1 = Number.parseInt(addressParts[0]);
      let part2 = Number.parseInt(addressParts[1]);
      let part3 = Number.parseInt(addressParts[2]);
      let part4 = Number.parseInt(addressParts[3]);
      if(part1 == 255 && part2 == 255 && isValidOctet(part3) && isValidOctet(part4)){
        let addressRange : number = (256 - part3) * (256 - part4);
        let dot = Math.round(32 - Math.log2(addressRange)); //we could do more checking both with respect to the range parts 
        //and the dot range but if the user enters and invalid mask and we don't catch it...well to bad we will decide for them.
        console.log(dot);
        return dot;
      }
      else{
        return -1;
      }
    }
    else{
      return -1;
    }
  }
  export function getMaskFromSubnetDot(dot : string){
    let iDot = Number.parseInt(dot);
    let addressBase : number = 32 - iDot;
    let mask = ["255", "255", "255", "255"];
    if(addressBase > 0 && addressBase <= 16){ //we are a class C network
        let hostBase = 16  - addressBase; //addressBase should be less than 16
        if(hostBase <= 8){
            let fourthOctet = 256 - Math.pow(2, addressBase);
            mask[3] = fourthOctet.toString();
        }
        else if(hostBase > 8 && hostBase <= 16){

            let thirdOctet = 256 - Math.pow(2, hostBase - 8);
            let fourthOctet = 256 - Math.pow(2, 16 - hostBase); //algebraically speaking....we could just use addressBase as 16 - hostBase = addressBase 
                                                                //because 16 - addresBase = hostBase
            mask[2] = thirdOctet.toString();
            mask[3] = fourthOctet.toString();
        }


    }
    return mask.join(".");

  }
  export function getFormattedErrorMessage(message : String, exception : any) : String{
    let finalMessage : String;  
    if(exception instanceof HttpErrorResponse){
        finalMessage = message + " details: " + exception.error;
    }
    else{
        finalMessage =  message + " details " + exception.toString();
    }
    return finalMessage;
  }
  export function assembleProxyAddress(parts : any){
    let proxy : string = "";
    let protocol = /http+[s]?:\/\//.exec(parts.url);
    let tempUserNamePassword :string = "";
    if(!isStringEmpty(parts.username)){
      if(!isStringEmpty(parts.password)){
        tempUserNamePassword = parts.username + ":" + parts.password + "@";
      }
      else{
        tempUserNamePassword = parts.username  + "@";
      }
    }
    if(protocol != null && protocol.index != -1){
      proxy = parts.url.replace(protocol[0], protocol[0]+tempUserNamePassword);
    
    }
    else{
      proxy = parts.url + tempUserNamePassword
    }
    if(!isStringEmpty(parts.port)){
      proxy += ":" + parts.port;
    }
    return proxy;
  }
  export function getProxyParts(proxyAddress : string){
    // JRN JIRA WWP-1 - Rework so that Safari doesn't blow up on use of regular expressions.
    // Now using JS URL() class to do the parsing instead of custom parsing code.

    let proxyParts = { username: "", password: "", url: "", port: null };
    // Strip off any characters before http:// or https:// for the URL() class
    let location = proxyAddress.indexOf("http://");
    if( location == - 1 )
    {
        location = proxyAddress.indexOf("https://");
    }
    if( location > 0 )
    {
        proxyAddress = proxyAddress.substr(location);
    }

    // create new URL object from passed in proxy string
    try
    {
      var url = new URL(proxyAddress);
      // use URL() method to parse and set values in proxyParts struct
      proxyParts.username = url.username;
      proxyParts.password = url.password;
      proxyParts.url = url.hostname;
      // if port isn't empty set it. Otherwise the default of null has been set above as the default
      if( url.port ) {
          proxyParts.port = url.port;
      }
      console.log(proxyParts);
    }
    catch(err) {}
    return proxyParts;
  }