import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import {PortalUser} from '../data/portal-user'
import { catchError, retry } from 'rxjs/operators'
import { throwError } from 'rxjs';
import { UserCredentials } from '../data/user.credentials';
@Injectable({
  providedIn: 'root'
})
export class AuthService {
  baseUrl: string = "/api/v1/user/"; ///login?userName=bella&password=bella";  
  httpHeaders = {
    headers: new HttpHeaders({
    'Accept': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }
  public authenticate(credentials : UserCredentials) : Observable<PortalUser>
  {
    let url = this.baseUrl + "authenticate";
      console.log(url);
      return this.http.post<PortalUser>(url,credentials, this.httpHeaders).pipe(
        catchError(this.handleError)
        
      );
      
  }
  public verifyToken(token : string) : Observable<string>{
    let url = this.baseUrl + "token";
    console.log(url);
    return this.http.post<string>(url,token, this.httpHeaders).pipe(
      catchError(this.handleError)
      
    );
    
  }
  handleError(error: HttpErrorResponse){
      console.log(error);
      return throwError(error);
    }
}
