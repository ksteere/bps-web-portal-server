import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse, HttpParams } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators'
import { throwError } from 'rxjs';
import { Network } from '../data/network';
import { WifiCredentials } from '../data/wifi.credentials.';
import { ConnectionDetails } from '../data/connection.details';
import { DeviceDetails } from '../data/device.details';
import { ClientConnectionEcho } from '../data/client.connection.echo';
import { ConnectionSetting } from '../data/connection.setting';
import { IPConnectionMethod } from '../data/ip.connection.method';
import { DeviceInfo } from '../data/device.info';
import { ConnectionInfo } from '../data/connection.info';
import { HttpParamsOptions } from '@angular/common/http/src/params';

@Injectable({
  providedIn: 'root'
})
export class ConnectivityService {
  baseUrl: string = "/api/v1/connectivity/";
  httpHeaders = {
    headers: new HttpHeaders({
    'Accept': 'application/json',
    })};
  constructor(private http: HttpClient) { }
  public uplinkTest(site : string) : Observable<string>{
    let url = site;//"https://cors-anywhere.herokuapp.com/" + site;
     return this.http.get(url, {headers : {'Accept': 'text/html', 'x-Requested-With' : 'XMLHttpRequest'}, responseType : "text"}).pipe(
        catchError(this.handleError)
      );
  }
  public networks(adapter : String) : Observable<Network[]>
  {
    let url = this.baseUrl + "wifinetworks?adapter="+adapter;
     return this.http.get<Network[]>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
  }
  public connectionDetails(connection : String) : Observable<ConnectionDetails>
  {
      let url = this.baseUrl + "connection/details?name="+connection;
      return this.http.get<ConnectionDetails>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
      
  }
  public connectionDelete(connection : ConnectionInfo) : Observable<any>
  {
      let url = this.baseUrl + "connection/delete";
      return this.http.post<ConnectionInfo>(url, connection, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
      
  }
  public devices() : Observable<DeviceInfo[]>
  {
    let url = this.baseUrl + "devices" ;
      return this.http.get<DeviceInfo[]>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
  }
  public connections() : Observable<ConnectionInfo[]>
  {
    let url = this.baseUrl + "connections" ;
      return this.http.get<ConnectionInfo[]>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
  }
  public deviceDetails(device : String) : Observable<DeviceDetails>
  {
    let url = this.baseUrl + "device/details?device="+device;
      return this.http.get<DeviceDetails>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
      );
  }
  public connectWifi(credentials : WifiCredentials) : Observable<any>{
    let url = this.baseUrl + "wifi/connect";
    return this.http.post<any>(url, credentials,this.httpHeaders).pipe(
      catchError(this.handleError)
    );
  }
  public hostConnectionEcho() : Observable<ClientConnectionEcho>
  {
    let url = this.baseUrl + "echo/host/connection" ;
    return this.http.get<ClientConnectionEcho>(url, this.httpHeaders).pipe(
      catchError(this.handleError)
    );
  }
  public apWifiConnection(credentials : WifiCredentials) {
    let url = this.baseUrl + "ap/connect";
    return this.http.post<any>(url, credentials,this.httpHeaders).pipe(
      catchError(this.handleError)
    );
  }
  public setConnectionSetting(setting : ConnectionSetting) : Observable<any>{
    let url = this.baseUrl + "connection/setting";   
    return this.http.post<ConnectionSetting>(url, setting,this.httpHeaders).pipe(
      catchError(this.handleError)
    );
  }
  public setConnectionMethod(setting : IPConnectionMethod) : Observable<any>{
    let url = this.baseUrl + "connection/ip/method";   
    return this.http.post<IPConnectionMethod>(url, setting,this.httpHeaders).pipe(
      catchError(this.handleError)
    );
  }
  ////////////////////////////ASYNC///////////////////////////////////
  ////////////////////////////////////////////////////////////////////
  public setConnectionSetting_async(setting : ConnectionSetting) : Promise<any>{
    return new Promise((resolve, reject)=>{
      this.setConnectionSetting(setting).subscribe((resp)=>{
        resolve(resp);
      },(err)=>{
        reject(err);
      })
    });
  }
  public setConnectionMethod_async(setting : IPConnectionMethod) : Promise<any>{
    return new Promise((resolve, reject)=>{
      this.setConnectionMethod(setting).subscribe((resp)=>{
        resolve(resp);
      },
      (err)=>{
        reject(err);
      })
    });
  }
  public apWifiConnection_async(credentials : WifiCredentials) : Promise<any>{
      return new Promise((resolve, reject)=>{
        this.apWifiConnection(credentials).subscribe((resp)=>{
          resolve(resp);
        }, (err)=>{
          reject(err);
        })
  
      })
  }
  public connectWifi_async(credentials : WifiCredentials) : Promise<any>{
    return new Promise((resolve, reject)=>{
      this.connectWifi(credentials).subscribe((resp)=>{
        resolve(resp);
      }, (err)=>{
        reject(err);
      })

    })
    
  }
  public networks_async(adapter : String) : Promise<Network[]>{
    return new Promise((resolve, reject)=>{
      this.networks(adapter).subscribe((resp)=>{
        resolve(resp);
      }, (err)=>{
        reject(err);

      });
      
    })
  
  }
  public hostConnectionEcho_async() : Promise<string>{
    return new Promise((resolve, reject)=>{
      this.hostConnectionEcho().subscribe((resp)=>{
          resolve(resp.address);
      },
      (err)=>{
          reject(err);
      });
    });
}
public devices_async() : Promise<DeviceInfo[]>{
    return new Promise((resolve, reject)=>{
      this.devices().subscribe((resp)=>{
       resolve(resp);
      },(err)=>{
        reject(err);
      });
    })
    
  }
  public connections_async() : Promise<ConnectionInfo[]>{
    return new Promise((resolve, reject)=>{
      this.connections().subscribe((resp)=>{
       resolve(resp);
      },(err)=>{
        reject(err);
      });
    })
    
  }
  public deviceDetails_async(device : String) : Promise<DeviceDetails>{
    return new Promise((resolve, reject)=>{
      this.deviceDetails(device).subscribe((resp)=>{
       resolve(resp);
      },(err)=>{
        reject(err);
      });
    })
    
  }
  public connectionDelete_async(connection : ConnectionInfo) : Promise<any>{
    return new Promise((resolve, reject)=>{
      this.connectionDelete(connection).subscribe((resp)=>{
        resolve(resp);
      },
      (err)=>{
        reject(err);
      });
    });
  }
  public connectionDetails_async(connection : String) : Promise<ConnectionDetails>{
    return new Promise((resolve, reject)=>{
      this.connectionDetails(connection).subscribe((resp)=>{
        resolve(resp);
      },
      (err)=>{
        reject(err);
      });
    });
  }
  handleError(error: HttpErrorResponse){
      console.log(error);
      return throwError(error);
    }
}
