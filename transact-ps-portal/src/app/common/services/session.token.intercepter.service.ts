import { Injectable } from '@angular/core';
import {
  HttpRequest,
  HttpHandler,
  HttpEvent,
  HttpInterceptor
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { RouteGuardService } from './route.guard.service';
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  constructor(public routeGuard : RouteGuardService) {}
  intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    
    request = request.clone({
      setHeaders: {
        Authorization: this.routeGuard.getSessionToken()
      }
    });
    console.log(request);
    return next.handle(request);
  }
}