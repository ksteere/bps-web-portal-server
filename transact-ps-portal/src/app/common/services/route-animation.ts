import {
    transition,
    trigger,
    query,
    style,
    animate,
    group,
    animateChild
 } from '@angular/animations';
 export const fadeTransition =
    trigger('routeAnimations', [
         transition('* <=> *', [
              query(':enter, :leave', 
                   style({ 
                        position: 'absolute',
                        left: 0,
                        width: '100%',
                        opacity: 0 ,
                        transform: 'scale(0) translate(100%)'

                    }), 
                   { optional: true }),        
              group([
                    query(':enter',[
                    animate('1000ms ease', 
                    style(
                        { 
                            position:'relative',
                            opacity: 1 ,
                            transform: 'scale(1) translate(0)'
                    }))
                ], { optional: true }),
                   
              ])
         ]),
    ]);