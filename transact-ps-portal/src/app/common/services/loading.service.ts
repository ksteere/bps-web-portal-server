import { Injectable } from "@angular/core";

@Injectable({
    providedIn: 'root'
  })
  export class LoadingService {
    private loadingBackground : any;
    private loadingContainer : any;
    private loadingText : any;
    constructor(){
        this.loadingBackground = document.getElementById("boha-app-loading-background");
        this.loadingContainer = document.getElementById("boha-app-loading-container");
        this.loadingText = document.getElementById("boha-app-loading-text");
        
        if(this.loadingBackground == undefined || this.loadingContainer == undefined || this.loadingText == undefined){
            throw("loading service failed to initialize due to missing elements...");
        }
    
    }
    public setWaiting(waiting : boolean, displayMessage : String = ""){
        try{
            if(waiting){
                this.loadingBackground.style.display = 'block';
                this.loadingBackground.style.opacity = ".75";
                this.loadingContainer.style.display = 'block';
                this.loadingText.innerText = displayMessage;
            }
            else{
                this.loadingContainer.style.display = 'none';
                this.loadingBackground.style.display = 'none';
                this.loadingBackground.style.opacity = "1";
            }
        }
        catch(err){
            console.log("An error occurred while setting wait screen %s", err);
        }
       
       
    }

  }