import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
import { catchError, retry } from 'rxjs/operators'
import { throwError } from 'rxjs';
import { Connection } from '../data/settings.connection';
import { UserInformation } from '../data/settings.user.information';
import { PortalUser } from '../data/portal-user';
import { DiscreteSetting } from '../data/settings.discrete';
import { stringify } from 'querystring';
@Injectable({
  providedIn: 'root'
})
export class SettingsService {
  baseUrl: string = "/api/v1/settings/";
  httpHeaders = {
    headers: new HttpHeaders({
    'Accept': 'application/json'
    })
  };
  constructor(private http: HttpClient) { }
  public discreteSetting(path : String) : Observable<DiscreteSetting>{
    let normalizedPath : string;
    if(path.charAt(0) == '/'){
      normalizedPath = path.substr(1, path.length -1).toLowerCase();
    }
    else{
      normalizedPath = path.toLowerCase();
    }
    let url = "/api/v1/setting/" + normalizedPath;
    return this.http.get<DiscreteSetting>(url, this.httpHeaders).pipe(
      catchError(this.handleError)
      
    );
  }
  public connections() : Observable<Connection[]>
  {
    let url = this.baseUrl + "connections";
      console.log(url);
      return this.http.get<Connection[]>(url, this.httpHeaders).pipe(
        catchError(this.handleError)
        
      );
      
  }
  public adminPassword(userInfo : UserInformation) : Observable<PortalUser>
  {
    let url = this.baseUrl + "user/admin/password";
      console.log(url);
      return this.http.post<PortalUser>(url,userInfo, this.httpHeaders).pipe(
        catchError(this.handleError)
        
      );
      
  }
  ///////////////////////////ASYNC///////////////////////////////
  public adminPassword_async(userInfo : UserInformation) :  Promise<PortalUser>{
    return new Promise((resolve, reject)=>{
      this.adminPassword(userInfo).subscribe((resp)=>{
          resolve(resp);
      },(err)=>{
        reject(err);
      });
    })
  }
  public connections_async() : Promise<Connection[]>{
    return new Promise((resolve, reject)=>{
      this.connections().subscribe((resp)=>{
          resolve(resp);
      },(err)=>{
        reject(err);
      });
    })

  }
  public discreteSetting_async(path : String) : Promise<DiscreteSetting>{
    return new Promise((resolve, reject)=>{
      this.discreteSetting(path).subscribe((resp)=>{
          resolve(resp);
      },(err)=>{
        reject(err);
      });
    })

  }
  handleError(error: HttpErrorResponse){
      console.log(error);
      return throwError(error);
    }
}
