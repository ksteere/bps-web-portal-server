import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'pop-up-message',
  template: '<div class="boha-error-container">' +
              '<div class="alert alert-danger" *ngIf="displayErrorMessage">' + 
                '<strong>Error!&nbsp;</strong>{{messageText}}'+
              '</div>'+
              '<div class="alert alert-success" *ngIf="displaySuccessMessage">'+
                '<strong>Success!&nbsp;</strong>{{messageText}}.'+
              '</div>'+
            '</div>',
  styleUrls: ['./pop-up-message.component.css'],
  //styles: ['boha-error-container { width: 100%;margin-top: 5px; position: relative; margin-bottom: 5px;}']
})
export class PopUpMessageComponent implements OnInit {
  @Input() messageText:String;
  @Input() messageTimeout : number = 5000;
  private displaySuccessMessage : boolean;
  private displayErrorMessage : boolean;
  constructor() { }

  ngOnInit() {
  }
  public displayMessage(success : boolean, message : String){
    if(!this.displayErrorMessage && !this.displaySuccessMessage){
      this.displaySuccessMessage = success;
      this.displayErrorMessage = !success;
      this.messageText = message;
      setTimeout(()=>{
        this.displaySuccessMessage = false;
        this.displayErrorMessage = false;
          
      }, this.messageTimeout);
    }

  }
}
