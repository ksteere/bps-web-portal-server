import { Component, OnInit, Input } from '@angular/core';
@Component({
  selector: 'modal-confirmation',
  template:'<div class="boha-modal-background" *ngIf="show">' +
              '<div class="boha-modal-box">' +
                '<div class="row boha-modal-header">'+
                  '<div class="col">{{title}}</div>'+
                '</div>'+
                '<div class="row">'+
                  '<div class="col boha-modal-message-area">'+
                    '{{messageText}}' +
                  '</div>'+
                '</div>'+
                '<div class="row">'+
                  '<div class="col">'+
                    '<button class="btn boha-modal-button" (click)="userResponse(0)">Cancel</button>'  +
                    '<button class="btn boha-modal-button" (click)="userResponse(1)">No</button>'+
                    '<button class="btn boha-modal-button" (click)="userResponse(2)">Yes</button>'+
                  '</div>'+
                '</div>'+
              '</div>'+
            '</div>',
  styleUrls: ['./modal-confirmation.component.css']
})
export class ModalConfirmationComponent implements OnInit {
  @Input() messageText:String = "";
  @Input() title:String = "";
  @Input() messageTimeout : number = 5000;
  private  resolve : Function;
  private reject : Function;
  private show : boolean;
  constructor() { }
  ngOnInit() {
  }
  public showDialog(message : String, title :String, autoHide : Boolean = false){
   return new Promise((resolve, reject)=>{
     try{
        if(!this.show){
          this.resolve = resolve;
          this.reject = reject;
          this.show = true;
          this.messageText = message;
          this.title = title;
          if(autoHide){
            setTimeout(()=>{
              this.show = false;
              this.respond(ModalResponse.Cancel);
            }, this.messageTimeout);
          }
          
        }
    
      }
      catch(error){
        this.respond(ModalResponse.Cancel, true);
      }
    })
   
  }
  private respond(response : ModalResponse, reject : Boolean = false){
    if(reject){
      if(this.reject != null && this.reject != undefined){
        this.reject(response);
      }
    }
    else{
      if(this.resolve != null && this.resolve != undefined){
        this.resolve(response);
      }
    
    }
  }
  public userResponse(response : ModalResponse){
    
    this.show = false;
    if(response == 2){
      this.respond(ModalResponse.Yes);
    }
    else if(response == 1){
      this.respond(ModalResponse.No);
    }
    else{
      this.respond(ModalResponse.Cancel);
    }

    
  }
  public hideDialog(){
    this.show = false;
    this.resolve(ModalResponse.Cancel);
  }
}
export const enum ModalResponse {Cancel = 0, No = 1, Yes = 2}; 
