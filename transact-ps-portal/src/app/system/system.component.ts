import { Component, OnInit, ViewChild, ChangeDetectorRef, HostListener } from '@angular/core';
import { PageHeaderService } from '../common/services/page.header.service';
import { SystemService } from '../common/services/system.service';
import { PopUpMessageComponent } from '../common/ui-widgets/pop-up-message/pop-up-message.component';
import { HttProxy } from '../common/data/http.proxy';
import { ConnectivityService } from '../common/services/connectivity.service';
import { IPConnectionMethod } from '../common/data/ip.connection.method';
import { SettingsService } from '../common/services/settings.service';
import { Connection } from '../common/data/settings.connection';
import { DeviceInfo } from '../common/data/device.info';
import { parsePrimaryIp, getSubnetFromMask, validateIPAddress, getFormattedErrorMessage, getMaskFromSubnetDot, getProxyParts, assembleProxyAddress } from '../common/services/global-helper-functions';
import { ModalConfirmationComponent, ModalResponse} from '../common/ui-widgets/modal-confirmation/modal-confirmation.component';
import { LoadingService } from '../common/services/loading.service';
@Component({
  selector: 'app-system',
  templateUrl: './system.component.html',
  styleUrls: ['./system.component.css'],
})
export class SystemComponent implements OnInit {
  @ViewChild(PopUpMessageComponent) popup : PopUpMessageComponent
  private proxySetting:String = "";
  private apIPAddress : String = "";
  private apConnectionName : String;
  private apSubnetAddress : String = "";  
  private apGatewayAddress : String = "";
  private apMethod : String = "auto";
  private apConnectionMethod : String;
  private apNetworkingSettingsTouched : boolean = false;
  private apIPFieldError : boolean = false;
  private apSubnetFieldError : boolean = false;
  private apGatewayFieldError : boolean = false;
  
  private isConnectionName : String;
  private isIPAddress : String = "";
  private isSubnetAddress : String = "";  
  private isGatewayAddress : String = "";
  private isMethod : String = "auto";
  private isConnectionMethod : String;
  private isNetworkingSettingsTouched : boolean = false;
  private isIPFieldError : boolean = false;
  private isSubnetFieldError : boolean = false;
  private isGatewayFieldError : boolean = false;
  
  private hostIPAddress : String;
  private proxySettingsTouched : boolean = false;
  private apManualIP :boolean = false;
  private isManualIP :boolean = false;
  @ViewChild(ModalConfirmationComponent) confirmation : ModalConfirmationComponent
  constructor(private pageHeaderService : PageHeaderService, private systemService : SystemService, 
    private connectivityService : ConnectivityService, private cd: ChangeDetectorRef, 
    private settingsService : SettingsService, private loadingService : LoadingService) { }
  ngOnInit() {
    this.systemService.getProxySetting().subscribe((resp)=>{
      this.proxySetting = resp.httpProxy;
      let parts = getProxyParts(this.proxySetting.toLowerCase());
      console.log(parts);
      let proxy = assembleProxyAddress(parts);
      console.log(proxy);
    },
    (err)=>{
      console.log("Error retrieving proxy setting "+ err.error);
    });
    this.getConnectionNames();
   
  }

  ngAfterViewInit(){
    console.log("System view init....");
    this.pageHeaderService.setHeaderTitle("Networking and System Configuration", "fa-network-wired");
  }
  ngDestroy(){
  }

  public async getAPConnectionInfo(deviceInfo : DeviceInfo){
    try{
      if(deviceInfo != null || deviceInfo != undefined){
        let apConnnection = deviceInfo.connection;
        let apcondetails = await this.connectivityService.connectionDetails_async(apConnnection);
        /**************AP CONNECTIVITY DETAILS*************************** */
        this.apConnectionName = apcondetails.id;
        this.apIPAddress = (apcondetails.primaryAddress  != null ?parsePrimaryIp(apcondetails.primaryAddress) : "NONE");
        this.apMethod = apcondetails.method;
        this.apSubnetAddress = getMaskFromSubnetDot(parsePrimaryIp(apcondetails.primaryAddress, false).toString());
        this.apGatewayAddress = apcondetails.gateway;
        this.apConnectionMethod = apcondetails.method;
        this.apManualIP = (this.apMethod == "shared" || this.isMethod == "manual");
        this.apNetworkingSettingsTouched = false;//this.apManualIP;
      }
      else{
        console.log("Device info is missing...unable get AP connection info")
      }  
    }
    catch(error){
      let message = getFormattedErrorMessage("An error was returned while getting AP connection info.", error);
      console.log(message);
    }
    

  }
  public async getISConnectionInfo(deviceInfo : DeviceInfo){
    try{
      if(deviceInfo != null || deviceInfo != undefined){
        /**************IS CONNECTIVITY DETAILS*************************** */
        let isConnnection = deviceInfo.connection;
        let iscondetails = await this.connectivityService.connectionDetails_async(isConnnection);//.then((iscondetails)=>{
        this.isConnectionName = iscondetails.id;
        this.isIPAddress = (iscondetails.primaryAddress  != null ? parsePrimaryIp(iscondetails.primaryAddress) : "NONE");
        this.isMethod = iscondetails.method;
        this.isSubnetAddress = getMaskFromSubnetDot(parsePrimaryIp(iscondetails.primaryAddress, false).toString());
        this.isGatewayAddress = iscondetails.gateway;
        this.isManualIP = (this.isMethod == "shared" || this.isMethod == "manual");
        this.isNetworkingSettingsTouched = false;//this.isManualIP;== "manual");
        this.apNetworkingSettingsTouched = false;//this.apManualIP;
      }
      else{
        console.log("Device info is missing...unable get IS connection info")
      }
    }
    catch(error){
      let message = getFormattedErrorMessage("An error was returned while getting IS connection info.", error);
      console.log(message);
    }
   

  }
  public async  getConnectionNames(){
    try{
        this.loadingService.setWaiting(true, "Retrieving connection information....");
        this.hostIPAddress = await this.connectivityService.hostConnectionEcho_async();
        let connsettings = await this.settingsService.connections_async();
        this.apManualIP = false;
        this.isManualIP = false;
        let apconn : Connection = connsettings.find(conn=>conn.mode == "ap");
        let isconn : Connection = connsettings.find(conn=>conn.mode == "is");
        if(apconn != null && isconn != null){
            let devicesInfo = await this.connectivityService.devices_async();
            let apdevice : DeviceInfo = devicesInfo.find(device=>device.device == apconn.adapter);
            let isdevice : DeviceInfo = devicesInfo.find(device=>device.device == isconn.adapter);
            await this.getAPConnectionInfo(apdevice);
            await this.getISConnectionInfo(isdevice);
          }
          else{
            console.log("Error: connection information is missing.");
          }
    }
    catch(error){
      let message = getFormattedErrorMessage("An error was returned while getting connection info.", error);
      console.log(message);
    }
    finally{
      this.loadingService.setWaiting(false, "");
     
    }

  } 
  /************************************PROXY*************************************** */
  public async saveProxySettings(){
    let httpProxySettings = new HttProxy();
    try{
      this.loadingService.setWaiting(true, "Saving Proxy settings.....");
      httpProxySettings.httpProxy = this.proxySetting;

    // JRN JIRA WWP-4 Make changing proxy setting persistent
    // call setProxySetting_async() instead of setProxySetting()
    let resp = await this.systemService.setProxySetting_async(httpProxySettings);
      this.proxySettingsTouched = false;
      this.popup.displayMessage(true, "Successfully saved proxy settings.");
    }
    catch(error){
      let message = getFormattedErrorMessage("Unable to save the proxy settings.", error);
      this.popup.displayMessage(false, message);
      console.log(message);
    }
    finally{
      this.loadingService.setWaiting(false, "");
    }
  }
  public proxySettingKeyEvent(event : Event){
    this.proxySettingsTouched = true;
  }
  /************************************GENERIC FUNCTIONS************************** */
  
  /**************************AP Mode******************************/
  public resetAPIPFieldErrors(){
    this.apIPFieldError = false;
    this.apSubnetFieldError = false;
    this.apGatewayFieldError = false;
  }
  public validateAPNetworSettings(){
      this.apIPFieldError = !validateIPAddress(this.apIPAddress);
      this.apSubnetFieldError = !validateIPAddress(this.apSubnetAddress);
      this.apGatewayFieldError = !validateIPAddress(this.apGatewayAddress);
      return (!this.apIPFieldError && !this.apGatewayFieldError && !this.apSubnetFieldError);
  }
  public async  saveAPNetworSettings(){
    try{
      this.loadingService.setWaiting(true, "Saving AP settings.....");
      let proceed = this.validateAPNetworSettings();
      if(proceed){
        let dot = getSubnetFromMask(this.apSubnetAddress);
        if(dot != -1){
          let method : IPConnectionMethod = new IPConnectionMethod();
          method.gateway = this.apGatewayAddress;
          method.ip = this.apIPAddress + "/" + dot.toString();
          method.subnet = this.apSubnetAddress;
          method.method = this.apMethod;
          method.connection =  this.apConnectionName;
          await this.connectivityService.setConnectionMethod_async(method);//.subscribe((resp)=>{
          this.apNetworkingSettingsTouched = false;
          this.popup.displayMessage(true, "Successfully saved AP settings.");
        }
        else{
          this.popup.displayMessage(false, "Subnet formation appears to be invalid.");
        }
      }
      else{
        setTimeout(()=>{this.resetAPIPFieldErrors()}, 5000);
        this.popup.displayMessage(false, "Invalid IP address format detected.");
      }
    }
    catch(error){
      let message = getFormattedErrorMessage("An error occured while saving AP network settings", error);
      this.popup.displayMessage(false, message);
      setTimeout(()=>{this.resetAPIPFieldErrors()}, 5000);
      console.log(message);
    }
    finally{
      this.loadingService.setWaiting(false, "Saving IS settings.....");
    }
    
   
  }
  /**************************Infrastructure Mode******************************/
  public resetISIPFieldErrors(){
    this.isIPFieldError = false;
    this.isSubnetFieldError = false;
    this.isGatewayFieldError = false;
  }
  public validateISNetworSettings(){
    this.isIPFieldError = !validateIPAddress(this.isIPAddress);
    this.isSubnetFieldError = !validateIPAddress(this.isSubnetAddress);
    this.isGatewayFieldError = !validateIPAddress(this.isGatewayAddress);
    return (!this.isIPFieldError && !this.isGatewayFieldError && !this.isSubnetFieldError);
  }
  public async saveISNetworSettings(){
    try{
      this.loadingService.setWaiting(true, "Saving IS settings.....");
      let proceed = this.validateISNetworSettings();
      if(proceed){
        let dot = getSubnetFromMask(this.isSubnetAddress);
        if(dot != -1){
          let method : IPConnectionMethod = new IPConnectionMethod();
          method.gateway = this.isGatewayAddress;
          method.ip = this.isIPAddress + "/" + dot.toString();
          method.subnet = this.isSubnetAddress;
          method.method = this.isMethod;
          method.connection = this.isConnectionName;
          await this.connectivityService.setConnectionMethod_async(method);
          this.isNetworkingSettingsTouched = false;
          this.popup.displayMessage(true, "Successfully saved AP settings.");
        }
        else{
          this.popup.displayMessage(false, "Subnet formation appears to be invalid.");
        }
      }
      else{
        setTimeout(()=>{this.resetISIPFieldErrors()}, 5000);
        this.popup.displayMessage(false, "Invalid IP address format detected.");
      }
    }
    catch(error){
      let message = getFormattedErrorMessage("An error occured while saving IS network settings", error);
      setTimeout(()=>{this.resetISIPFieldErrors()}, 5000);
      this.popup.displayMessage(false, message);
      console.log(message);
    }
    finally{
      this.loadingService.setWaiting(false, "");
    }
  }
  public apAddressKeyEvent(event : any){
    this.apNetworkingSettingsTouched = true;
    
  }
  public isAddressKeyEvent(event : any){
    this.isNetworkingSettingsTouched = true;
    
  }
  public toggleISManual(){
    let temp = this.isManualIP;
    this.isManualIP = !this.isManualIP;
    console.log(this.isManualIP);
    this.isMethod = (this.isManualIP ? "manual" : "auto");
    this.cd.markForCheck();
    this.isNetworkingSettingsTouched = true; 
  }
  public toggleAPManual(){
    this.apManualIP = !this.apManualIP;
    console.log(this.apManualIP);
    this.apMethod = (this.apManualIP ? "shared" : "auto");
    this.cd.markForCheck();
    this.apNetworkingSettingsTouched = true;

  }
  async preCheckConnectionData(connectionMode : String){
    if(connectionMode == "ap"){
      if(this.apIPAddress == this.hostIPAddress){
        let response = await this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirmation", false);
        if(response == ModalResponse.Yes){
          this.saveAPNetworSettings();
        }
        
      }
      else{
        this.saveAPNetworSettings();
      }
    }
    else if(connectionMode == "is"){
      if(this.isIPAddress == this.hostIPAddress){
        let response = await this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirm change", false);
        if(response == ModalResponse.Yes){
          this.saveISNetworSettings();
        }
      }
      else{
        this.saveISNetworSettings();
      }
    }
  }
}
