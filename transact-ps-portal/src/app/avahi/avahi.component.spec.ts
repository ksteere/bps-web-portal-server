import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AvahiComponent } from './avahi.component';

describe('AvahiComponent', () => {
  let component: AvahiComponent;
  let fixture: ComponentFixture<AvahiComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AvahiComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AvahiComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
