import { Component, OnInit, ViewChild } from '@angular/core';
import { PageHeaderService } from '../common/services/page.header.service';
import { PopUpMessageComponent } from '../common/ui-widgets/pop-up-message/pop-up-message.component';
import { SystemService } from '../common/services/system.service';
import { AvahiService } from '../common/data/avahi.service';

@Component({
  selector: 'app-avahi',
  templateUrl: './avahi.component.html',
  styleUrls: ['./avahi.component.css']
})
export class AvahiComponent implements OnInit {
  private avahiServiceName : String = "Avahi Service";
  private avahiSettingsTouched : Boolean = false;
  @ViewChild(PopUpMessageComponent) popup : PopUpMessageComponent
  constructor(private pageHeaderService : PageHeaderService, private systemService : SystemService) { }

  ngOnInit() {
    this.getAvahiServicInformation();
  }
  ngAfterViewInit(){
    console.log("Avahi view init....");
    this.pageHeaderService.setHeaderTitle("Avahi Configuration", "fa-magic");
  }
  ngDestroy(){
    
  }
  public getAvahiServicInformation(){
    this.systemService.getAvahiName().subscribe((resp)=>{
      this.avahiServiceName = resp.serviceName.trim();//just in case we get some whitespace by mistake
    },(err)=>{
      var error = "An error occured while retieving the avahi service information. " + err.error;
      this.popup.displayMessage(false, error);
      console.log( error);
    })
  }
  public test(){
    console.log("this is a test");
  }
  public saveAvahiServiceInformation(){
    console.log("saveAvahiServiceInformation");
    let serviceData : AvahiService = new AvahiService();
    serviceData.serviceName = this.avahiServiceName;
    if(this.avahiServiceName.length > 0){
      this.systemService.setAvahiName(serviceData).subscribe((resp)=>{
        console.log(resp);
        this.popup.displayMessage(true, "Successfully updated avahi service name");  
        this.avahiSettingsTouched = false;
      },(err)=>{
        console.log("saveAvahiServiceInformation error A");
        var error = "An error occured while retieving the avahi service information. " + err.error;
        this.popup.displayMessage(false, error);
        console.log( error);
      })
    }
    else{
      console.log("saveAvahiServiceInformation error B");
      var error = "Service name can not be blank. Please provide a valid name.";
      this.popup.displayMessage(false, error);
      console.log( error);
    }

  }
  public avahiServiceNameKeyEvent(event : Event){
    this.avahiSettingsTouched = true;
  }
}
