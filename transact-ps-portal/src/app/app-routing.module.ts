import { NgModule } from "@angular/core";
import { Routes, RouterModule } from "@angular/router";
import { HomeComponent } from "./home/home.component";
import { LoginComponent } from "./login/login.component";
import { ConnectivityComponent } from "./connectivity/connectivity.component";
import { RouteGuardService } from "./common/services/route.guard.service";
import { AdminComponent } from "./admin/admin.component";
import { SystemComponent } from "./system/system.component";
import { AvahiComponent } from "./avahi/avahi.component";

const routes: Routes = [  
          
    {
      path: "home",
      canActivate: [RouteGuardService],
      //loadChildren: "./home-component/home.module#HomeModule",
      component: HomeComponent,
      data: { pageTitle: "Home", animation:"Avahi"}
    },
    {
      path: 'login',
      component: LoginComponent,
      data: { pageTitle: "Login", animation : "Login"}
    },
    {   
      canActivate: [RouteGuardService],
      path: 'connect',
      component: ConnectivityComponent,
      data: { pageTitle: "Connectivity", animation: "Connectivity"}

    },
    {   
      canActivate: [RouteGuardService],
      path: 'admin',
      component: AdminComponent,
      data: { pageTitle: "Administration", animation: "Administration"}

    },
    {   
      canActivate: [RouteGuardService],
      path: 'system',
      component: SystemComponent,
      data: { pageTitle: "System", animation: "System"}

    },
    {   
      canActivate: [RouteGuardService],
      path: 'avahi',
      component: AvahiComponent,
      data: { pageTitle: "Avahi",animation: "Avahi" }

    },
    {
      path: '',
      redirectTo: '/home',
      pathMatch: 'full'
    }
      
  
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule]
})
export class AppRoutingModule 
{  

  
}
