import { Component, OnInit, HostListener } from '@angular/core';
import { Connection } from '../common/data/settings.connection';
import { SettingsService } from '../common/services/settings.service';
import { ConnectivityService } from '../common/services/connectivity.service';
import { LoadingService } from '../common/services/loading.service';
import { DeviceDetails } from '../common/data/device.details';
import { ConnectionDetails } from '../common/data/connection.details';
import { PageHeaderService } from "../common/services/page.header.service";
import { AvahiService } from '../common/data/avahi.service';
import { ConnectivityMethod } from '../common/data/global.enumerations';
import { SystemService } from '../common/services/system.service';
import { throwError } from 'rxjs';
import { parsePrimaryIp } from '../common/services/global-helper-functions';
import { DeviceInfo } from '../common/data/device.info';
import { DiscreteSetting } from '../common/data/settings.discrete';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
     
export class HomeComponent implements OnInit {
  private NOTCONNECTED : string = "Not connected";
  private refreshing: boolean = false;
  private longPollHandle = null;
  private hostIPAddress : string;
  private isDevice : String;
  private apDevice : String;
  private isIPAddress : String;
  private apIPAddress : String;
  private iStructureDetails : ConnectionDetails;
  private apConnDetails : ConnectionDetails;
  private connectionMode : ConnectivityMethod; 
  private avahiService : AvahiService;
  ///////////////Connectivity Testing///////////////////////////
  private remoteByteCount : number = 0;;
  private remoteErrorCount : number = 0;;
  private remoteRequestCount : number = 0;
  private windowLoads : number = 0;
  private testInterval : any = null;
  private isTestMode : boolean = false;

  /////////////////////////////////////////////////////////////
  constructor(private settingsService : SettingsService, 
    private connectivityService : ConnectivityService, private loadingService : LoadingService, 
    private pageHeaderService : PageHeaderService, private systemConfigService : SystemService) { 
    this.iStructureDetails = new ConnectionDetails();
    this.apConnDetails = new ConnectionDetails();
    this.connectionMode = ConnectivityMethod.NONE;
    this.refreshing = false;
    this.avahiService = new AvahiService();
  }
  ngOnInit() {
    console.log("Home init....");
    this.updateConnectionData();
    this.updateAvahiData();
    this.getMode();
    this.pageHeaderService.autoRefreshClicked.subscribe((refresh)=>{
      if(refresh && this.longPollHandle == null){
        this.longPollHandle = setInterval(()=>{
          console.log("Refreshing....");
          this.updateConnectionData();
        }, 5000);
      }
      else{
        if(this.longPollHandle != null){
          clearInterval(this.longPollHandle);
          this.longPollHandle = null;
        }
      }
    
    });
    if(this.isTestMode){
      this.startConnectivityTest();
    }
  }
  ngAfterViewInit(){
    console.log("Home view init....");
    this.pageHeaderService.setHeaderTitle("Home (configuration overview)", "fa-home-alt");
  }
  ngDestroy(){
    this.pageHeaderService.autoRefreshClicked.unsubscribe();
    if(this.longPollHandle){
      clearInterval(this.longPollHandle);
    }
  }
  ///////////////Connectivity Testing///////////////////////////
  public async getMode(){
    try{

      let mode = await this.settingsService.discreteSetting_async("/mode");
      this.isTestMode = (mode != null && mode.data == "debug");
    }
    catch(error){
      console.log(error);
    }
  
  }
  public startStopTest(){
    if(this.testInterval == null){
      this.startConnectivityTest();
    }
    else{
      this.stopConnectivityTest();
    }
  }
  public stopConnectivityTest(){
    if(this.testInterval){
      clearInterval(this.testInterval);
      this.testInterval = null;
    }
  }
  public startConnectivityTest(){
    if(this.testInterval == null){
      console.log("Starting connectivity test...");
      let testwindow = window.open("about:blank");
      let urls = ["http://www.google.com", "http://www.microsoft.com", "http://www.boston.com", "https://www.raspberrypi.org/"];
      this.testInterval = setInterval(()=>{
        var count = Math.floor(Math.random() * Math.floor(5));
        var urlindex = Math.floor(Math.random() * Math.floor(3));
        let reloadWindow : boolean = Math.floor(Math.random() * Math.floor(10))%2 == 0;
        if(reloadWindow){
          var url = urls[urlindex];
          testwindow.location.href = url;
          this.windowLoads ++;
        }
        for(var index = 0; index < count; index ++){
          this.remoteRequestCount ++;
          this.connectivityService.uplinkTest("https://cors-anywhere.herokuapp.com/").subscribe((resp)=>{
              this.remoteByteCount += resp.length;
          },(err)=>{
              this.remoteErrorCount++;
              console.log(err);
            })
        }
       
      }, 20000);
     
    }
  }
  /////////////////////////////////////////////////////////////
  public async evaluateConnectivityMode(devices : DeviceInfo []){
      //The crux of the sanity checks below try to determine 2 things.
      //1 which device or HW are are evaluating 
      //2 does the mode and type align with what we expect
      //We have a dedicated radio for AP which should never be in infrastructure mode but also never of type ethernet except for testing
      //and we have infrastructure HW that should never be in AP/Hotspot mode
    let mode : ConnectivityMethod = ConnectivityMethod.NONE;
    try{
      for(let index = 0; index < devices.length; index ++){
        let device = devices[index];
        console.log(device);
        if(device.state.toLowerCase() === "connected")
        {
          let details  = await this.connectivityService.deviceDetails_async(device.device);
          let ipAddress = parsePrimaryIp(details.ip4address_1);
          if(ipAddress == this.hostIPAddress){
            if(this.isDevice == device.device){
              if(device.type == "wifi"){
                mode = ConnectivityMethod.Wifi;
              }
              else if(device.type == "ethernet"){
                mode = ConnectivityMethod.Ethernet;
              }
              
            }
            else if(this.apDevice == device.device){
              if(device.type == "wifi"){
                mode = ConnectivityMethod.AP;
              }
              else if(device.type == "ethernet"){
                mode = ConnectivityMethod.Ethernet;
              }
              
            }
            else{
              if(device.type == "wifi"){
                mode = ConnectivityMethod.Wifi;
              }
              else if(device.type == "ethernet"){
                mode = ConnectivityMethod.Ethernet;
              }
             
            }  
          }
          

        }
      
      }
    }
    catch(error){
      console.log(error);
    }
    finally{
      return mode;
    }
    
  }
  public getAvahiServiceName() : Promise<AvahiService>{
    return new Promise((resolve, reject)=>{
      this.systemConfigService.getAvahiName().subscribe((resp)=>{
          resolve(resp);
      },(err)=>{
        reject(err);
      });
    })
  }
  public async updateAvahiData(){
    try{
      this.avahiService = await this.getAvahiServiceName();
    }
    catch(error){
      console.log("An error occured while retrieving the avahi service name. " + error.error);
    }

  }
  public async updateConnectionData(){
    let devices : DeviceInfo[] = [];
    try{
      this.refreshing = true;
      const connectionSettings = await this.settingsService.connections_async();
      devices = await this.connectivityService.devices_async();
      const hostIPAddress = await this.connectivityService.hostConnectionEcho_async();
      this.hostIPAddress = hostIPAddress;
      await this.updateAPConnectionData(devices, connectionSettings);
      await this.updateISConnectionData(devices, connectionSettings);
    }
    catch(error){
      if(error.error == undefined){
        console.log(error);
      }
      else{
        console.log("An error occured while updating the data. " +  error.error);
      }
    }
    finally{
      let mode = await this.evaluateConnectivityMode(devices); 
      this.setConnectivityMode(mode);
      this.refreshing = false;
  
    }
    
  }
  public async updateISConnectionData(devices : DeviceInfo[], connections : Connection[]){
    try{
      this.isIPAddress = "";
      this.isDevice = "";
      this.iStructureDetails = new ConnectionDetails();
      if(devices != null && devices != undefined){
        let isConnection = connections.find(connsetting=>connsetting.mode == "is");
        let isDevice = devices.find(d=>d.device == isConnection.adapter);
        console.log(isConnection);
        if(isDevice  != undefined && isDevice != null){
          this.isDevice = isDevice.device;
          const isConnectionDetails = await this.connectivityService.connectionDetails_async(isDevice.connection);
          this.iStructureDetails = isConnectionDetails;
          this.isIPAddress = parsePrimaryIp(this.iStructureDetails.primaryAddress);
        }
        else{
          throwError("IS Device NOT FOUND!");
        }
      }
      
    }catch(error){
      if(error.error == undefined){
        console.log(error);
      }
      else{
        console.log("An error occured while updating the data. " +  error.error);
      }
      
    }
  }
  public async updateAPConnectionData(devices : DeviceInfo[], connections : Connection[]){
    try{
      this.apIPAddress = "";
      this.apDevice = null;
      this.apConnDetails =  new ConnectionDetails();
      if(devices != null && devices != undefined){
        let apConnection = connections.find(connsetting=>connsetting.mode == "ap");
        console.log(apConnection);
        let apDevice = devices.find(d=>d.device == apConnection.adapter);
        if(apDevice != undefined && apDevice != null){
          this.apDevice = apDevice.device
          const apConnectionDetails = await this.connectivityService.connectionDetails_async(apDevice.connection);
          this.apConnDetails = apConnectionDetails;
          this.apIPAddress = parsePrimaryIp(this.apConnDetails.primaryAddress);
        }
        else{
          throwError({error : "AP Device NOT FOUND!"});
        }
      }
      
    }catch(error){
      if(error.error == undefined){
        console.log(error);
      }
      else{
        console.log("An error occured while updating the data. " +  error.error);
      }
    }

  }
 private setConnectivityMode(mode : ConnectivityMethod){
    this.connectionMode =  mode;
    console.log("Connection mode");
    console.log(mode);
    this.pageHeaderService.setConnectivityIconState(mode);
  }
  public connectedOn(details : ConnectionDetails){
    if(details != null && details != undefined){
      return (parsePrimaryIp(details.primaryAddress) == this.hostIPAddress);
    }
    else{
      return false;
    }
  }
  public navigate(to : string){
    if(to === "is"){
      let location = "http://"+this.isIPAddress+":1337";
      window.location.href = location;
    }
    else if(to === "ap"){
      let location = "http://"+this.apIPAddress+":1337";
      window.location.href = location;
    }
    
  }
}
