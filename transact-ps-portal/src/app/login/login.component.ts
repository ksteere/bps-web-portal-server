import { Component, OnInit, ApplicationRef, ChangeDetectorRef, ViewChild } from '@angular/core';
import { AuthService } from '../common/services/auth.service';
import { RouteGuardService } from '../common/services/route.guard.service';
import { PortalUser } from '../common/data/portal-user';
import { catchError } from 'rxjs/operators';
import { HttpErrorResponse } from '@angular/common/http';
import { UserCredentials } from '../common/data/user.credentials';
import { PageHeaderService } from '../common/services/page.header.service';
import { PopUpMessageComponent } from '../common/ui-widgets/pop-up-message/pop-up-message.component';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  authenticationService : AuthService;
  routerGuardService : RouteGuardService;
  user : PortalUser;
  missingusername:boolean;
  missingpassword:boolean;
  submitted:boolean;
  username : string;
  password : string;
  @ViewChild(PopUpMessageComponent) popup : PopUpMessageComponent
    constructor(authService : AuthService, 
    routeGaurdService : RouteGuardService, private pageHeaderService : PageHeaderService) { 
    this.authenticationService = authService;
    this.routerGuardService = routeGaurdService;
    this.missingusername = false;
    this.missingpassword = false;
    this.submitted = false;
    this.username = "";
    this.password = "";
  }


  ngOnInit() {
    this.routerGuardService.setAuthenticationFlag("admin", "", false, false);
  
  }
  ngAfterViewInit(){
    console.log("Login view init....");
    this.pageHeaderService.setHeaderTitle("Login", 'fa-user');
  }
  ngDestroy(){
   
  }
  public clearErrorStatus(){
    this.missingpassword = false;
    this.missingusername = false;
  }
  //call the authenicate API from the auth service and set the appropriate memeber variables 
  public authenticate(event){
    event.preventDefault();
    this.missingusername = this.username === '';
    this.missingpassword = this.password === '';
    try{
        if(!this.missingpassword && !this.missingpassword){
          let credentials : UserCredentials = new UserCredentials;
          credentials.username = this.username;
          credentials.password = this.password;
          this.authenticationService.authenticate(credentials).subscribe(
            (resp)=>{console.log(this.user);
              this.user = resp
              this.routerGuardService.setAuthenticationFlag(this.user.username,this.user.sessionToken,this.user.isAuthenticated, true);
            },
            (err)=>{
              this.routerGuardService.setAuthenticationFlag("","",false, false);
              this.missingpassword = false;
              this.missingusername = false;
              this.popup.displayMessage(false, "Error authenticating the supplied user. Invalid credentials. " + err.error);
              console.log("error authenticating");
              setTimeout(()=>{
                this.clearErrorStatus();       
                },5000);
              }
             
          );
        }
        else{
          this.popup.displayMessage(false, "Username and password are required!");
          setTimeout(()=>{
            this.clearErrorStatus();   
          },5000);
        }
    
        console.log(event);
    }
    catch(err){
      console.log(err);
    }
  
  }
  //Deprcated function
  public verifyInputs(){
    //Depricated for now
    return !(this.missingusername || this.missingpassword);
  }

}
