import { Component, OnInit, HostListener, ViewChild } from '@angular/core';
import { Connection } from '../common/data/settings.connection';
import { SettingsService } from '../common/services/settings.service';
import { Network } from '../common/data/network';
import { ConnectivityService } from '../common/services/connectivity.service';
import { WifiCredentials } from '../common/data/wifi.credentials.';
import { LoadingService } from '../common/services/loading.service';
import { DeviceDetails } from '../common/data/device.details';
import { ConnectionDetails } from '../common/data/connection.details';
import { PageHeaderService } from '../common/services/page.header.service';
import { PopUpMessageComponent } from '../common/ui-widgets/pop-up-message/pop-up-message.component';
import { HttpErrorResponse } from '@angular/common/http';
import { DeviceInfo } from '../common/data/device.info';
import { parsePrimaryIp, isNullOrUndefined, isStringEmpty, getFormattedErrorMessage } from '../common/services/global-helper-functions';
import { ConnectionInfo } from '../common/data/connection.info';
import { ModalConfirmationComponent, ModalResponse } from '../common/ui-widgets/modal-confirmation/modal-confirmation.component';

@Component({
  selector: 'app-connect',
  templateUrl: './connectivity.component.html',
  styleUrls: ['./connectivity.component.css']
})

export class ConnectivityComponent implements OnInit {
  private NOTCONNECTED : string = "Not connected";
  private wifidropdown : boolean = false;
  private wifipassword : string;
  private wifissid : string;
  private hostIPAddress : String;
  /************CONNECTION INFORMATION*********************/
  /************IS*************************************** */
  private isConnectionMode : String;
  private isConnectionType : String;
  private isConnectionWifi : Boolean = false;
  private isConnectionId :String;
  private isConnectionSSID : string = "Not connected";
  private isConnectionOn : boolean = false;
  private isConnection : ConnectionDetails = null;
  private isConnectionSettingsTouched : Boolean = false;
  private isDevice : String;
  private isIPAddress : String;
  private isDeviceMissingError : boolean = false;
  /************CONNECTION INFORMATION*********************/
  /************AP*************************************** */
  private apConnectionSSID : String;
  private apConnectionMode : String;
  private apConnectionType : String;
  private apDevice : String;
  private apConnectionSSIDOriginal : String;
  private apConnectionId :String;
  private apConnectionSettingsTouched : Boolean = false;
  private apConnectionPassword1 : String;
  private apConnectionPassword2 : String;
  private apConnectionOn : boolean = false;
  private apConnection : ConnectionDetails=null;
  private apIPAddress : String;
  private apDeviceMissingError : boolean = false;
  private networks : Network[] = null;
 
  private showPassword : boolean = false;
  private wifiNetworksRefreshing: boolean = false;
  private longPollHandle = null;
  @ViewChild(PopUpMessageComponent) popup : PopUpMessageComponent
  @ViewChild(ModalConfirmationComponent) confirmation : ModalConfirmationComponent
  constructor(private settingsService : SettingsService, 
    private connectivityService : ConnectivityService, private loadingService : LoadingService, 
    private pageHeaderService : PageHeaderService) { 
  }
  ngOnInit() {
    console.log("Home init....")
    this.getConnectionInfo();
    this.pageHeaderService.autoRefreshClicked.subscribe((refresh)=>{
      console.log(refresh);
      if(refresh && this.longPollHandle == null){
        this.longPollHandle = setInterval(()=>{
          if(!this.wifidropdown){
            this.getConnectionInfo();
          }
        }, 5000);
      }
      else{
        if(this.longPollHandle != null){
          clearInterval(this.longPollHandle);
        }
      }

    });

  }
  ngAfterViewInit(){
    console.log("Connectivity view init....");
    this.pageHeaderService.setHeaderTitle("Connectivity", "fa-wifi");
  }
  ngDestroy(){
    if(this.longPollHandle){
      clearInterval(this.longPollHandle);
    }
  }

  public resetISConnectionInfoData(){
    this.isConnectionMode = "";
    this.isConnectionWifi = false;
    this.isDevice = "";
    this.isConnectionSSID = "NONE";
    this.isConnectionId = "";
    this.isConnectionOn = false;      
    this.isConnection = null;      
    this.apIPAddress = "";
  }
  public resetAPConnectionInfoData(){
    this.apConnectionMode = "";
    this.apConnectionType = "";
    this.apDevice = "";
    this.apConnectionSSID =  "NONE";
    this.apConnectionId = "";
    this.apConnectionSSIDOriginal = "";
    this.apConnectionOn = false;
    this.apConnection = null;
    this.isIPAddress = "";
    
      
  }
  public async getConnectionInfo(){
    try{
      this.loadingService.setWaiting(true, "Retrieving connection info....");
      let connsettings = await this.settingsService.connections_async();
      let devicesInfo = await this.connectivityService.devices_async();
      this.resetISConnectionInfoData();
      this.resetAPConnectionInfoData();
      this.hostIPAddress = await this.connectivityService.hostConnectionEcho_async();
      await this.getISConnectionInfo(devicesInfo, connsettings);
      await this.getAPConnectionInfo(devicesInfo, connsettings);
      
    }
    catch(error)
    {
      console.log(error);
    }
    finally{
      this.loadingService.setWaiting(false, "");
    }
   
  }
  public async getISConnectionInfo(devicesInfo : DeviceInfo[], connsettings : Connection[]){
    try{
      this.isDeviceMissingError = false;
      let isConn : Connection = connsettings.find(conn=>conn.mode == "is");
      console.log(isConn);
      if(isConn != null){
        this.isConnectionMode = isConn.mode;
        this.isConnectionType = isConn.type;
        this.isConnectionWifi = (isConn.type == "wifi");
      }
      let isdevice : DeviceInfo = devicesInfo.find(device=>device.device == isConn.adapter);
      if(isdevice != null){
        this.isDevice = isdevice.device;
        let isConnnection = isdevice.connection;
        if(isConnnection != null && isConnnection != undefined && isConnnection.length != 0 && isConnnection != "--"){
          let isCondetails = await this.connectivityService.connectionDetails_async(isConnnection); 
          this.isConnectionSSID = (isCondetails.ssid  != null ?isCondetails.ssid.toString() : "NONE");
          this.isConnectionId = isCondetails.id;
          this.isConnectionOn = isCondetails.ssid  != null;    
          this.isConnection = isCondetails;  
          this.wifissid = isCondetails.ssid.toString();      
          this.isIPAddress = parsePrimaryIp(isCondetails.primaryAddress);
        }
        this.getWifiNetworks(this.isDevice);
      }
      else{
        this.isDeviceMissingError = true;
      }
    }
    catch(error)
    {
      console.log(error);
    }
  }
  public async  getAPConnectionInfo(devicesInfo : DeviceInfo[], connsettings : Connection[]){
    try{
      this.apDeviceMissingError = false;
      let apConn : Connection = connsettings.find(conn=>conn.mode == "ap");
      console.log(apConn);
      this.isConnectionMode = apConn.mode;
      this.isConnectionType = apConn.type;
      let apdevice : DeviceInfo = devicesInfo.find(device=>device.device == apConn.adapter);
      if(apdevice != null){
        this.apDevice = apdevice.device;
        let apConnnection = apdevice.connection;
        console.log(apConnnection);
        if(apConnnection != null && apConnnection != undefined && apConnnection.length != 0 && apConnnection != "--"){
          let apCondetails = await this.connectivityService.connectionDetails_async(apConnnection);             
          this.apConnectionSSID = (apCondetails.ssid  != null ?apCondetails.ssid : "NONE");
          this.apConnectionId = apCondetails.id;
          console.log(this.apConnectionId);
          this.apConnectionSSIDOriginal = this.apConnectionSSID;
          this.apConnectionOn = apCondetails.ssid  != null;
          this.apConnection = apCondetails;
          this.apIPAddress = parsePrimaryIp(apCondetails.primaryAddress);
        }
      }
      else{
        this.apDeviceMissingError = true;
      }
     
    }
    catch(error)
    {
      console.log(error);
    }
  }
  public async getWifiNetworks(adapter : String){
    try{
      this.wifiNetworksRefreshing = true;
      this.networks = await this.connectivityService.networks_async(adapter);
    }
    catch(error){
      console.log(error);
    }
    finally{
      this.wifiNetworksRefreshing = false;
    }
   
  }
  public selectWifiNetwork(ssid : string){
    this.dropDownWifiSelection(false);
    this.wifissid = ssid;
    this.isConnectionSettingsTouched = true;
  }
  public dropDownWifiSelection(drop : boolean){
    this.wifidropdown = drop;
  }
  public async connectToWifiNetwork(){
    try{
      this.loadingService.setWaiting(true, "Attempting to connect....");
      let connsettings = await this.settingsService.connections_async();
      //credentials.adapter = this.isDevice.toString();
      let credentials : WifiCredentials = new WifiCredentials;
      credentials.ssid = this.wifissid;
      credentials.password = this.wifipassword;
      let isConn : Connection = connsettings.find(conn=>conn.mode == "is" && conn.type =="wifi");
      credentials.adapter = isConn.adapter; //this.isDevice
      await this.connectivityService.connectWifi_async(credentials);
      await this.getConnectionInfo();
      this.popup.displayMessage(true, "Successfully connected to network " + this.wifissid + ".");
    }
    catch(error){
      let message = getFormattedErrorMessage( "Failed connecting to " + this.wifissid + ".", error);
      console.log(message);
      this.popup.displayMessage(false, message);
      
    }
    finally{
      this.loadingService.setWaiting(false);
      this.isConnectionSettingsTouched = false;
    }
    
  }
  public onShowPassword(show : boolean){
    this.showPassword = show;
    let element : any = document.getElementById("input-wifi-password");
    if (this.showPassword) {
      element.type = "text";
    } 
    else {
      element.type = "password";
    }
  }

  public async saveAPSettings(){
    try{
      let credentials : WifiCredentials = new WifiCredentials();
      credentials.ssid = this.apConnectionSSID.toString();
      credentials.password = this.apConnectionPassword1.toString();
      credentials.adapter = this.apDevice.toString();
      //let connsettings = await this.settingsService.connections_async();
      //let apConn : Connection = connsettings.find(conn=>conn.type == "ap");
      this.loadingService.setWaiting(true, "Attempting to save AP settings....");
      let connected = await this.connectivityService.apWifiConnection_async(credentials);
      console.log(connected);
      this.apConnectionSettingsTouched = false;
      this.popup.displayMessage(true, "Successfully updated AP settigns.");
    
    }
    catch(error){
      let message = getFormattedErrorMessage("An error occured while trying to save AP settings. " , error);
      console.log(message);
    }
    finally{
      this.loadingService.setWaiting(false);
    }
  }
  apFieldKeyEvent(event : Event){
    this.apConnectionSettingsTouched =  true;
  }
  isSSIDKeyEvent(event : Event){
    this.isConnectionSettingsTouched =  true;
  }
  async preCheckConnectionData(connectionMode : String){
    if(connectionMode == "ap"){
      if(this.apIPAddress == this.hostIPAddress){
        let response = await this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirmation", false);
        if(response == ModalResponse.Yes){
          this.saveAPSettings();
        }
        //this.popup.displayMessage(false, "Making changes on the same (Access Point) network you are connnected to. Please connect to IS (" + this.isConnectionSSID + ").");
      }
      else{
        if(!isStringEmpty(this.apConnectionPassword1) && !isStringEmpty(this.apConnectionPassword2)){
          if(this.apConnectionPassword1 != this.apConnectionPassword2){
            this.popup.displayMessage(false, "Passwords must match exactly. Please verify.");
            console.log("Passwords don't match.");
          }
          else{
            this.saveAPSettings();
          }
        }
        else{
          this.popup.displayMessage(false, "Password(s) can't be left blank and must match exactly.");
        }
        
      }
    }
    else if(connectionMode == "is"){
      if(this.isIPAddress == this.hostIPAddress){
        let response = await this.confirmation.showDialog("You are about to make a change to the network you are connected on. If you continue you will lose your connectivity. Do you wish to continue?", "Confirm change", false);
        if(response == ModalResponse.Yes){
          this.connectToWifiNetwork();
        }
        //need to have a setting to do one or the other - allow with confirmation or not allow at all
        //this.popup.displayMessage(false, "You can't make changes on the same (Infrastructre) network you are connnected on. Please connect to the Acecess Point(" + this.apConnectionSSID + ").");
      }
      else{

        this.connectToWifiNetwork();
      }
    }
  }

  public async deleteConnection(connectionMode : String){
    try{
 
      let connectionInfo : ConnectionInfo = new ConnectionInfo();
      if(connectionMode == "ap"){
        if(!isStringEmpty(this.apConnectionId)){
          connectionInfo.name = this.apConnectionId;
        }
        else{
          console.log("Connection ID can't be empty.");
        }
        
      }
      else if(connectionMode == "is"){
        if(!isStringEmpty(this.isConnectionId)){
          connectionInfo.name = this.isConnectionId;
        }
        else{
          console.log("Connection ID can't be empty.");
        }
        
        
      }
      let response = await this.confirmation.showDialog("You are about to delete the network connection " + connectionInfo.name + ". Do you wish to continue?", "Confirm delete " + connectionInfo.name, false);
      if(response == ModalResponse.Yes){
        this.loadingService.setWaiting(true, "Attempting to delete connection " + connectionInfo.name);
        await this.connectivityService.connectionDelete_async(connectionInfo);
        this.popup.displayMessage(true, "Successfully delete connection " + connectionInfo.name);
      }
      
    }
    catch(error){
      let message = getFormattedErrorMessage("An error occured while trying to delete the connection. ", error );
      this.popup.displayMessage(false, message);
      
    }
    finally{
      this.loadingService.setWaiting(false);

    }
    
  }
  @HostListener('document:click', ['$event.target']) documentClickEvent(target : HTMLElement) : void{
    let drowdownTarget : boolean = false;
    if(target != null){
      if(target.id === 'wifi-input-drop-down-button'){
        drowdownTarget = true;
        
      }
      else{
        if(target.parentElement != null){
          if(target.parentElement.id === 'wifi-input-drop-down-button' ){
            drowdownTarget = true;
          }
        }
      }
    }
    else{
      drowdownTarget =false;
    }
    if(!drowdownTarget){
      this.dropDownWifiSelection(false);
    }
  }
}
